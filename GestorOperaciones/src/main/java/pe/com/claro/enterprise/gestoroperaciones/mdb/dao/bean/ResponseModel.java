package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class ResponseModel {

	private int codigoRespuesta;
	private String mensajeRespuesta;

	public int getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(int codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}

	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}

	
	
}
