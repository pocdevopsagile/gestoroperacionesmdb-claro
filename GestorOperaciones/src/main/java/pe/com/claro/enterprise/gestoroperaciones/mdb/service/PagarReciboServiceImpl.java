package pe.com.claro.enterprise.gestoroperaciones.mdb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.Proceso;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.OperacionGOC;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ActEstadoTransaccionRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoResponseModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.RegistroMonitoreoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.JMSConnector;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.response.PagarReciboResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.GestorOperacionesUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;



@Service
public class PagarReciboServiceImpl implements PagarReciboService{

	private static final Logger logger = Logger.getLogger(PagarReciboServiceImpl.class);

	@Autowired
	private OperacionGOC mpagDBDao;
	
	@Autowired
	private OperacionService operacionService;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Autowired
	private JMSConnector jMSConnector;
	
	@Override
	public void pagarRecibo(String trazabilidad,PagarReciboRequest request ) {
		
		long tiempoInicio = System.currentTimeMillis();
		PagarReciboResponse wfRegularDeudaResponse = null;
		String nroTransaccion=request.getTransaccionData().getNumeroTransaccion();
		try {
			logger.info(trazabilidad + " Inicio para metodo procesar Mensaje");
			
	        if(!Constantes.CADENA_CERO.equalsIgnoreCase(request.getTransaccionData().getNumeroIntento())) {
	        	
	        	logger.info(trazabilidad + " [INICIO]-[Actividad 1. Obtiene actividad de Fallo y modifica Objeto]");
	        	ConsultaReintentoResponseModel consultaReintento = mpagDBDao.consultarReintento(trazabilidad, obtenerConsultaReintentoReq(nroTransaccion));
	        	if(consultaReintento.getCodigoRespuesta()==Constantes.CERO) {
					
					List<Proceso> listActividadesFallo = new ArrayList<>();
					
					if(consultaReintento.getCodigoRespuesta()==Constantes.CERO) {
						
						String[] nroActividadesFallo = consultaReintento.getCodComponente().split(Constantes.SEPARADOR_COMA_UNIC);
						
						for (int i = 0; i < nroActividadesFallo.length; i++) {
							
							Proceso actividadFallo = new Proceso();
							actividadFallo.setNumero(Integer.parseInt( nroActividadesFallo[i]));
							listActividadesFallo.add(actividadFallo);
						}
					}
					request.getTransaccionData().setActividadFallos(listActividadesFallo);
	        	}
	        }else {
				logger.info(trazabilidad + " Nueva Transaccion ");

			}
			
			
			logger.info(trazabilidad + " [INICIO]-[Actividad 2. Registra Request ]");
			
			mpagDBDao.insertarRequest(trazabilidad, nroTransaccion,Constantes.COMPONENTE_GOC,
					Constantes.OPERACION_WFREGULARDEUDA,GestorOperacionesUtil.printPrettyJSONString(request));
			
			
			logger.info(trazabilidad + " [FIN]-[Actividad 2. Registra Request ]");
					
			String estado = Constantes.ESTADO_TRX_FINALIZADO_CONERRORES;
			int reintentos=Integer.parseInt( propertiesExterno.reintentoWF);
			int reintentosCont=Integer.parseInt( request.getTransaccionData().getNumeroIntento());
			
			logger.info(trazabilidad + " [INICIO]-[Actividad 3.pagar recibo ]");
			wfRegularDeudaResponse = operacionService.pagarRecibo(trazabilidad, request, obtenerHeaderRequestRest(nroTransaccion));
			logger.info(trazabilidad + " [FIN]-[Actividad 3.pagar recibo ]");
			logger.info(trazabilidad + " Response al pagar recibo : "+GestorOperacionesUtil.printPrettyJSONString(wfRegularDeudaResponse));
			
			if(Constantes.EXITO.equalsIgnoreCase( wfRegularDeudaResponse.getAuditResponse().getCodigoRespuesta())){
				logger.info(trazabilidad + " Exito al pagar recibo");
			}else {
				if(reintentos >reintentosCont) {	
					reintentosCont++;
					
					logger.info(trazabilidad + " [INICIO]-[Actividad 4. encolar Reintento]");
					request.getTransaccionData().setNumeroIntento(reintentosCont+Constantes.TEXTO_VACIO);
					request.getTransaccionData().setNumeroTransaccion(nroTransaccion);
					request.getTransaccionData().setActividadFallos(null);			
					
					jMSConnector.enviarCola(nroTransaccion, trazabilidad, GestorOperacionesUtil.printPrettyJSONString(request));
					logger.info(trazabilidad + " [FIN]-[Actividad 4. encolar Reintento]");
				}else {
						
					logger.info(trazabilidad + " [INICIO]-[Actividad 5. Registrar en tabla Monitoreo]");
					
					mpagDBDao.registrarMonitoreo(trazabilidad, obtenerReqMonitoreoWFMP(nroTransaccion, reintentosCont, wfRegularDeudaResponse));
					logger.info(trazabilidad + " [FIN]-[Actividad 5. Registrar en tabla Monitoreo]");
					
					logger.info(trazabilidad + " [INICIO]-[Actividad 6. Actualizar Estado Transaccion]");
					
					mpagDBDao.actualizarEstadoTransaccion(trazabilidad, propertiesExterno, obtenerActEstTraxWFMP(nroTransaccion,estado));
					
					logger.info(trazabilidad + " [FIN]-[Actividad 6. Actualizar Estado Transaccion]");
				}
			}
				
			
			
		}catch (Exception e) {		
			logger.error(trazabilidad + "[Error Generado:]" + e);
			e.printStackTrace();
			
		}finally {	
			logger.info(trazabilidad + "[FIN][TiempoTotalTransaccion(ms):"+ (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(trazabilidad + "[FIN] - [METODO: procesar] ");
		}
	}
	
	private HeaderRequest obtenerHeaderRequestRest(String nroTransaccion) {
		HeaderRequest headerRequest = new HeaderRequest();
		headerRequest.setIdTransaccion(nroTransaccion);
		headerRequest.setMsgid(nroTransaccion);
		headerRequest.setTimestamp(GestorOperacionesUtil.getFechaString(new Date(), Constantes.FORMATO_TIMESTAMP));
		headerRequest.setUserId(propertiesExterno.usrId);
		headerRequest.setAccept(MediaType.APPLICATION_JSON_VALUE);
		return headerRequest;
	}
	
	
	private RegistroMonitoreoRequestModel obtenerReqMonitoreoWFMP(String idTransaccion,Integer nroReintento,PagarReciboResponse response) {
		RegistroMonitoreoRequestModel registroMonitoreoRequest = new RegistroMonitoreoRequestModel();
		registroMonitoreoRequest.setIdTransaccion(idTransaccion);
		registroMonitoreoRequest.setNroIntento(nroReintento);
		return registroMonitoreoRequest;
	}
	
	private ActEstadoTransaccionRequestModel obtenerActEstTraxWFMP(String nroTransaccion,String estado) {
		ActEstadoTransaccionRequestModel actEstTrxRequest = new ActEstadoTransaccionRequestModel();
		actEstTrxRequest.setIdTransaccion(nroTransaccion);
		actEstTrxRequest.setEstado(estado);
		return actEstTrxRequest;
	}
	
	private ConsultaReintentoRequestModel obtenerConsultaReintentoReq(String nroTransaccion) {
		ConsultaReintentoRequestModel consReintentoReq = new ConsultaReintentoRequestModel();
		consReintentoReq.setNroTransaccion(nroTransaccion);
		return consReintentoReq;
	}
}
