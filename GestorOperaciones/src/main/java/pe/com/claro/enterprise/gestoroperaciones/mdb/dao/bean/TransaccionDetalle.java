package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class TransaccionDetalle {

	
	private String numeroTransaccion;
	private String codigoComponente;
	private String numeroComponente;
	private String numeroDocumento;
	private double monto;
	private String puntoControl;
	private String idAplicacion;
	private String estado;
	private String body;

	private int numeroTransaccionDetale;
	private String customerId;
	private String tipoDocumento;
	private String fechaEmision;
	private int intento;
	
	public String getNumeroTransaccion() {
		return numeroTransaccion;
	}
	public void setNumeroTransaccion(String numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}
	public String getCodigoComponente() {
		return codigoComponente;
	}
	public void setCodigoComponente(String codigoComponente) {
		this.codigoComponente = codigoComponente;
	}
	public String getNumeroComponente() {
		return numeroComponente;
	}
	public void setNumeroComponente(String numeroComponente) {
		this.numeroComponente = numeroComponente;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public String getPuntoControl() {
		return puntoControl;
	}
	public void setPuntoControl(String puntoControl) {
		this.puntoControl = puntoControl;
	}
	public String getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public int getNumeroTransaccionDetale() {
		return numeroTransaccionDetale;
	}
	public void setNumeroTransaccionDetale(int numeroTransaccionDetale) {
		this.numeroTransaccionDetale = numeroTransaccionDetale;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public int getIntento() {
		return intento;
	}
	public void setIntento(int intento) {
		this.intento = intento;
	}
	@Override
	public String toString() {
		return "TransaccionDetalle [numeroTransaccion=" + numeroTransaccion + ", codigoComponente=" + codigoComponente
				+ ", numeroComponente=" + numeroComponente + ", numeroDocumento=" + numeroDocumento + ", monto=" + monto
				+ ", puntoControl=" + puntoControl + ", idAplicacion=" + idAplicacion + ", estado=" + estado + ", body="
				+ body + ", numeroTransaccionDetale=" + numeroTransaccionDetale + ", customerId=" + customerId
				+ ", tipoDocumento=" + tipoDocumento + ", fechaEmision=" + fechaEmision + ", intento=" + intento + "]";
	}
	

}
