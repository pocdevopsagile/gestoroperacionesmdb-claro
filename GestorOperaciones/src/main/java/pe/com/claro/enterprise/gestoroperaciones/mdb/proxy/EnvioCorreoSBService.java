package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.EnvioCorreoRequestBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequestBean;


public interface EnvioCorreoSBService {

	public BodyResponse enviarCorreo(String trazabilidad, HeaderRequestBean headerRequest,EnvioCorreoRequestBean requestBean,String trasanccion,ELKLogLegadoBean elkLegadoBean);
	
}
