package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;

import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.JMSException;

public interface JMSConnector {

	public int enviarCola(String idTransaccion,String trazabilidad,String message)
			throws JMSException, Exception;
}
