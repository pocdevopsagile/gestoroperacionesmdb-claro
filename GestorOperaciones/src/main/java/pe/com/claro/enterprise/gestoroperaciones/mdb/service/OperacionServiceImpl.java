package pe.com.claro.enterprise.gestoroperaciones.mdb.service;

import java.math.BigDecimal;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.crm.clarify.InteraccionType;
import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetDocumentoReqType;
import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetDocumentosReqType;
import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetServicioReqType;
import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetServiciosReqType;
import pe.com.claro.eai.servicecommons.AuditType;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.Actividad;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.AuditoriaRequestSOAPBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.DetalleDocumento;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.DetalleError;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.Documento;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.EnvioCorreoRequestBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequestBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.PagosDetalle;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.Proceso;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.TransaccionData;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.OperacionGOC;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.AplicacionTarjeta;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.Customer;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ListaCustomer;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionDetalle;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.BDException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.EnvioCorreoSBService;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.MotorPagos;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.RecaudacionesPagos;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.TransaccionInteraccionesAsyn;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.TransaccionPagosProxy;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.ConsultaPagoOACRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearInteraccionRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRespo;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.MotorVerificacionAppRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.response.PagarReciboResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ClaroUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ConcentradorPagosUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.GestorOperacionesUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.RSAMotorPagos;

@Service
public class OperacionServiceImpl  implements OperacionService{
	
	private static final Logger logger = Logger.getLogger(OperacionServiceImpl.class);
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Autowired
	private TransaccionInteraccionesAsyn transaccionInteraccionesAsyn;
	
	@Autowired
	private TransaccionPagosProxy transaccionPagosProxy;
	
	@Autowired
	private MotorPagos motorPagos;
	
	@Autowired
	private RecaudacionesPagos recaudacionesPagos;
	
	@Autowired
	private EnvioCorreoSBService envioCorreoSBService;
	
	@Autowired
	private OperacionGOC operacionCCLRepository;
	
	@Override
    public PagarReciboResponse pagarRecibo(String traza, PagarReciboRequest request,HeaderRequest header){
		
		logger.info(traza+" Inicio Proceso Regular Deuda ");
		PagarReciboResponse response = new PagarReciboResponse();
		BodyResponse auditResponse = new BodyResponse();
		ELKLogLegadoBean elkLegadoBean;
		
		double numeroActividadActual = 1;
		try{
			String endpointURI = Constantes.GESTOROPERACIONESMDB;
			int totalIntentos=Integer.parseInt(propertiesExterno.reintentoWF);
			request= cargarRequest(traza, request);
			elkLegadoBean = ELKUtil.obtenerELKLogLegadoBean(header.getIdTransaccion(), endpointURI, request.getCodigoPortal().toString(), numeroActividadActual);
			
            AplicacionTarjeta 	appTarjeta=obtenerDatosAplicacion(traza,request);
            
			//Registrar en OAC
			PagarReciboResponse actividadRegistrarOAC=null;
			if(buscarActividad(request.getTransaccionData().getActividadFallos(), 1)){
				logger.info(traza + "*********** 1. Registrar pago OAC ***********");		
				actividadRegistrarOAC=registrarOAC(traza, request, header,elkLegadoBean,appTarjeta);
			}
			
			//registrar Tipificacion
			PagarReciboResponse actividadTipificacion=null;
			if(buscarActividad(request.getTransaccionData().getActividadFallos(), 2)){
				logger.info(traza + "*********** 2. Registrar tipificacion ***********");
				actividadTipificacion=registrarTipi(traza, request, header,elkLegadoBean,appTarjeta);
			}
						
			//consultar Pago OAC
			Actividad actividadConsultarPago=new Actividad();
			actividadConsultarPago.setResultado(true);
			if(buscarActividad(request.getTransaccionData().getActividadFallos(), 3)){
				logger.info(traza + "*********** 3. consultar Pago OAC ***********");
				actividadConsultarPago=consultarPago(traza, request, header,elkLegadoBean,appTarjeta);
			}
		
			
			//Verificar Motor Pagos
			BodyResponse actividadVerificar=null;
			
			boolean exitoRegistrarOAC=true;
			if(buscarActividad(request.getTransaccionData().getActividadFallos(), 4)){
				logger.info(traza + "*********** 4. Verificar Motor de Pagos ***********");
				MotorVerificacionAppRequest appRequest=new MotorVerificacionAppRequest();
				appRequest.setCodigoComercio(request.getCodigoComercio());
				appRequest.setCodigoPortal(request.getCodigoPortal());
				appRequest.setLinea(request.getLinea());
				appRequest.setNumeroOrdenPortal(request.getNumeroOrdenPortal());
				appRequest.setNumeroOrdenMotor(request.getNumeroOrdenMotor());
				appRequest.setMedioPago(request.getMedioPago());

				//Verificar  en OAC
				if(null!=actividadRegistrarOAC){
					exitoRegistrarOAC=registrarOACExito(traza, actividadRegistrarOAC);
				}		
				
				//Inicio determinar ccl
				List<DetalleError> listaErroVer=new  ArrayList<>();
				listaErroVer=respuestaErrorparcial(traza, actividadTipificacion,actividadConsultarPago);						
				boolean actividadesVer=determinarIDF(listaErroVer);
				//Fin determinar ccl

				
				if(totalIntentos<=Integer.parseInt(request.getTransaccionData().getNumeroIntento()) || (exitoRegistrarOAC && actividadesVer)){
					actividadVerificar=pagarMotorPag(traza, appRequest, header,exitoRegistrarOAC
							,actividadesVer,request.getTransaccionData().getNumeroIntento(),elkLegadoBean);
					
				}else{
					actividadVerificar=new BodyResponse();
					actividadVerificar.setIdTransaccion(header.getIdTransaccion());
					actividadVerificar.setMensajeRespuesta(Constantes.PENDIENTEDES);
					
					//Registrar Transaccion Detalle
					TransaccionDetalle transac=new TransaccionDetalle();
					transac.setNumeroTransaccion( header.getIdTransaccion());
					transac.setCodigoComponente(Constantes.MOTORPAGOS);
					transac.setNumeroComponente(String.valueOf( request.getNumeroOrdenMotor()));
					transac.setEstado(Constantes.PENDIENTE);
					transac.setBody(Constantes.TEXTO_VACIO);
					transac.setIntento(Integer.parseInt(request.getTransaccionData().getNumeroIntento()));
                    insertarDetalle(traza, transac);
				}
			}
			logger.info(traza+" Procesando Verificar Motor de Pagos ");
			Actividad actividadCorreo=null;
			boolean exitoVerificar=true;

			//Enviar Correo
			if(buscarActividad(request.getTransaccionData().getActividadFallos(), 5)){	
				//Verificar  en Motor Pagos
				if(null!=actividadVerificar){
					exitoVerificar=verificarPagos(traza, actividadVerificar);
					logger.info(traza+" envio correo - Verificar OK");
				}
				//Ejecutar Envio Correo cuando Request es nuevo
				actividadCorreo=ejecutarCorreo(traza, request, header, exitoRegistrarOAC,exitoVerificar
						,actividadConsultarPago.isResultado(),elkLegadoBean);
				
			}
				
			List<DetalleError> listaErro=new  ArrayList<>();
			//cargando Respuesta Actividades
			listaErro=respuestaError(traza, actividadRegistrarOAC, actividadTipificacion, actividadCorreo,actividadVerificar,actividadConsultarPago);
			
						
			//Determinar IDF
			boolean actividades=determinarIDF(listaErro);
			
			auditResponse=cargarIdF(actividades);
			auditResponse.setIdTransaccion(header.getIdTransaccion());
			response.setAuditResponse(auditResponse);
			response.setTrazabilidadActividad(listaErro);		
			logger.info(traza+" Termino Proceso Regular Deuda ");
		}catch(Exception ex){
    		logger.error(traza + Constantes.ERRORINVOCACION + ex.toString());
    		logger.error(traza + Constantes.ERRORINVOCACION + ex.getCause());
			
			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional3);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional3);
			auditResponse.setIdTransaccion(header.getIdTransaccion());
			auditResponse.setMensajeError(ex.getMessage());
			response.setAuditResponse(auditResponse);
       	}
    	finally {
    		logger.info(traza+" Fin Proceso Regular Deuda ");
		}
		return response;
	}
    
    
    private PagarReciboRequest cargarRequest(String traza,PagarReciboRequest request){
    	if(!Constantes.EXITO.equalsIgnoreCase( request.getTransaccionData().getNumeroIntento())){
			logger.info(traza + "*********** Inicia Carga de Actividades Personalizada ***********");					
		}else{				
			logger.info(traza + "*********** Inicia Carga de Actividades por Defaul ***********");
			Proceso act1=new Proceso();
			act1.setNumero(Constantes.UNO);
			
			Proceso act2=new Proceso();
			act2.setNumero(Constantes.DOS);
			
			Proceso act3=new Proceso();
			act3.setNumero(Constantes.TRES);
			
			Proceso act4=new Proceso();
			act4.setNumero(Constantes.CUATRO);
			
			Proceso act5=new Proceso();
			act5.setNumero(Constantes.CINCO);
			
			List<Proceso> list=new ArrayList<>();
			list.add(act1);
			list.add(act2);
			list.add(act3);
			list.add(act4);
			list.add(act5);
			
			TransaccionData data=new TransaccionData();
			data.setActividadFallos(list);
			data.setNumeroIntento(Constantes.CADENA_CERO);
			data.setNumeroTransaccion(request.getTransaccionData().getNumeroTransaccion());
			request.setTransaccionData(data);
		} return request;
    }
    
  //Determinar IDF
    private boolean determinarIDF(List<DetalleError> listaErro){		
		boolean actividades=true;
		for(DetalleError er:listaErro){
			if(!Constantes.EXITOMENSAJE.equalsIgnoreCase(er.getDescripcionError())){
				actividades=false;
				break;
			}
		} return actividades;
    }
    
    //cargar IDF
    private BodyResponse cargarIdF(boolean actividades){
    	BodyResponse auditResponse = new BodyResponse();
    	if(actividades){
			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional0);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional0);
			auditResponse.setMensajeError(Constantes.TEXTO_VACIO);				
		}else{
			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional2);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional2);
			auditResponse.setMensajeError(Constantes.TEXTO_VACIO);
		} return auditResponse; 
    }
    
    //Determinar  Ejecucion de Correo x unica vez
    private  Actividad ejecutarCorreo(String traza, PagarReciboRequest request,HeaderRequest header, 
    		boolean exitoOAC,boolean exitoverificar,boolean exitoConsultarOAC,ELKLogLegadoBean elkLegadoBean){
    	Actividad actividadCorreo=null;

    	int totalIntentos=Integer.parseInt(propertiesExterno.reintentoWF);
    	logger.info(traza+" inicio envio correo ");
		if(totalIntentos <= Integer.parseInt(request.getTransaccionData().getNumeroIntento())
				|| (exitoverificar && exitoOAC&&exitoConsultarOAC)){
			actividadCorreo=ejecutarEnvioCorreo1(traza, request, header,exitoverificar,exitoOAC,exitoConsultarOAC,elkLegadoBean);
			logger.info(traza+" inicio envio correo ");
		}else{
			
			actividadCorreo=new Actividad();
			DetalleError detalleError=new DetalleError();
			detalleError.setActividad(Constantes.ENVIAR_CORREO_ELECTRONICO);
			detalleError.setDescripcionError(Constantes.PENDIENTEDES);
			actividadCorreo.setDetalleError(detalleError);
			
			//Registrar Transaccion Detalle
			TransaccionDetalle transac=new TransaccionDetalle();
			transac.setNumeroTransaccion( header.getIdTransaccion());
			transac.setCodigoComponente(Constantes.ENVIOCORREOSB);
			transac.setNumeroComponente(  header.getIdTransaccion());
			transac.setEstado(Constantes.PENDIENTE);
			transac.setBody(Constantes.TEXTO_VACIO);
			transac.setIntento(Integer.parseInt(request.getTransaccionData().getNumeroIntento()));
            insertarDetalle(traza, transac);
            logger.info(traza+" inicio envio correo ");
		}
		return actividadCorreo;
    }
    private Actividad ejecutarEnvioCorreo1(String traza, PagarReciboRequest request,HeaderRequest header,boolean exitoverificar
    		,boolean exitoOAC,boolean exitoConsultarOAC,ELKLogLegadoBean elkLegadoBean){
    	Actividad actividadCorreo=new Actividad();
    	//Enviar Correo
    	DetalleError detalleEnvioMail= new DetalleError();
    	String html=Constantes.TEXTO_VACIO;
    	try{
    		if(exitoverificar &&  exitoOAC &&exitoConsultarOAC){
    			//Correo para  escenarios  de pagos completos. 
    			html = propertiesExterno.mensajeSB ;		
    			actividadCorreo=enviarCorreo(traza,request,header,formarCuepoExitoSBAPP(traza, request,html),elkLegadoBean);		
    		}else{
    			//Correo para  escenarios  de pagos incompletos. 
    			String flagEnviarNoExito=propertiesExterno.flagNoExito;
    			if(Constantes.CADENA_UNO.equalsIgnoreCase(flagEnviarNoExito)){
    				html = propertiesExterno.mensajeNoExito ;		
        			actividadCorreo=enviarCorreo(traza, request,header,formarCuepoExitoSBAPP(traza, request,html),elkLegadoBean);	
    			}else{
    				logger.info(traza+" No se  envia Correo Electronico");
    				DetalleError detalleError=new DetalleError();
    				detalleError.setActividad(Constantes.ENVIAR_CORREO_ELECTRONICO);
    				detalleError.setDescripcionError(Constantes.EXITOMENSAJE);
    				actividadCorreo.setDetalleError(detalleError);
    				logger.info(traza+" Se  deja registro de envio Correo OK.");
    			}
    			
    		}
    	}catch(Exception e){
    		logger.error(traza+Constantes.ERRORCORREO+ e + ".");
 			actividadCorreo.setResultado(false);
 			detalleEnvioMail.setActividad(Constantes.ENVIAR_CORREO_ELECTRONICO);
			detalleEnvioMail.setDescripcionError(Constantes.ERROR);
			actividadCorreo.setDetalleError(detalleEnvioMail);
    	}
		 return actividadCorreo;
    }
    
    //cargando Respuesta Actividades
    private List<DetalleError>  respuestaError(String traza,PagarReciboResponse actividadRegistrarOAC,PagarReciboResponse actividadTipificacion,
    		Actividad actividadCorreo,BodyResponse resp,Actividad actConsultaOAC){
    	List<DetalleError> listaError=new ArrayList<>();
    	try{
    		if(null != actividadRegistrarOAC 
    				&& null != actividadRegistrarOAC.getTrazabilidadActividad() 
    				&& null!=actividadRegistrarOAC.getTrazabilidadActividad().get(0)){
    			listaError.add(actividadRegistrarOAC.getTrazabilidadActividad().get(0));
    		}
    		if(null != actividadTipificacion
    				&& null!=actividadTipificacion.getTrazabilidadActividad()
    				&& null!=actividadTipificacion.getTrazabilidadActividad().get(0)){
    			listaError.add(actividadTipificacion.getTrazabilidadActividad().get(0));
    		} 
    		if(null!=actConsultaOAC
    				&& null!= actConsultaOAC.getDetalleError()){
    			listaError.add(actConsultaOAC.getDetalleError());
    		}
    		if(null != resp){
    			DetalleError detalle=new DetalleError();
    			detalle.setActividad("verificar Pagos");
    			detalle.setDescripcionError(resp.getMensajeRespuesta());
    			
    			listaError.add(detalle);
    		}
    		if(null!=actividadCorreo
    				&& null!= actividadCorreo.getDetalleError()){
    			listaError.add(actividadCorreo.getDetalleError());
    		}
		}catch(Exception e){
			logger.error(traza + "Error Cargando Respuesta "+ e.getCause());
		} return listaError;
    }
    
  //cargando Respuesta ccl - tipificacion
    private List<DetalleError>  respuestaErrorparcial(String traza,PagarReciboResponse actividadTipificacion,Actividad actconsultaPagoOAC){
    	List<DetalleError> listaError=new ArrayList<>();
    	try{
    		if(null!=actconsultaPagoOAC
    				&& null!= actconsultaPagoOAC.getDetalleError()){
    			listaError.add(actconsultaPagoOAC.getDetalleError());
    		}
    		if(null != actividadTipificacion
    				&& null!=actividadTipificacion.getTrazabilidadActividad()
    				&& null!=actividadTipificacion.getTrazabilidadActividad().get(0)){
    			listaError.add(actividadTipificacion.getTrazabilidadActividad().get(0));
    		}  		
		}catch(Exception e){
			logger.error(traza + "Error Cargando Respuesta "+ e.getCause());
		} return listaError;
    }
    
    //Determinar estado de la  Orden en OAC
    private boolean registrarOACExito(String traza,PagarReciboResponse actividadRegistrarOAC){
    	boolean exito=false;
    	try{
    		if(null != actividadRegistrarOAC 
    				&& null != actividadRegistrarOAC.getTrazabilidadActividad() 
    				&& null !=actividadRegistrarOAC.getTrazabilidadActividad().get(0)
    				&& Constantes.EXITOMENSAJE.equalsIgnoreCase(actividadRegistrarOAC.getTrazabilidadActividad().get(0).getDescripcionError())){
    			exito=true;
    		}
		}catch(Exception e){
			logger.error(traza + "Error Cargando Traza "+ e.getCause());
		} return exito;
    }
    
    //Determinar estado de la  Orden en VerificarPagos
    private boolean verificarPagos(String traza,BodyResponse actividadVerificar){
    	boolean exito=false;
    	try{
    		if( Constantes.EXITOMENSAJE.equalsIgnoreCase(actividadVerificar.getMensajeRespuesta())){
    			exito=true;
    		}
		}catch(Exception e){
			logger.error(traza + "Error Cargando Traza "+ e.getCause());
		} return exito;
    }
    
    //Inicio registrar  En OAC
    public PagarReciboResponse registrarOAC(String traza, PagarReciboRequest request,HeaderRequest header
    		,ELKLogLegadoBean elkLegadoBean,AplicacionTarjeta appTarjeta){
		
		PagarReciboResponse response = new PagarReciboResponse();
		boolean isErrorVisa = header.isErrorVisa();
		String mensaje = Constantes.EXITOMENSAJE;
		DetalleError detalleRegistroPago = new DetalleError();
		List<DetalleError> listaError = new ArrayList<>();
		BodyResponse auditResponse = new BodyResponse();
		try{
			
			if(!isErrorVisa){
							
				Actividad actipagarOAC=registrarReciboOAC(traza, request, header,elkLegadoBean,appTarjeta);
				detalleRegistroPago=actipagarOAC.getDetalleError();					
	
				//Registrar Response en DB
				logger.info(traza+Constantes.LOG_FIN_ACTIVIDAD + Constantes.REGISTRAR_PAGO);
			} else {
				
				detalleRegistroPago.setActividad(Constantes.REGISTRAR_PAGO);
				detalleRegistroPago.setDescripcionError("No se realiza esta actividad");
				detalleRegistroPago.setMensajeError("Error");			
			}

			listaError.add(detalleRegistroPago);				

			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional0);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional0);
			auditResponse.setMensajeError(Constantes.TEXTO_VACIO);
			auditResponse.setIdTransaccion(header.getIdTransaccion());
			response.setAuditResponse(auditResponse);
			response.setTrazabilidadActividad(listaError);		
		}catch(Exception ex){
    		logger.error(mensaje + Constantes.ERRORINVOCACION + ex.toString());
			
			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional0);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional0);
			auditResponse.setIdTransaccion(header.getIdTransaccion());
			auditResponse.setMensajeError(propertiesExterno.msjErrorFuncional0);
			response.setAuditResponse(auditResponse);
			
       	}finally{
       		logger.info(mensaje + Constantes.RESUMEN+ Constantes.REGISTRAR_PAGO + GestorOperacionesUtil.printPrettyJSONString(response));
       	}
		return response;
	}

    //buscar Actividad  a ejcutar
	public static boolean buscarActividad(List<Proceso> lista,int act){
		boolean existe=false;
		for(Proceso actividad:lista){
			if(actividad.getNumero() == act){
				existe=true;
			}
		}
		return existe;
	}
	
	private Actividad registrarReciboOAC(String traza, PagarReciboRequest request,HeaderRequest header
			,ELKLogLegadoBean elkLegadoBean,AplicacionTarjeta appTarjeta){
		logger.info(traza+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.REGISTRAR_PAGO);
		String mensaje = Constantes.EXITOMENSAJE;
		DetalleError detalleRegistroPago = new DetalleError();
		Actividad actividad =new Actividad();
		actividad.setResultado(true);
		try {
			int contador = 0;
			ListaCustomer listaCustomer=new ListaCustomer();
			List<Customer> lista=new ArrayList<>();
			
			for (DetalleDocumento detalleDocumen : request.getListaDetalleDocumentos()) {
				contador++;
				
				//validar si es  reintento
				if(Constantes.CADENA_CERO.equalsIgnoreCase(request.getTransaccionData().getNumeroIntento())){
					listaCustomer.setIngreso(true);
				}else{
					//validar  si customer se encuentra procesada					
					listaCustomer=buscarCustomer(traza,header.getIdTransaccion(),detalleDocumen.getIdentificador(),lista);
					lista=listaCustomer.getLista();
				}
				
				if(listaCustomer.isIngreso()){
					
					PagosDetalle pagostDet=new PagosDetalle();
					pagostDet.setCodigoOperacion(request.getCodigoOperacion());
					pagostDet.setIdTransaccion(header.getIdTransaccion());
					pagostDet.setContador(contador);
					pagostDet.setMoneda(request.getMoneda());
					pagostDet.setNumeroComercio(request.getTipoTarjeta());
					CrearPagoRequest crearPagoRequest = this.createRequestPagoBD( detalleDocumen,pagostDet,appTarjeta);
					
					CrearPagoRespo crearPagoResponse = realizarPagoWS(traza, crearPagoRequest,header.getIdTransaccion(),
							request.getTransaccionData().getNumeroIntento(),elkLegadoBean);

					if((null != crearPagoResponse.getAudit() 
							&& crearPagoResponse.getAudit().value.getErrorCode().equalsIgnoreCase(String.valueOf(Constantes.CERO)))){
						logger.info(traza+" Se realizo con exito el registro del pago en OAC "+detalleDocumen.getIdentificador());
					}else{
						actividad.setResultado(false); 
						mensaje = Constantes.ERROR;	
						logger.info(traza+" Respuesta OAC: "+crearPagoResponse.getAudit().value.getErrorMsg());
						logger.info(traza+" No se pudo registrar el pago en OAC "+detalleDocumen.getIdentificador());
						
					}
				}
			}
		} catch (Exception e) {
			logger.error(traza+Constantes.ERROREXCEPTION + e.getMessage() + "] ", e);
			mensaje = Constantes.ERROR;
			actividad.setResultado(false);
		} finally {
			detalleRegistroPago.setActividad(Constantes.REGISTRAR_PAGO);
			detalleRegistroPago.setDescripcionError(mensaje);
			actividad.setDetalleError(detalleRegistroPago);
			logger.info(traza+Constantes.LOG_FIN_ACTIVIDAD + Constantes.REGISTRAR_PAGO);
		}
		return actividad;
	}
	
	//RealizarPagoWS con Reintentos
	public  CrearPagoRespo realizarPagoWS(String traza,CrearPagoRequest request,String transaccion,String numerointent,ELKLogLegadoBean elkLegadoBean){
		CrearPagoRespo crearPagoResponse=null;
		TransaccionResponse responseTransacc=new TransaccionResponse();
		try{
			int reintento=Integer.parseInt(propertiesExterno.reintentoWS);
			int inicio=0;
			
			String idt3=propertiesExterno.codError3;
			String idt4=propertiesExterno.codError4;
			//REGISTRAR REQUEST
			insertarRequest(traza, transaccion,Constantes.REGISTRAROAC, 
			    		Constantes.OACFLUJO, GestorOperacionesUtil.printPrettyJSONString(request),numerointent);
			
			while(inicio<=reintento){				
				if(inicio>0){
					logger.info(traza + Constantes.INICIAREINTENT+inicio);
				}
				crearPagoResponse = transaccionPagosProxy.realizarPago( request,traza,elkLegadoBean);
				logger.info(traza + Constantes.RESPONSE +  GestorOperacionesUtil.printPrettyJSONString(crearPagoResponse));
				
				if(idt3.equals(crearPagoResponse.getAudit().value.getErrorCode())
						  ||idt4.equals(crearPagoResponse.getAudit().value.getErrorCode())){
					inicio++;
				}else{
					break;
				}
			}
			//Registrar Transaccion Detalle
			
			TransaccionDetalle transac=new TransaccionDetalle();
			transac.setNumeroTransaccion(transaccion);
			transac.setCodigoComponente(Constantes.REGISTRAROAC);
			transac.setNumeroComponente(request.getpNroOperacion());
			
			if(null != crearPagoResponse.getAudit() 
					&& (crearPagoResponse.getAudit().value.getErrorCode().equalsIgnoreCase(String.valueOf(Constantes.CERO)))){		
				transac.setEstado(Constantes.EJECUTADO);
			}else{								
				transac.setEstado(Constantes.FALLO);		
			}
			
			insertarResponse(traza, transaccion, Constantes.REGISTRAROAC,
		    		Constantes.OACFLUJO, GestorOperacionesUtil.printPrettyJSONString(crearPagoResponse));	
			
			transac.setBody( GestorOperacionesUtil.printPrettyJSONString(crearPagoResponse));
			transac.setIntento(Integer.parseInt(numerointent));
			responseTransacc=insertarDetalle(traza, transac);
			
			//Actualizar Recibo
			TransaccionDetalle actualiza=new TransaccionDetalle();
            actualiza.setNumeroTransaccion(transaccion);
            actualiza.setCustomerId(request.getpDatoIdentific());
            actualiza.setNumeroTransaccionDetale(null!=responseTransacc?responseTransacc.getNumeroTransaccionDetale():null);        
            actualizarRecibo(traza, actualiza);
			
		}catch(Exception e){
			logger.error(traza+Constantes.ERROREXCEPTION + e.getMessage() + "] ", e);
		} return crearPagoResponse;
	}
	
	
    public PagarReciboResponse registrarTipi(String traza, PagarReciboRequest request,HeaderRequest header,
    		ELKLogLegadoBean elkLegadoBean,AplicacionTarjeta appTarjeta){

		PagarReciboResponse response = new PagarReciboResponse();
		boolean isErrorVisa = header.isErrorVisa();
		String fechaPago = GestorOperacionesUtil.obtenerFechaString(Constantes.TEXTO_VACIO, null);
			
		String mensaje = Constantes.EXITOMENSAJE;
		DetalleError detalleRegistroTipificacion= new DetalleError();
		List<DetalleError> listaError = new ArrayList<>();
		BodyResponse auditResponse = new BodyResponse();
		try{
			
			logger.info("fechaPago: {}"+fechaPago);
			
			if(!isErrorVisa){			
				Actividad actividadTipificar=registrarTipificacion(traza, request, header,fechaPago
						,appTarjeta,elkLegadoBean);
				detalleRegistroTipificacion=actividadTipificar.getDetalleError();			
	
				logger.info(traza+Constantes.LOG_FIN_ACTIVIDAD + Constantes.REGISTRAR_TIPIFICACION);
			} else {
				
				detalleRegistroTipificacion.setActividad(Constantes.REGISTRAR_TIPIFICACION);
				detalleRegistroTipificacion.setDescripcionError("No se realiza esta actividad");
				detalleRegistroTipificacion.setMensajeError("Error");
				
			}
			listaError.add(detalleRegistroTipificacion);		
		}catch(Exception ex){
    		logger.error(mensaje + Constantes.ERRORINVOCACION + ex.toString());		
    		logger.error(mensaje + "Se deja Evidencia de Error en Log :"+ ex.toString());	
       	}
    	finally {
			auditResponse.setCodigoRespuesta(propertiesExterno.codErrorFuncional0);
			auditResponse.setMensajeRespuesta(propertiesExterno.msjErrorFuncional0);
			auditResponse.setMensajeError(Constantes.TEXTO_VACIO);
			auditResponse.setIdTransaccion(header.getIdTransaccion());
			response.setAuditResponse(auditResponse);
    		response.setTrazabilidadActividad(listaError);
			logger.info(mensaje + Constantes.RESUMEN+ Constantes.REGISTRAR_TIPIFICACION + GestorOperacionesUtil.printPrettyJSONString(response));			
		}
		return response;
	}
	
	private Actividad registrarTipificacion(String traza, PagarReciboRequest request,HeaderRequest header,String fechaPago,AplicacionTarjeta appTarjeta
			,ELKLogLegadoBean elkLegadoBean){
		logger.info(traza+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.REGISTRAR_TIPIFICACION);
		DetalleError detalleRegistroTipificacion= new DetalleError();
		String mensaje = Constantes.EXITOMENSAJE;
		String descripcion = Constantes.TEXTO_VACIO;
		Actividad actividad =new Actividad();
		actividad.setResultado(true);
		try {
			
			for (DetalleDocumento detalleDocumento : request.getListaDetalleDocumentos()) {
				CrearInteraccionRequest crearInteraccionRequest = this.crearRequestInteraccion(detalleDocumento,
																	header.getIdTransaccion(), 
																	fechaPago, 
																	request,
																	appTarjeta);
				logger.info(traza + " REQUEST: " +  GestorOperacionesUtil.printPrettyJSONString(crearInteraccionRequest));
				AuditType auditType=realizarInterracionWS(traza, crearInteraccionRequest,Constantes.CIEN
						                                  ,header.getIdTransaccion(),request.getTransaccionData().getNumeroIntento(),elkLegadoBean);

				if (auditType.getErrorCode().equals(String.valueOf(Constantes.CERO))) {
					logger.info(traza + " Se registro con exito la tipificacion, identificado: {}"+ detalleDocumento.getIdentificador());
				} else {
					if(propertiesExterno.codError3.equals(auditType.getErrorCode())
							  ||propertiesExterno.codError4.equals(auditType.getErrorCode())){
						actividad.setResultado(false); 
						logger.info(traza+ " ERROR: No se pudo registrar la interaccion, identificado: {}"+ detalleDocumento.getIdentificador());
					}else{
						descripcion = auditType.getErrorMsg();
						logger.info(traza+"Respuesta : "+descripcion);
					}
				}
			}
		} catch (Exception e) {
			logger.error(traza+Constantes.ERROREXCEPTION + e.getMessage() + "] ", e);
			descripcion = e.getMessage();
			actividad.setResultado(false);
		} finally {
			detalleRegistroTipificacion.setActividad(Constantes.REGISTRAR_TIPIFICACION);
			detalleRegistroTipificacion.setDescripcionError(mensaje);
			actividad.setDetalleError(detalleRegistroTipificacion);
			logger.info(traza+Constantes.LOG_FIN_ACTIVIDAD + Constantes.REGISTRAR_TIPIFICACION);
		}
		return actividad;
	}
	
	//InterracionWS con Reintentos
	public  AuditType realizarInterracionWS(String traza,CrearInteraccionRequest request,String flagProceso,String transaccion,
			String numeroIntento,ELKLogLegadoBean elkLegadoBean){
		AuditType auditType=new AuditType();
		try{
			int reintento=Integer.parseInt(propertiesExterno.reintentoWS);
			int inicio=0;
			
			String idt3=propertiesExterno.codError3;
			String idt4=propertiesExterno.codError4;
			
			//REGISTRAR REQUEST
			insertarRequest(traza, transaccion,Constantes.REGISTRARTIPIFICACION, 
		    		Constantes.TIPIFICACIONFLUJO, GestorOperacionesUtil.printPrettyJSONString(request),numeroIntento);
			request.setFlagProceso(propertiesExterno.transaccionInteraccionFlagProceso);
			while(inicio<=reintento){				
				if(inicio>0){
					logger.info(traza + Constantes.INICIAREINTENT+inicio);
				}
				auditType = transaccionInteraccionesAsyn.crearInteraccion(traza,request,flagProceso,elkLegadoBean);
				logger.info(traza + Constantes.RESPONSE +  GestorOperacionesUtil.printPrettyJSONString(auditType));
				
				if(idt3.equals(auditType.getErrorCode())
						  ||idt4.equals(auditType.getErrorCode())){
					inicio++;
				}else{
					break;
				}
			}
			

	        //Registrar Transaccion Detalle
			
			TransaccionDetalle transac=new TransaccionDetalle();
			transac.setNumeroTransaccion(transaccion);
			transac.setCodigoComponente(Constantes.REGISTRARTIPIFICACION);
			transac.setNumeroComponente(request.getInteraccionType().getTelefono());
			
			if(!idt3.equals(auditType.getErrorCode())
					  ||!idt4.equals(auditType.getErrorCode())){
				insertarResponse(traza, transaccion, Constantes.REGISTRARTIPIFICACION,
			    		Constantes.TIPIFICACIONFLUJO, GestorOperacionesUtil.printPrettyJSONString(auditType));
				
				transac.setEstado(Constantes.EJECUTADO);
                
			}else{
				transac.setEstado(Constantes.FALLO);
			}
			transac.setBody( GestorOperacionesUtil.printPrettyJSONString(auditType));
			transac.setIntento(Integer.parseInt(numeroIntento));
			insertarDetalle(traza, transac);
		}catch(Exception e){
			logger.error(traza+Constantes.ERROREXCEPTION + e.getMessage() + "] ", e);
		} return auditType;
	}
	
	private CrearPagoRequest createRequestPagoBD( DetalleDocumento request,PagosDetalle pagostDet,AplicacionTarjeta appTarjeta) {
		CrearPagoRequest crearPagoRequest = new CrearPagoRequest();
		try{
			HashMap<String, String> listaTipoRecibo = new HashMap<>();
	
			Date fechaTransaccionPago = new Date();
			
			XMLGregorianCalendar fechaActual = GestorOperacionesUtil.obtenerFechaXMLGregorianCalendar(fechaTransaccionPago);
			
			Holder<String> pExtorno = new Holder<>();
			pExtorno.value = propertiesExterno.transaccionPagoRequestCodigoExtorno;
			
			for (Documento documentos : request.getListaDocumentos()) {
				listaTipoRecibo.put(documentos.getTipo(), documentos.getTipo());
			}
			
			if(Constantes.EXITO.equalsIgnoreCase(appTarjeta.getCodigoRespuesta())) {
				crearPagoRequest.setTxId(pagostDet.getIdTransaccion());
				crearPagoRequest.setpCodAplicacion(appTarjeta.getListTarjeta().get(0).getCodigoCanal());
				crearPagoRequest.setpExtorno(pExtorno);
				crearPagoRequest.setpCodBanco(appTarjeta.getListTarjeta().get(0).getCodigoTarjeta());
				crearPagoRequest.setpCodMoneda( ClaroUtil.obtenerCodigoMoneda(propertiesExterno.transaccionPagoRequestMon, pagostDet.getMoneda()));
				crearPagoRequest.setpTipoIdentific(request.getTipoIdentificador());
				crearPagoRequest.setpDatoIdentific(request.getIdentificador());
				crearPagoRequest.setpFechaHora(fechaActual);
				crearPagoRequest.setpTrace(pagostDet.getCodigoOperacion()+Constantes.STRING_GUION_ARRIBA+pagostDet.getContador());
				crearPagoRequest.setpNroOperacion(pagostDet.getCodigoOperacion()+Constantes.STRING_GUION_ARRIBA+pagostDet.getContador());
				crearPagoRequest.setpMedioPago(appTarjeta.getListTarjeta().get(0).getMedioPago());
				crearPagoRequest.setpNombreComercio(Constantes.PAGOSRECIBOS);
				crearPagoRequest.setpNroComercio(pagostDet.getNumeroComercio());
				crearPagoRequest.setpCodCanal(appTarjeta.getListTarjeta().get(0).getCodigoCanalOAC());
				
				BigDecimal bd1 = BigDecimal.valueOf(request.getMonto());

				crearPagoRequest.setpPagoTotal(bd1);
		
				crearPagoRequest.setpDetDocumentos(cargarPago(listaTipoRecibo, request));
			}
			
		}catch(Exception e){
			logger.error(pagostDet.getIdTransaccion()+Constantes.ERROR+e);
        }
		return crearPagoRequest;
	}
	
	
	
	private CrearPagoDetServiciosReqType cargarPago(HashMap<String, String> listaTipoRecibo,DetalleDocumento request){
		
		BigDecimal montoPagadoTipoRecibo = null;
		CrearPagoDetServicioReqType crearPagoDetServicioReqType = null;
		CrearPagoDetDocumentoReqType crearPagoDetDocumentoReqType = null;
		List<CrearPagoDetDocumentoReqType> listaCrearPagoDetDocumentoReqType = null;
		CrearPagoDetDocumentosReqType crearPagoDetDocumentosReqType = null;
		CrearPagoDetServiciosReqType crearPagoDetServiciosReqType = new CrearPagoDetServiciosReqType();
		
		for (Map.Entry<String, String> tipoRecibo : listaTipoRecibo.entrySet()) {
			montoPagadoTipoRecibo = BigDecimal.ZERO;
			crearPagoDetServicioReqType = new CrearPagoDetServicioReqType();
			listaCrearPagoDetDocumentoReqType = new ArrayList<>();
			crearPagoDetDocumentosReqType = new CrearPagoDetDocumentosReqType();
			for (Documento documento : request.getListaDocumentos()) {
				if(tipoRecibo.getValue().equals(documento.getTipo())){
					BigDecimal montod = BigDecimal.valueOf(documento.getMonto());
					crearPagoDetDocumentoReqType = new CrearPagoDetDocumentoReqType();
					crearPagoDetDocumentoReqType.setPTipoServicio(documento.getTipo());
					crearPagoDetDocumentoReqType.setPNumeroDoc(documento.getNumero());
					crearPagoDetDocumentoReqType.setPMontoPagado(montod);
					listaCrearPagoDetDocumentoReqType.add(crearPagoDetDocumentoReqType);
					montoPagadoTipoRecibo = montoPagadoTipoRecibo.add(montod);
				}
			}
			crearPagoDetDocumentosReqType.getPDetDocumento().addAll(listaCrearPagoDetDocumentoReqType);
			
			crearPagoDetServicioReqType.setPCodTipoServicio(tipoRecibo.getValue());
			crearPagoDetServicioReqType.setPEstadoDeudor( propertiesExterno.transaccionPagoRequestEstadoDeudor );
			crearPagoDetServicioReqType.setPNroDocs(new BigDecimal(listaCrearPagoDetDocumentoReqType.size()));
			crearPagoDetServicioReqType.setPMontoPagado(montoPagadoTipoRecibo);
			crearPagoDetServicioReqType.setPDetalleDocs(crearPagoDetDocumentosReqType);
			
			crearPagoDetServiciosReqType.getPDetServicio().add(crearPagoDetServicioReqType);
			
		}
		return crearPagoDetServiciosReqType;
		
	}
	
	private CrearInteraccionRequest crearRequestInteraccion(DetalleDocumento request, String idTransaccion, String fechaPago,
			PagarReciboRequest pagarReciRequest,AplicacionTarjeta appTarjeta) {

		int contador = 0;
		StringBuilder numeroRecibo =new StringBuilder( Constantes.TEXTO_VACIO);

		if (request.getListaDocumentos().size() == 1) {
			numeroRecibo =numeroRecibo.append(request.getListaDocumentos().get(0).getNumero());
		} else {
			for (Documento documento : request.getListaDocumentos()) {
				contador++;
				numeroRecibo =numeroRecibo.append(documento.getNumero());
				if (contador != request.getListaDocumentos().size()) {
					numeroRecibo =numeroRecibo.append( Constantes.CHAR_COMA);
				}
			}
		}
		
		String notas =GestorOperacionesUtil.convertProperties( propertiesExterno.transaccionInteraccionNota);
		notas = notas.replace(Constantes.BUSCAR_T_APLICACION, appTarjeta.getListTarjeta().get(0).getDescripcion());
		notas = notas.replace(Constantes.BUSCAR_T_NUMERO_TARJETA, pagarReciRequest.getNumeroTarjeta());
		notas = notas.replace(Constantes.BUSCAR_T_TIPO_TARJETA, pagarReciRequest.getTipoTarjeta());
		notas = notas.replace(Constantes.BUSCAR_T_FECHA_HORA, fechaPago);
		notas = notas.replace(Constantes.BUSCAR_T_NUMERO_OPERACION, pagarReciRequest.getCodigoOperacion());
		notas = notas.replace(Constantes.BUSCAR_T_MONTO, String.valueOf(request.getMonto()));
		notas = notas.replace(Constantes.BUSCAR_T_NUMERO_CUENTA, "");
		notas = notas.replace(Constantes.BUSCAR_T_NUMERO_RECIBOS, numeroRecibo);
		notas = notas.replace(Constantes.BUSCAR_T_AUTORIZACION_BANCARIA, pagarReciRequest.getNumeroAutorizacion());
		notas = notas.replace(Constantes.BUSCAR_T_CUSTOMER, request.getIdentificador());

		CrearInteraccionRequest crearInteraccionRequest = new CrearInteraccionRequest();
		InteraccionType interaccionType = new InteraccionType();  
		interaccionType.setClase(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionClase));
		interaccionType.setCodigoEmpleado(appTarjeta.getListTarjeta().get(0).getEmpleadoTipi());
		interaccionType.setCodigoSistema(appTarjeta.getListTarjeta().get(0).getEmpleadoTipi());
		interaccionType.setCuenta(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionCuenta));
		interaccionType.setFlagCaso(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionFlagCaso) );
		interaccionType.setHechoEnUno(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionHechoUno) );
		interaccionType.setMetodoContacto(appTarjeta.getListTarjeta().get(0).getMetodoTipi());
		interaccionType.setNotas(notas);
		interaccionType.setObjId(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionObjid) );
		interaccionType.setSiteObjId(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionSiteObjId) );
		interaccionType.setSubClase(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionSubClase) );
		
		interaccionType.setTextResultado(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionTextResultado) );
		
		//definir telefono o customer
		if(Constantes.TIPOIDENTIFICADORCUSTOMER.equalsIgnoreCase(request.getTipoIdentificador())) {
			interaccionType.setTelefono(Constantes.ADDCUSTOMER+request.getIdentificador());
		}else {
			interaccionType.setTelefono(request.getIdentificador());
		}
		
		//definir tipo o tecnologia
		if(null==request.getTipoTecnologia()||request.getTipoTecnologia().isEmpty()) {
			interaccionType.setTipo(GestorOperacionesUtil
					.convertProperties(propertiesExterno.transaccionInteraccionTipo) );
		}else {
			interaccionType.setTipo(GestorOperacionesUtil
					.convertProperties(request.getTipoTecnologia()) );
		}

		interaccionType.setTipoInteraccion(GestorOperacionesUtil
				.convertProperties(propertiesExterno.transaccionInteraccionTipoInteraccion) );

		crearInteraccionRequest.setIdTransaccion(idTransaccion);
		crearInteraccionRequest.setInteraccionType(interaccionType);

		return crearInteraccionRequest;
	}
	
	
	//pagar Motor de Pagos
    public BodyResponse pagarMotorPag(String trazabilidad,MotorVerificacionAppRequest request,HeaderRequest header
    		,boolean estado,boolean estadoCCLTipi,String numeroIntento,ELKLogLegadoBean elkLegadoBean){
		
		BodyResponse response = new BodyResponse();	
			try{
				logger.info(trazabilidad+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.VERIFICARPAGO);
				String idt3=propertiesExterno.codError3;
				String idt4=propertiesExterno.codError4;
				//estado 9: OK      - estado 12: Error en OAC      - estado 16: Error en CCL-Tipificacion
				if(estado && estadoCCLTipi){
					request.setEstadoOrden(Constantes.NUEVE);
				}else{
					if(!estadoCCLTipi){
						request.setEstadoOrden(Constantes.DIECISIETE);
					}else{
						request.setEstadoOrden(Constantes.DOCE);
					}				
				}
				
				String requestEncrip = GestorOperacionesUtil.printPrettyJSONString(request);
				logger.info(trazabilidad+" Request : "+GestorOperacionesUtil.printPrettyJSONString(request));
				RSAMotorPagos rsaMotorPagos = new RSAMotorPagos();
				RSAPublicKey publicKey = rsaMotorPagos.getPublicKey(Constantes.KEY_REQUEST_ENCRYPT_APPS);
				String requestEncriptado=rsaMotorPagos.encryptJson(requestEncrip, publicKey);
				logger.info("Request encriptado:\n" + requestEncriptado);

				VerificarAppResponse verificarResponse=verificarPagosWS(trazabilidad, requestEncriptado, header,numeroIntento,request.getNumeroOrdenMotor(),elkLegadoBean);
				
				if(!idt3.equals(verificarResponse.getCodigoRespuesta()) && !idt4.equals(verificarResponse.getCodigoRespuesta())){
					
					response.setMensajeRespuesta(Constantes.EXITOMENSAJE);
				}else{
					response.setMensajeRespuesta(Constantes.ERROR);
				}
				
				response.setCodigoRespuesta(verificarResponse.getCodigoRespuesta());
				
								
			}catch(Exception e){
				logger.error(trazabilidad+Constantes.ERROR+e);
				response.setCodigoRespuesta(String.valueOf(Constantes.UNO));
				response.setMensajeError(Constantes.ERROR);
				response.setMensajeRespuesta(Constantes.ERROR);
			}finally{
				logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(response));
				logger.info(trazabilidad+Constantes.LOG_FIN_ACTIVIDAD + Constantes.VERIFICARPAGO);
			}
			
			return response;
	}
    
	public VerificarAppResponse verificarPagosWS(String trazabilidad,String req,HeaderRequest header,String numeroIntento,Long numeroOrden,ELKLogLegadoBean elkLegadoBean){
		
		VerificarAppResponse response = new VerificarAppResponse();	
			try{
				logger.info(trazabilidad+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.VERIFICARPAGO);
				
				VerificarAppRequest request=new VerificarAppRequest();
				request.setRequest(req);
				
			    int reintento=Integer.parseInt(propertiesExterno.reintentoWS);
				int inicio=0;				
				 insertarRequest(trazabilidad, header.getIdTransaccion(),Constantes.MOTORPAGOS, 
				    		Constantes.VERIFICARFLUJO, GestorOperacionesUtil.printPrettyJSONString(request),numeroIntento);
				 
				while(inicio<=reintento){	
					if(inicio>0){
						logger.info(trazabilidad + Constantes.INICIAREINTENT+inicio);
					}
					response = motorPagos.verificarPago(trazabilidad, header, request,elkLegadoBean);
					logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(response));
					
					if(null!=response.getCodigoRespuesta()){
						inicio++;
					}else{
						break;
					}
				}	
				
				//Registrar Transaccion Detalle
				TransaccionDetalle transac=new TransaccionDetalle();
				transac.setNumeroTransaccion( header.getIdTransaccion());
				transac.setCodigoComponente(Constantes.MOTORPAGOS);
				transac.setNumeroComponente( String.valueOf(numeroOrden));
				transac.setBody(GestorOperacionesUtil.printPrettyJSONString(response));
				if(response.getCodigoRespuesta().equals(Constantes.EXITO)){
					insertarResponse(trazabilidad, header.getIdTransaccion(), Constantes.MOTORPAGOS, 
				    		Constantes.VERIFICARFLUJO, GestorOperacionesUtil.printPrettyJSONString(response));				
            		
					transac.setEstado(Constantes.EJECUTADO);
				}else{
					transac.setEstado(Constantes.FALLO);
					response.setCodigoRespuesta(response.getCodigoRespuesta());
					response.setMensajeRespuesta(response.getMensajeRespuesta());
					response.setMensajeError(response.getMensajeError());
				}
				transac.setIntento(Integer.parseInt(numeroIntento));
				 insertarDetalle(trazabilidad, transac);
				 
				 
				 
			}catch(EJBException ex){
				logger.error(trazabilidad+Constantes.ERROR+ex);
				response.setCodigoRespuesta(String.valueOf(Constantes.UNO));
				response.setMensajeError(ex.getMessage());
	       	}catch(Exception e){
				logger.error(trazabilidad+Constantes.ERROR+e);
				response.setCodigoRespuesta(String.valueOf(Constantes.UNO));
				response.setMensajeError(e.getMessage());
			}finally{
				logger.info(trazabilidad+Constantes.LOG_FIN_ACTIVIDAD + Constantes.VERIFICARPAGO);
			}		
			return response;
	}
	
  //ConsultarPago	
  public BodyResponse consultarPagoOAC(String trazabilidad,PagarReciboRequest request
		  ,ELKLogLegadoBean elkLegadoBean,ConsultaPagoOACRequest requestConsul){
		
	 BodyResponse response = new BodyResponse();	
			try{
				logger.info(trazabilidad+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.CONSULTAR_PAGO);
				
			    int reintento=Integer.parseInt(propertiesExterno.reintentoWS);
				int inicio=0;		
				 
				while(inicio<=reintento){	
					if(inicio>0){
						logger.info(trazabilidad + Constantes.INICIAREINTENT+inicio);
					}
					response=recaudacionesPagos.consultarPago(trazabilidad, requestConsul, elkLegadoBean);
					logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(response));
					
					if(null!=response.getCodigoRespuesta()){
						inicio++;
					}else{
						break;
					}
				}	
				
				//Registrar Transaccion Detalle
				TransaccionDetalle transac=new TransaccionDetalle();
				transac.setNumeroTransaccion( request.getTransaccionData().getNumeroTransaccion());
				transac.setCodigoComponente(Constantes.CONSULTAROAC);
				transac.setNumeroComponente( String.valueOf(request.getNumeroOrdenMotor()));
				transac.setBody(GestorOperacionesUtil.printPrettyJSONString(response));
				if(response.getCodigoRespuesta().equals(Constantes.EXITO)){

					transac.setEstado(Constantes.EJECUTADO);
				}else{
					transac.setEstado(Constantes.FALLO);
					response.setCodigoRespuesta(response.getCodigoRespuesta());
					response.setMensajeRespuesta(response.getMensajeRespuesta());
					response.setMensajeError(response.getMensajeError());
				}
				transac.setIntento(Integer.parseInt(request.getTransaccionData().getNumeroIntento()));
				 insertarDetalle(trazabilidad, transac);
				 
				 
				 
			}catch(EJBException ex){
				logger.error(trazabilidad+Constantes.ERROR+ex);
				response.setCodigoRespuesta(String.valueOf(Constantes.UNO));
				response.setMensajeError(ex.getMessage());
	       	}catch(Exception e){
				logger.error(trazabilidad+Constantes.ERROR+e);
				response.setCodigoRespuesta(String.valueOf(Constantes.UNO));
				response.setMensajeError(e.getMessage());
			}finally{
				logger.info(trazabilidad+Constantes.LOG_FIN_ACTIVIDAD + Constantes.CONSULTAR_PAGO);
			}		
			return response;
	}
   
   //ConsultarPago	
    private Actividad consultarPago(String traza, PagarReciboRequest request,HeaderRequest header
    		,ELKLogLegadoBean elkLegadoBean,AplicacionTarjeta appTarjeta){
		logger.info(traza+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.CONSULTAR_PAGO);
		String mensaje = Constantes.EXITOMENSAJE;
		DetalleError detalleRegistroPago = new DetalleError();
		Actividad actividad =new Actividad();
		actividad.setResultado(true);
		try {
			int contador = 0;
			ListaCustomer listaCustomer=new ListaCustomer();
			List<Customer> lista=new ArrayList<>();
			
			for (DetalleDocumento detalleDocumen : request.getListaDetalleDocumentos()) {
				contador++;
				
				//validar si es  reintento
				if(Constantes.CADENA_CERO.equalsIgnoreCase(request.getTransaccionData().getNumeroIntento())){
					listaCustomer.setIngreso(true);
				}else{
					int intX=Integer.parseInt(request.getTransaccionData().getNumeroIntento());
					int intenAnterior=intX-1;
					//validar  si customer se encuentra procesada					
					listaCustomer=buscarCustomerAP(traza,header.getIdTransaccion(),detalleDocumen.getIdentificador(),lista
							      ,Constantes.FALLO,Constantes.CONSULTAROAC,String.valueOf(intenAnterior));
					lista=listaCustomer.getLista();
				}
				
				if(listaCustomer.isIngreso()){
					
					ConsultaPagoOACRequest requestConsul=new ConsultaPagoOACRequest();
					requestConsul.setTransaccionId(request.getTransaccionData().getNumeroTransaccion());
					requestConsul.setCodAplicacion(appTarjeta.getListTarjeta().get(0).getCodigoCanal());
					requestConsul.setNroOperacion(request.getCodigoOperacion()+Constantes.STRING_GUION_ARRIBA+contador);
					requestConsul.setTrace(request.getCodigoOperacion()+Constantes.STRING_GUION_ARRIBA+contador);
					
					BodyResponse consultarResponse=consultarPagoOAC(traza, request, elkLegadoBean, requestConsul);
					
					if(Constantes.EXITO.equalsIgnoreCase(consultarResponse.getCodigoRespuesta())){
						logger.info(traza+" Se realizo con exito el registro del pago en OAC "+detalleDocumen.getIdentificador());
					}else{
						actividad.setResultado(false); 
						mensaje = Constantes.ERROR;	
						logger.info(traza+" No se pudo consultar el pago en OAC "+detalleDocumen.getIdentificador());
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(traza+Constantes.ERROREXCEPTION + e.getMessage() + "] ", e);
			mensaje = Constantes.ERROR;
			actividad.setResultado(false);
		} finally {
			detalleRegistroPago.setActividad(Constantes.CONSULTAR_PAGO);
			detalleRegistroPago.setDescripcionError(mensaje);
			actividad.setDetalleError(detalleRegistroPago);
			logger.info(traza+Constantes.LOG_FIN_ACTIVIDAD + Constantes.CONSULTAR_PAGO);
		}
		return actividad;
	}
	
	//EnvioCorreoSB
	public Actividad enviarCorreo(String trazabilidad,PagarReciboRequest request,HeaderRequest header,String mensaje,ELKLogLegadoBean elkLegadoBean) {
		BodyResponse resp=null;
		Actividad actividad =new Actividad();
		actividad.setResultado(true);
		DetalleError detalleEnvioMail= new DetalleError();
		String  desError= Constantes.EXITOMENSAJE;
		logger.info(trazabilidad + "*********** 6. Enviar Correo Electronico ***********");
		try{
			logger.info(trazabilidad+Constantes.LOG_INICIO_ACTIVIDAD + Constantes.ENVIAR_CORREO_ELECTRONICO);
			
			String iDT3=propertiesExterno.codError3;
			String iDT4=propertiesExterno.codError4;
				
		    AuditoriaRequestSOAPBean auditCorreo = new AuditoriaRequestSOAPBean();
		    auditCorreo.setIdTransaccion(header.getIdTransaccion());
		    auditCorreo.setIpAplicacion(ClaroUtil.getIp());
		    auditCorreo.setNombreAplicacion(propertiesExterno.nombreAplicacionSB);
		    auditCorreo.setUsuarioAplicacion(propertiesExterno.userIdSB);

		    EnvioCorreoRequestBean correoRequestBean = new EnvioCorreoRequestBean();
		    correoRequestBean.setAuditoriaRequest(auditCorreo);
		    correoRequestBean.setRemitente(propertiesExterno.remitenteSB);
		    correoRequestBean.setDestinatario(request.getCorreo());
		    correoRequestBean.setAsunto(propertiesExterno.asuntoSB);
		    correoRequestBean.setMensaje(mensaje);
		    correoRequestBean.setHtmlFlag(propertiesExterno.htmlFlag);
		    
		    
		    int reintento=Integer.parseInt(propertiesExterno.reintentoWS);
			int inicio=0;
			while(inicio<=reintento){				
				if(inicio>0){
					logger.info(trazabilidad + Constantes.INICIAREINTENT+inicio);
				}
				logger.info(trazabilidad + Constantes.LOG_REQUEST +  GestorOperacionesUtil.printPrettyJSONString(correoRequestBean));
				HeaderRequestBean headerCorreo = obtenerHeaderRequestRest(header.getIdTransaccion());
				resp= envioCorreoSBService.enviarCorreo(trazabilidad, headerCorreo,correoRequestBean,header.getIdTransaccion(),elkLegadoBean);
				logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(resp));
				
				if(iDT3.equals(resp.getCodigoRespuesta()) || iDT4.equals(resp.getCodigoRespuesta())){
					inicio++;
				}else{
					break;
				}
			}			
			
			//Registrar Transaccion Detalle
			TransaccionDetalle transac=new TransaccionDetalle();
			transac.setNumeroTransaccion(header.getIdTransaccion());
			transac.setCodigoComponente(Constantes.ENVIOCORREOSB);
			transac.setNumeroComponente(header.getIdTransaccion());
			transac.setEstado(Constantes.EJECUTADO);
			if(Constantes.EXITO.equalsIgnoreCase(resp.getCodigoRespuesta())){		
				transac.setEstado(Constantes.EJECUTADO);
				logger.info(trazabilidad+"Se envio con exito el correo electronico");
			}else{				
				actividad.setResultado(false);
				desError= Constantes.ERROR;
				transac.setEstado(Constantes.FALLO);

				logger.info(trazabilidad+"ERROR: No se pudo enviar el correo electronico");
			}
			transac.setIntento(Integer.parseInt(request.getTransaccionData().getNumeroIntento()));
			insertarDetalle(trazabilidad, transac);
		    logger.info(trazabilidad+"**   Fin Envio Correo  **");
		}catch(Exception e){
    		logger.error(trazabilidad+Constantes.ERRORCORREO+ e + ".");
 			desError= Constantes.ERROR;
 			actividad.setResultado(false);
    	}finally{
			detalleEnvioMail.setActividad(Constantes.ENVIAR_CORREO_ELECTRONICO);
			detalleEnvioMail.setDescripcionError(desError);
			actividad.setDetalleError(detalleEnvioMail);
			logger.info(trazabilidad+Constantes.LOG_FIN_ACTIVIDAD + Constantes.ENVIAR_CORREO_ELECTRONICO);
		}return actividad;
	}
	
	//Armar Cuerpo Mensaje SB
	private String formarCuepoExitoSBAPP(String trazabilidad,PagarReciboRequest request,String html) {
		StringBuilder fila =new StringBuilder(Constantes.TEXTO_VACIO);
		logger.info(trazabilidad + "**** Inicio formar Correo ****");
		try{
			
			String fechaPago = ClaroUtil.obtenerFechaString(Constantes.TEXTO_VACIO,null);
			//Inicio Cuerpo Mensaje Correo	
			int totaldocumntos=Constantes.CERO;
						
			for (DetalleDocumento detalleDocumento : request.getListaDetalleDocumentos()) {
				for (Documento documento : detalleDocumento.getListaDocumentos()) {
					if(totaldocumntos>Constantes.CERO){
						fila.append(Constantes.SALTOLINEAHTML);
					}
					fila.append(documento.getNumero());
					
					totaldocumntos++;
				}				
			}
			
			html = html.replace(Constantes.BUSCAR_T_MONTOTOAL, String.valueOf(request.getMontoTotal()))
				   .replace(Constantes.BUSCAR_T_NUMERO_TARJETA, request.getNumeroTarjeta())
				   .replace(Constantes.BUSCAR_T_FECHA_HORA, fechaPago)
				   .replace(Constantes.BUSCAR_T_NUMERO_OPERACION, request.getCodigoOperacion())
				   .replace(Constantes.BUSCAR_T_AUTORIZACION_BANCARIA, request.getNumeroAutorizacion())
				   .replace(Constantes.BUSCAR_T_TOTALDOC, String.valueOf(totaldocumntos))
				   .replace(Constantes.BUSCAR_T_RECIBOREF, fila)
				   .replace(Constantes.EMAILHTML, request.getCorreo());
			//Fin Cuerpo Mensaje Correo
			
		    logger.info(trazabilidad+"**** Fin formar Correo ****");
		}catch(Exception e){
    		logger.error(trazabilidad+" Error al formar Correo "+ e + ".");
    	}return html;
	}
	
	
	
	//Insertar Request WS en base datos, Intento Cero
	private void insertarRequest(String trazabilidad, String numeroTransaccion, String cComponent,String cFlujo,String body,String numeroIntento){
		try{
			int numeroInten=Integer.parseInt(numeroIntento);
			if(Constantes.CERO == numeroInten){
				logger.info(trazabilidad+" Inicio - Insertar Request ");
				BodyResponse resp=operacionCCLRepository.insertarRequest(trazabilidad, numeroTransaccion, cComponent, cFlujo, body);
				logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(resp));
				logger.info(trazabilidad+" Fin - Insertar Request ");
			}else{
				logger.info(trazabilidad+" No se Inserta request en Reintentos ");
			}
		}catch(Exception e){
			logger.error(trazabilidad+"  Error - Insertar Request "+ e + ".");
		}
	}
	
	//Insertar Response WS en base datos
	private void insertarResponse(String trazabilidad, String numeroTransaccion, String cComponent,String cFlujo,String body){
		try{
			logger.info(trazabilidad+" Inicio - Insertar Response ");
			BodyResponse resp=operacionCCLRepository.insertarResponse(trazabilidad, numeroTransaccion, cComponent, cFlujo, body);
			logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(resp));
			logger.info(trazabilidad+" Fin - Insertar Response ");
		}catch(Exception e){
			logger.error(trazabilidad+" Error - Insertar Response "+ e + ".");
		}
	}
	
	//Insertar Detalle WS en base datos
	private TransaccionResponse insertarDetalle(String trazabilidad, TransaccionDetalle input){
		TransaccionResponse resp=null;
		try{
			logger.info(trazabilidad+" Inicio - Insertar Detalle ");
			resp=operacionCCLRepository.insertarTransaccionDetalle(trazabilidad, input);
			logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(resp));
			logger.info(trazabilidad+" Fin - Insertar Detalle ");
		}catch(Exception e){
			logger.error(trazabilidad+" Error - Insertar Detalle "+ e + ".");
		}return resp;
	}
	
	//Actualizar Recibo WS en base datos
	private BodyResponse actualizarRecibo(String trazabilidad, TransaccionDetalle input){
		BodyResponse resp=null;
		try{
			logger.info(trazabilidad+" Inicio - Actualizar Recibo");
			resp=operacionCCLRepository.actualizarRecibo(trazabilidad, input);
			logger.info(trazabilidad + Constantes.RESPONS +  GestorOperacionesUtil.printPrettyJSONString(resp));
			logger.info(trazabilidad+" Fin - Actualizar Recibo");
		}catch(Exception e){
			logger.error(trazabilidad+" Error - Actualizar Recibo "+ e + ".");
		}return resp;
	}
	
	//Buscar Customer
	private ListaCustomer buscarCustomer(String trazabilidad, String transaccion,String customer,List<Customer> lista){
	    ListaCustomer listaCustomer=new ListaCustomer();
		try{
			logger.info(trazabilidad+" Inicio - Buscar Customer");
			if(lista.isEmpty()){
				listaCustomer=operacionCCLRepository.consultaCustomer(trazabilidad, transaccion);
			}else{
				logger.info(trazabilidad+" Lista Cargada  ");
				listaCustomer.setLista(lista);
			}
			
			listaCustomer.setIngreso(false);
			logger.info(trazabilidad + " Response : " +  GestorOperacionesUtil.printPrettyJSONString(listaCustomer.getLista()));
			for(Customer cust: listaCustomer.getLista()){
				if(customer.equalsIgnoreCase(cust.getCustomerId())){
					listaCustomer.setIngreso(true);
					break;
				}
			}
			logger.info(trazabilidad+" Fin - Buscar Customer");
		}catch(Exception e){
			logger.error(trazabilidad+" Error - Buscar Customer "+ e + ".");
		}return listaCustomer;
	}
	
	//Buscar CustomerCOnsultar
	private ListaCustomer buscarCustomerAP(String trazabilidad, String transaccion,String customer,List<Customer> lista
			,String estado,String componente,String intento){
	    ListaCustomer listaCustomer=new ListaCustomer();
		try{
			logger.info(trazabilidad+" Inicio - Buscar Customer Consultar");
			if(lista.isEmpty()){
				listaCustomer=operacionCCLRepository.consultaCustomerAP(trazabilidad, transaccion,estado,componente,intento);
			}else{
				logger.info(trazabilidad+" Lista Cargada  ");
				listaCustomer.setLista(lista);
			}
			
			listaCustomer.setIngreso(false);
			logger.info(trazabilidad + " Response : " +  GestorOperacionesUtil.printPrettyJSONString(listaCustomer.getLista()));
			for(Customer cust: listaCustomer.getLista()){
				if(customer.equalsIgnoreCase(cust.getCustomerId())){
					listaCustomer.setIngreso(true);
					break;
				}
			}
			logger.info(trazabilidad+" Fin - Buscar Customer Consultar");
		}catch(Exception e){
			logger.error(trazabilidad+" Error - Buscar Customer Consultar"+ e + ".");
		}return listaCustomer;
	}
	
	private AplicacionTarjeta obtenerDatosAplicacion(String trazabilidad, PagarReciboRequest request) throws BDException{
		AplicacionTarjeta 	appTarjeta;
		try {
			   	appTarjeta=operacionCCLRepository.consultarAplicacion(trazabilidad,
		                request.getCanal(),request.getTipoTarjeta());
		        if(Constantes.CERO==appTarjeta.getListTarjeta().size()) {
		        	 throw new BDException(Constantes.CADENA_UNO, Constantes.ERROR);
		        }
		}catch (Exception e) {
			logger.error(trazabilidad+" Error - Buscar Datos de aplicacion "+ e + ".");
			 throw new BDException(Constantes.CADENA_UNO, e.getMessage(), e);
		}
      
        return appTarjeta;
	}
	
	private HeaderRequestBean obtenerHeaderRequestRest(String idTransaccion) {
		HeaderRequestBean headerRequest = new HeaderRequestBean();
		headerRequest.setIdTransaccion(idTransaccion);
		headerRequest.setMsgid(idTransaccion);
		headerRequest.setTimestamp(ConcentradorPagosUtil.getFechaString(new Date(), Constantes.FORMATO_TIMESTAMP));
		headerRequest.setUserId(Constantes.USUARIO_AUDITORIA);
		headerRequest.setAccept(MediaType.APPLICATION_JSON_VALUE);
		return headerRequest;
	}		

}
