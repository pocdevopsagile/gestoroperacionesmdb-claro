package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class RegistroResponseModel {

	private String idTransaccion;
	private String codComponente;
	private String codWF;
	private String response;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getCodComponente() {
		return codComponente;
	}
	public void setCodComponente(String codComponente) {
		this.codComponente = codComponente;
	}
	public String getCodWF() {
		return codWF;
	}
	public void setCodWF(String codWF) {
		this.codWF = codWF;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	
	
	
	
}
