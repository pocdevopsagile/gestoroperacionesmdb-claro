package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import java.io.Serializable;

import javax.xml.ws.Holder;

import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetServiciosRespType;
import pe.com.claro.eai.servicecommons.AuditType;

public class CrearPagoRespo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Holder<AuditType> audit;
	private Holder<String> xCodAplicacion;
	private Holder<String> xCodMoneda;
	private Holder<String> xTipoIdentific;
	private Holder<String> xDatoIdentific;
	private Holder<String> xNombreCliente;
	private Holder<String> xTrace;
	private Holder<String> xRucAcreedor;
	private Holder<String> xNroIdentifCli;
	private Holder<String> xNroOperacionCobr;
	private Holder<String> xNroOperacionAcre;
	private Holder<String> xNroReferencia;
	private Holder<String> xCodZonaDeudor;
	private Holder<String> xDatoTransaccion;
	private Holder<CrearPagoDetServiciosRespType> xDetDocumentos;
	private Holder<String> xErrStatus;
	private Holder<String> xErrMessage;
	
	public Holder<AuditType> getAudit() {
		return audit;
	}
	public void setAudit(Holder<AuditType> audit) {
		this.audit = audit;
	}
	public Holder<String> getxCodAplicacion() {
		return xCodAplicacion;
	}
	public void setxCodAplicacion(Holder<String> xCodAplicacion) {
		this.xCodAplicacion = xCodAplicacion;
	}
	public Holder<String> getxCodMoneda() {
		return xCodMoneda;
	}
	public void setxCodMoneda(Holder<String> xCodMoneda) {
		this.xCodMoneda = xCodMoneda;
	}
	public Holder<String> getxTipoIdentific() {
		return xTipoIdentific;
	}
	public void setxTipoIdentific(Holder<String> xTipoIdentific) {
		this.xTipoIdentific = xTipoIdentific;
	}
	public Holder<String> getxDatoIdentific() {
		return xDatoIdentific;
	}
	public void setxDatoIdentific(Holder<String> xDatoIdentific) {
		this.xDatoIdentific = xDatoIdentific;
	}
	public Holder<String> getxNombreCliente() {
		return xNombreCliente;
	}
	public void setxNombreCliente(Holder<String> xNombreCliente) {
		this.xNombreCliente = xNombreCliente;
	}
	public Holder<String> getxTrace() {
		return xTrace;
	}
	public void setxTrace(Holder<String> xTrace) {
		this.xTrace = xTrace;
	}
	public Holder<String> getxRucAcreedor() {
		return xRucAcreedor;
	}
	public void setxRucAcreedor(Holder<String> xRucAcreedor) {
		this.xRucAcreedor = xRucAcreedor;
	}
	public Holder<String> getxNroIdentifCli() {
		return xNroIdentifCli;
	}
	public void setxNroIdentifCli(Holder<String> xNroIdentifCli) {
		this.xNroIdentifCli = xNroIdentifCli;
	}
	public Holder<String> getxNroOperacionCobr() {
		return xNroOperacionCobr;
	}
	public void setxNroOperacionCobr(Holder<String> xNroOperacionCobr) {
		this.xNroOperacionCobr = xNroOperacionCobr;
	}
	public Holder<String> getxNroOperacionAcre() {
		return xNroOperacionAcre;
	}
	public void setxNroOperacionAcre(Holder<String> xNroOperacionAcre) {
		this.xNroOperacionAcre = xNroOperacionAcre;
	}
	public Holder<String> getxNroReferencia() {
		return xNroReferencia;
	}
	public void setxNroReferencia(Holder<String> xNroReferencia) {
		this.xNroReferencia = xNroReferencia;
	}
	public Holder<String> getxCodZonaDeudor() {
		return xCodZonaDeudor;
	}
	public void setxCodZonaDeudor(Holder<String> xCodZonaDeudor) {
		this.xCodZonaDeudor = xCodZonaDeudor;
	}
	public Holder<String> getxDatoTransaccion() {
		return xDatoTransaccion;
	}
	public void setxDatoTransaccion(Holder<String> xDatoTransaccion) {
		this.xDatoTransaccion = xDatoTransaccion;
	}
	public Holder<CrearPagoDetServiciosRespType> getxDetDocumentos() {
		return xDetDocumentos;
	}
	public void setxDetDocumentos(Holder<CrearPagoDetServiciosRespType> xDetDocumentos) {
		this.xDetDocumentos = xDetDocumentos;
	}
	public Holder<String> getxErrStatus() {
		return xErrStatus;
	}
	public void setxErrStatus(Holder<String> xErrStatus) {
		this.xErrStatus = xErrStatus;
	}
	public Holder<String> getxErrMessage() {
		return xErrMessage;
	}
	public void setxErrMessage(Holder<String> xErrMessage) {
		this.xErrMessage = xErrMessage;
	}
	
	@Override
	public String toString() {
		return "CrearPagoResponse [audit=" + audit + ", xCodAplicacion=" + xCodAplicacion + ", xCodMoneda=" + xCodMoneda
				+ ", xTipoIdentific=" + xTipoIdentific + ", xDatoIdentific=" + xDatoIdentific + ", xNombreCliente="
				+ xNombreCliente + ", xTrace=" + xTrace + ", xRucAcreedor=" + xRucAcreedor + ", xNroIdentifCli="
				+ xNroIdentifCli + ", xNroOperacionCobr=" + xNroOperacionCobr + ", xNroOperacionAcre="
				+ xNroOperacionAcre + ", xNroReferencia=" + xNroReferencia + ", xCodZonaDeudor=" + xCodZonaDeudor
				+ ", xDatoTransaccion=" + xDatoTransaccion + ", xDetDocumentos=" + xDetDocumentos + ", xErrStatus="
				+ xErrStatus + ", xErrMessage=" + xErrMessage + "]";
	}
	
}
