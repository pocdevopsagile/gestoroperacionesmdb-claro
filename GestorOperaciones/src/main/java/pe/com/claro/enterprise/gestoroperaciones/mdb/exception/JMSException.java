package pe.com.claro.enterprise.gestoroperaciones.mdb.exception;

public class JMSException extends RuntimeException{

	private static final long serialVersionUID = -7482288873992395827L;
	private final String codigo;
	private final String mensaje;
	
	public  JMSException(String codigo,String mensaje,Exception e){
		super(codigo,e);
		this.codigo=codigo;
		this.mensaje=mensaje;
	}

	public String getCodigo() {
		return codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	
}
