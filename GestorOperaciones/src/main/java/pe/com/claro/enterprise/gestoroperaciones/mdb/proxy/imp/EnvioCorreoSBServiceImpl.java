package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;

import static pe.com.claro.enterprise.gestoroperaciones.mdb.util.ConcentradorPagosUtil.printPrettyJSONString;

import java.util.Locale;


import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.EnvioCorreoRequestBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequestBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.EnvioCorreoSBService;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.JAXBUtilitarios;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;


@Service
public class EnvioCorreoSBServiceImpl implements EnvioCorreoSBService {

	private static final Logger logger = LogManager.getLogger(EnvioCorreoSBServiceImpl.class);
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public BodyResponse enviarCorreo(String trazabilidad,HeaderRequestBean headerRequest, EnvioCorreoRequestBean requestBean,String trasanccion,ELKLogLegadoBean elkLegadoBean) {
		long tiempoInicio = System.currentTimeMillis();
		BodyResponse response = null;
		String responseJson = null;
		
		String url = propertiesExterno.envioCorreoMotor;
		Integer timeoutConexion=Integer.parseInt( propertiesExterno.envioCorreoSBConexion);
		Integer timeoutEjecucion = Integer.parseInt(propertiesExterno.envioCorreoSBEjecucion);		
		
		try {
			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			httpHeaders.set(Constantes.NOMBRECAMPOAUDITORIA_IDTRANSACCION, headerRequest.getIdTransaccion());
			httpHeaders.set(Constantes.NOMBRECAMPOAUDITORIA_MSGID, headerRequest.getMsgid());
			httpHeaders.set(Constantes.NOMBRECAMPOAUDITORIA_TIMESTAMP, headerRequest.getTimestamp());
			httpHeaders.set(Constantes.NOMBRECAMPOAUDITORIA_USERID, headerRequest.getUserId());
			httpHeaders.set(Constantes.NOMBRECAMPOAUDITORIA_ACCEPT, headerRequest.getAccept());
			
			logger.info(trazabilidad + Constantes.SIGUIENTEWS + url);
			logger.info(trazabilidad + Constantes.TIPO + HttpMethod.POST);
			logger.info(trazabilidad + Constantes.DATOSHEADER + printPrettyJSONString(httpHeaders));	
			
	        AuditTypeRequest auditRequest = new AuditTypeRequest();
	        auditRequest.setIdTransaccion(requestBean.getAuditoriaRequest().getIdTransaccion());
	        auditRequest.setIpAplicacion(requestBean.getAuditoriaRequest().getIpAplicacion());
	        auditRequest.setCodigoAplicacion(requestBean.getAuditoriaRequest().getNombreAplicacion());
	        auditRequest.setUsrAplicacion(requestBean.getAuditoriaRequest().getUsuarioAplicacion());
	        
			EnviarCorreoRequest request = new EnviarCorreoRequest();
			request.setAuditRequest(auditRequest);
			request.setRemitente(requestBean.getRemitente());
			request.setDestinatario(requestBean.getDestinatario());
			request.setAsunto(requestBean.getAsunto());
			request.setMensaje(requestBean.getMensaje());
			request.setHtmlFlag(requestBean.getHtmlFlag());
			
			String requestJson = printPrettyJSONString(request);
			logger.info(trazabilidad + Constantes.DATOSENTRADA + requestJson);
			
			elkLegadoBean.setNumeroActividad(Double.parseDouble(Constantes.ENVIOCORREOSB));
			elkLegadoBean.setNombreActividad("enviaCorreo");
			elkLegadoBean.setDetalleActividad("Envio de Correo");
			elkLegadoBean.setTipoConsulta(ELKUtil.TIPO_CONSULTA_WS);
			elkLegadoBean.setDuenoConsulta(Constantes.NOMBRE_API);
			elkLegadoBean.setOrigenConsulta(url);
			elkLegadoBean.setDetalleConsulta("enviarCorreos");
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_REQUEST);
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);
			elkLegadoBean.setMensaje(JAXBUtilitarios.anyObjectToXmlText(request));

			ELKUtil.logObjetoLegado(elkLegadoBean);			
			
			HttpEntity<String> httpEntity = new HttpEntity<>(requestJson, httpHeaders);
			
			SimpleClientHttpRequestFactory httpRequestFactory = new SimpleClientHttpRequestFactory();
			httpRequestFactory.setConnectTimeout(timeoutConexion);
			httpRequestFactory.setReadTimeout(timeoutEjecucion);
			
			logger.info(trazabilidad + Constantes.TIMEOUTCEXCEPTION + timeoutConexion);
			logger.info(trazabilidad + Constantes.TIMEOUTEJECUCION + timeoutEjecucion);
			
			RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
			ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity,
					String.class);
			
			logger.info(trazabilidad + Constantes.WSEJECUTADO);
			responseJson = responseEntity.getBody();
			response = new ObjectMapper().readValue(responseJson, BodyResponse.class);
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(JAXBUtilitarios.anyObjectToXmlText(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDF);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		} catch (Exception e) {
			logger.error(trazabilidad + "Exception: " + e.getMessage(), e);
			response = new BodyResponse();

			String trazaError = ExceptionUtils.getStackTrace(e);
			String codigoError = Constantes.CADENA_VACIA;
			String msgError = Constantes.CADENA_VACIA;

			if (trazaError.toUpperCase(Locale.getDefault()).contains(Constantes.WS_TIMEOUT_EXCEPTION)) {
				codigoError = propertiesExterno.codError3;
				msgError = propertiesExterno.msjError3.replace(Constantes.CONST_WS_NAME, url);
			} else {
				codigoError = propertiesExterno.codError4;
				msgError = propertiesExterno.msjError4.replace(Constantes.CONST_WS_NAME, url);
			}

			response.setCodigoRespuesta(codigoError);
			response.setMensajeError(msgError);
			logger.error(trazabilidad + "getCodigoRespuesta: " + response.getCodigoRespuesta());
			logger.error(trazabilidad + "getMensajeError: " + response.getMensajeError());
			logger.error(trazabilidad + "getMensajeRespuesta: " + response.getMensajeRespuesta());
			logger.error(trazabilidad + "msgError: " + ("Error: Supero tiempo de espera - WS:'$nombre_WS'").replace(Constantes.CONST_WS_NAME, url));
		} finally {
			logger.info(trazabilidad + Constantes.DATOSDESALIDA + responseJson);
			logger.info(trazabilidad + Constantes.TIEMPOINVOCACION + (System.currentTimeMillis() - tiempoInicio)
					+ Constantes.MILIS_TXT);
		}
		
		return response;
	}

}