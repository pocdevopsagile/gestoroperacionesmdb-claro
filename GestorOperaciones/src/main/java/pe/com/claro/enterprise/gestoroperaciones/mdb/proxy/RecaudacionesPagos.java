package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;

import com.fasterxml.jackson.core.JsonProcessingException;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.ConsultaPagoOACRequest;

public interface RecaudacionesPagos {

	public BodyResponse consultarPago(String trazabilidad,
			ConsultaPagoOACRequest request,ELKLogLegadoBean elkLegadoBean)throws JsonProcessingException  ;
}
