package pe.com.claro.enterprise.gestoroperaciones.mdb.repositories;

public interface BancoRepository{

	Object findById(Long bancoId);
//  List<Banco> findAll();
//  Banco findById(Long id);
//  void update(Banco banco);

	void save(pe.com.claro.enterprise.gestoroperaciones.mdb.models.Banco any);

}
