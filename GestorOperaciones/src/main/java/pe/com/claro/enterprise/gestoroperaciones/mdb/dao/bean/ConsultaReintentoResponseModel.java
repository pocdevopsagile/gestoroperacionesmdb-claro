package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class ConsultaReintentoResponseModel extends ResponseModel{

	private String codComponente;

	public String getCodComponente() {
		return codComponente;
	}

	public void setCodComponente(String codComponente) {
		this.codComponente = codComponente;
	}
	
	
}
