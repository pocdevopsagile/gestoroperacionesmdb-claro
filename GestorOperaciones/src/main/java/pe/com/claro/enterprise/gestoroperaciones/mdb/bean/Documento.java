package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Documento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numero;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String tipo;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Double monto;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String moneda;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	@Override
	public String toString() {
		return "Documento [numero=" + numero + ", tipo=" + tipo + ", monto=" + monto + ", moneda=" + moneda + "]";
	}
	
}
