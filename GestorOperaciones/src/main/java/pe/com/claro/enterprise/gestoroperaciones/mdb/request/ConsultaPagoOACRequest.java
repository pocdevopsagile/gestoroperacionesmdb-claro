package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

public class ConsultaPagoOACRequest {

	
	private String transaccionId;
	private String codAplicacion;
	private String trace;
	private String nroOperacion;
	
	public String getTransaccionId() {
		return transaccionId;
	}
	public void setTransaccionId(String transaccionId) {
		this.transaccionId = transaccionId;
	}
	public String getCodAplicacion() {
		return codAplicacion;
	}
	public void setCodAplicacion(String codAplicacion) {
		this.codAplicacion = codAplicacion;
	}
	public String getTrace() {
		return trace;
	}
	public void setTrace(String trace) {
		this.trace = trace;
	}
	public String getNroOperacion() {
		return nroOperacion;
	}
	public void setNroOperacion(String nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	
	@Override
	public String toString() {
		return "ConsultaPagoOACRequest [transaccionId=" + transaccionId + ", codAplicacion=" + codAplicacion
				+ ", trace=" + trace + ", nroOperacion=" + nroOperacion + "]";
	}
	
	
	
}
