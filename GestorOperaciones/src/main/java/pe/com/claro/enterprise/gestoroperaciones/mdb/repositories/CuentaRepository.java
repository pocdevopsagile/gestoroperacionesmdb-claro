package pe.com.claro.enterprise.gestoroperaciones.mdb.repositories;

import java.util.List;
import java.util.Optional;

import pe.com.claro.enterprise.gestoroperaciones.mdb.models.Cuenta;

public interface CuentaRepository{
    @Query("select c from Cuenta c where c.persona=?1")
    Optional<Cuenta> findByPersona(String persona);

	List<Cuenta> findAll();

	Object findById(Long id);

	Cuenta save(Cuenta cuentaPepe);

	void delete(Cuenta cuenta);

//    List<Cuenta> findAll();
//    Cuenta findById(Long id);
//    void update(Cuenta cuenta);
}