package pe.com.claro.enterprise.gestoroperaciones.mdb.models;

public class DineroInsuficienteException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DineroInsuficienteException(String message) {
        super(message);
    }
}

