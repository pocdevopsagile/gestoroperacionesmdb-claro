package pe.com.claro.enterprise.gestoroperaciones.mdb;

import javax.ejb.MessageDriven;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.service.PagarReciboService;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.GestorOperacionesUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.SpringBeanInterceptors;


@MessageDriven
@Interceptors(SpringBeanInterceptors.class)
public class GestorOperacionesMDB implements MessageListener{

	
	private static final Logger logger = Logger.getLogger(GestorOperacionesMDB.class);
	
	@Autowired
	private  PagarReciboService pagarReciboService;
	
	public GestorOperacionesMDB() {
		logger.info("*** [GestorOperacionesMDB] - Inicializado ***");
	}
	
	
	@Override
	public void onMessage(Message message) {
		long tiempoProceso = System.currentTimeMillis();
		logger.info(" [INICIO]- METODO: onMessage");
		String msjTx = " [GestorOperacionesMDB  idTx= ";
		try {
			if (message instanceof TextMessage) {
				TextMessage txtMessage = (TextMessage) message;
				String mensaje = txtMessage.getText();

				logger.info(msjTx+" [INICIO] - obtiene mensaje ");	
				ObjectMapper objectMapper = new ObjectMapper();
				PagarReciboRequest request = objectMapper.readValue(mensaje, PagarReciboRequest.class);
				
				if(Constantes.CADENA_CERO.equalsIgnoreCase(request.getTransaccionData().getNumeroIntento())){
					msjTx=msjTx+request.getTransaccionData().getNumeroTransaccion()+"]";
				}else{
					msjTx=msjTx+request.getTransaccionData().getNumeroTransaccion()+" reIn: "+request.getTransaccionData().getNumeroIntento()+ "]";
				}	
				logger.info(msjTx + " Objeto WFRegularDeudaRequest" + GestorOperacionesUtil.printPrettyJSONString(request));
				logger.info(msjTx+" [FIN] - obtiene mensaje ");
				
				pagarReciboService.pagarRecibo(msjTx, request);
				
			}else {
				logger.error(msjTx+" El mensaje no es de tipo TextMessage.");
			}
		} catch (JMSException e) {
			logger.error(msjTx+" [JMSException][Error:" + e.getMessage() + "]", e);
		} catch (Exception e) {
			logger.error(msjTx+" [Exception][Error:" + e.getMessage() + "]", e);
		} finally {
			logger.info(msjTx+" - Tiempo total de proceso(ms): "
					+ (System.currentTimeMillis() - tiempoProceso) + " milisegundos.");
			logger.info(msjTx+" [FIN][METODO:onMessage]");
		}
		
	}

}
