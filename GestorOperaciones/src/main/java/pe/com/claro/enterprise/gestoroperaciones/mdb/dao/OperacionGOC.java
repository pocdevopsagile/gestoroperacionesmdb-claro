package pe.com.claro.enterprise.gestoroperaciones.mdb.dao;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ActEstadoTransaccionRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.AplicacionTarjeta;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoResponseModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ListaCustomer;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.RegistroMonitoreoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionDetalle;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBNoDisponibleException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBTimeoutException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;

public interface OperacionGOC {

	public void registrarMonitoreo(String trazabilidad,RegistroMonitoreoRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public TransaccionResponse insertarTransaccionDetalle(String mensajeTransaccion, TransaccionDetalle input);
	
	public BodyResponse actualizarRecibo(String mensajeTransaccion, TransaccionDetalle input);
	
	public ListaCustomer consultaCustomer(String mensajeTransaccion, String transaccion);
	
	public BodyResponse insertarRequest(String mensajeTransaccion, String numeroTransaccion, String cComponent,
			String cFlujo, String body);
	
	public BodyResponse insertarResponse(String mensajeTransaccion, String numeroTransaccion, String cComponent,
			String cFlujo, String body);
	
	public ConsultaReintentoResponseModel consultarReintento(String trazabilidad,
			ConsultaReintentoRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public void actualizarEstadoTransaccion(String trazabilidad, PropertiesExterno properties,
			ActEstadoTransaccionRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException ;
	
	public AplicacionTarjeta consultarAplicacion(String mensajeTransaccion, String identificador, String tarjeta);
	
	public ListaCustomer consultaCustomerAP(String mensajeTransaccion, String transaccion,String estado,String componente,String intento);
}
