package pe.com.claro.enterprise.gestoroperaciones.mdb.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MotorVerificacionAppResponse extends BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String codigoComercio;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String usuarioDebito;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String contraDebito;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String response;
	
	public String getCodigoComercio() {
		return codigoComercio;
	}
	public void setCodigoComercio(String codigoComercio) {
		this.codigoComercio = codigoComercio;
	}
	public String getUsuarioDebito() {
		return usuarioDebito;
	}
	public void setUsuarioDebito(String usuarioDebito) {
		this.usuarioDebito = usuarioDebito;
	}
	public String getContraDebito() {
		return contraDebito;
	}
	public void setContraDebito(String contraDebito) {
		this.contraDebito = contraDebito;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
}
