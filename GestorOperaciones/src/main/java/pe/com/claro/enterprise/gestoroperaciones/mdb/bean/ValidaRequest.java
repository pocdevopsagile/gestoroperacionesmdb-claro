package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import javax.ws.rs.core.HttpHeaders;

import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;


public class ValidaRequest {

	private ValidaRequest(){}

	public  static boolean validarHeaderRequest(HttpHeaders httpHeaders){
		boolean valid=true;
		if(null == httpHeaders.getRequestHeader(Constantes.IDTRANSACCION) ){valid=false;}
		if(null == httpHeaders.getRequestHeader(Constantes.MSGID)){valid=false;}
		if(null == httpHeaders.getRequestHeader(Constantes.TIMESTAMP)){valid=false;}
		if(null == httpHeaders.getRequestHeader(Constantes.USERID) ){valid=false;}
		if(null == httpHeaders.getRequestHeader(Constantes.ACCEPT) ){valid=false;}		
		return valid;
	}
	
	public  static boolean validarPagarRecibo(PagarReciboRequest request){
		boolean valid=true;
		if(null == request.getTipoTarjeta() ||request.getTipoTarjeta().isEmpty() ){valid=false;}
		if(null == request.getNumeroTarjeta() || request.getNumeroTarjeta().isEmpty() ){valid=false;}
		if(null == request.getCodigoOperacion()|| request.getCodigoOperacion().isEmpty() ){valid=false;}
		if(null == request.getCanal() ||request.getCanal().isEmpty() ){valid=false;}	
		if(null == request.getNumeroAutorizacion() ||request.getNumeroAutorizacion().isEmpty() ){valid=false;}	
		if(null == request.getCorreo() ||request.getCorreo().isEmpty() ){valid=false;}	
		if(null == request.getMoneda() ||request.getMoneda().isEmpty() ){valid=false;}	
		if(!validarPagarRec(request)){valid=false;}
		return valid;
	}
	
	public  static boolean validarPagarRec(PagarReciboRequest request){
		boolean valid=true;
		if(null == request.getMontoTotal() ){valid=false;}
		if(null == request.getCodigoComercio() || request.getCodigoComercio().isEmpty() ){valid=false;}
		if(null == request.getCodigoPortal() ){valid=false;}
		if(null == request.getLinea() ||request.getLinea().isEmpty() ){valid=false;}	
		if(null == request.getNumeroOrdenMotor() ){valid=false;}	
		if(null == request.getNumeroOrdenPortal() ||request.getNumeroOrdenPortal().isEmpty() ){valid=false;}	
		if(null == request.getMedioPago() ||request.getMedioPago().isEmpty() ){valid=false;}	
		if(null == request.getTransaccionData() ){valid=false;}	
		if(null == request.getListaDetalleDocumentos() || request.getListaDetalleDocumentos().isEmpty()){valid=false;}	
		return valid;
	}
	
	public  static String validarPagar(PagarReciboRequest request){
		String texto=validarPagar2(request);
		if(null == request.getTipoTarjeta() ||request.getTipoTarjeta().isEmpty() ){texto="TipoTarjeta";return texto;}
		if(null == request.getNumeroTarjeta() || request.getNumeroTarjeta().isEmpty() ){texto="NumeroTarjeta";return texto;}
		if(null == request.getCodigoOperacion()|| request.getCodigoOperacion().isEmpty() ){texto="CodigoOperacion";return texto;}
		if(null == request.getCanal() ||request.getCanal().isEmpty() ){texto="Canal";return texto;}	
		if(null == request.getNumeroAutorizacion() ||request.getNumeroAutorizacion().isEmpty() ){texto="NumeroAutorizacion";return texto;}	
		if(null == request.getCorreo() ||request.getCorreo().isEmpty() ){texto="Correo";return texto;}	
		if(null == request.getMoneda() ||request.getMoneda().isEmpty() ){texto="Moneda";return texto;}	
		return texto;
	}
	
	public  static String validarPagar2(PagarReciboRequest request){
		String texto=Constantes.EXITO;
		if(null == request.getMontoTotal() ){texto="MontoTotal";return texto;}
		if(null == request.getCodigoComercio() || request.getCodigoComercio().isEmpty() ){texto="CodigoComercio";return texto;}
		if(null == request.getCodigoPortal() ){texto="CodigoPortal";return texto;}
		if(null == request.getLinea() ||request.getLinea().isEmpty() ){texto="Linea";return texto;}	
		if(null == request.getNumeroOrdenMotor() ){texto="NumeroOrdenMotor";return texto;}	
		if(null == request.getNumeroOrdenPortal() ||request.getNumeroOrdenPortal().isEmpty() ){texto="NumeroOrdenPortal";return texto;}	
		if(null == request.getMedioPago() ||request.getMedioPago().isEmpty() ){texto="MedioPago";return texto;}	
		if(null == request.getTransaccionData() ){texto="TransaccionData";return texto;}	
		if(null == request.getListaDetalleDocumentos() || request.getListaDetalleDocumentos().isEmpty()){texto="ListaDetalleDocumentos";return texto;}	
		return texto;
	}
	
	
}
