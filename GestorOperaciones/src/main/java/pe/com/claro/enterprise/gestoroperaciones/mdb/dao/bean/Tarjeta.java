package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class Tarjeta {

	
	private String codigoCanal;
	private String descripcion;
	private String canal;
	private String medioPago;
	private String codigoTarjeta;
	private String codigoCanalOAC;
	private String empleadoTipi;
	private String metodoTipi;
	
	public String getCodigoCanal() {
		return codigoCanal;
	}
	public void setCodigoCanal(String codigoCanal) {
		this.codigoCanal = codigoCanal;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	public String getCodigoTarjeta() {
		return codigoTarjeta;
	}
	public void setCodigoTarjeta(String codigoTarjeta) {
		this.codigoTarjeta = codigoTarjeta;
	}
	public String getCodigoCanalOAC() {
		return codigoCanalOAC;
	}
	public void setCodigoCanalOAC(String codigoCanalOAC) {
		this.codigoCanalOAC = codigoCanalOAC;
	}
	public String getEmpleadoTipi() {
		return empleadoTipi;
	}
	public void setEmpleadoTipi(String empleadoTipi) {
		this.empleadoTipi = empleadoTipi;
	}
	public String getMetodoTipi() {
		return metodoTipi;
	}
	public void setMetodoTipi(String metodoTipi) {
		this.metodoTipi = metodoTipi;
	}
	
	
	
}
