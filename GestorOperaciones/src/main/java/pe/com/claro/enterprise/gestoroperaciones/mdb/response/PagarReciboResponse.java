package pe.com.claro.enterprise.gestoroperaciones.mdb.response;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.DetalleError;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PagarReciboResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private BodyResponse auditResponse;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<DetalleError> trazabilidadActividad;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String actividad;
	
	public BodyResponse getAuditResponse() {
		return auditResponse;
	}
	public void setAuditResponse(BodyResponse auditResponse) {
		this.auditResponse = auditResponse;
	}
	public List<DetalleError> getTrazabilidadActividad() {
		return trazabilidadActividad;
	}
	public void setTrazabilidadActividad(List<DetalleError> trazabilidadActividad) {
		this.trazabilidadActividad = trazabilidadActividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	
	@Override
	public String toString() {
		return "PagarReciboResponse [auditResponse=" + auditResponse + ", trazabilidadActividad="
				+ trazabilidadActividad + ", actividad=" + actividad + "]";
	}
	
	
	


}
