package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;

public class AplicacionTarjeta  extends BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private List<Tarjeta> listTarjeta;

	public List<Tarjeta> getListTarjeta() {
		return listTarjeta;
	}

	public void setListTarjeta(List<Tarjeta> listTarjeta) {
		this.listTarjeta = listTarjeta;
	}

	@Override
	public String toString() {
		return "AplicacionTarjeta [listTarjeta=" + listTarjeta + "]";
	}

}
