package pe.com.claro.enterprise.gestoroperaciones.mdb.repositories;

public @interface Query {

	String value();

}
