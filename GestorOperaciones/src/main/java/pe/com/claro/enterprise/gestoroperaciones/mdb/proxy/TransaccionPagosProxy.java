package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRespo;

public interface TransaccionPagosProxy {
	
	public CrearPagoRespo realizarPago(CrearPagoRequest request,String transaccion,ELKLogLegadoBean elkLegadoBean);

}
