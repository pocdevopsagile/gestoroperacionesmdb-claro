package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.JMSException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.JMSConnector;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;
import java.util.Hashtable;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

@Service
public class JMSConnectorImpl implements JMSConnector {

	
    private static final Logger logger = Logger.getLogger(JMSConnectorImpl.class);
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	public int enviarCola(String idTransaccion,String trazabilidad,String message)
			throws JMSException, Exception {
		
		int respJms = Constantes.UNO;
		QueueSender qsender = null;
		QueueSession qsession = null;
		QueueConnection qcon = null;
		
		try {
			
			String provider = propertiesExterno.jmsProvider;
			String jndifactory = propertiesExterno.jmsFactory;
			String cola = propertiesExterno.jmsQueue;
			logger.info(trazabilidad + "Provider JMS: " + provider);
			logger.info(trazabilidad + "Connection Factory: " + jndifactory);
			logger.info(trazabilidad + "Cola: " + cola);
			
			String providerUrl = provider.trim();
			String jmsFactory = jndifactory.trim();
			String queueC = cola.trim();

			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, Constantes.CONTEXTO);
			env.put(Context.PROVIDER_URL, providerUrl);

			Context ctx = new InitialContext(env);

			QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup(jmsFactory);
			qcon = qconFactory.createQueueConnection();

			qsession = qcon.createQueueSession(false, 0);
			Queue queue = (Queue) ctx.lookup(queueC);

			qsender = qsession.createSender(queue);
			
			final TextMessage sendMsg = qsession.createTextMessage();
			sendMsg.setText(message);
			qsender.send(sendMsg);
			respJms = Constantes.CERO;
			
		} catch (JMSException e) {
			respJms = Constantes.MENOSUNO;
			logger.info(trazabilidad + "Error al enviar el mensaje a la cola: " + e.getMessage());
			
		} catch (Exception e) {
			respJms = Constantes.MENOSUNO;
			logger.info(trazabilidad + "Error al enviar el mensaje a la cola: " + e.getMessage());
			
		} finally {
			if (qsender != null) {
				qsender.close();
			}
			if (qsession != null) {
				qsession.close();
			}
			if (qcon != null) {
				qcon.close();
			}
			
			logger.info(trazabilidad + "Mensaje a enviar: " + message);
		}
		return respJms;
	}
}
