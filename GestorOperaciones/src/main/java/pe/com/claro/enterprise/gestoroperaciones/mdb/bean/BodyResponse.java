package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String idTransaccion;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String mensajeError;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String codigoRespuesta;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String mensajeRespuesta;
	
	public BodyResponse() {
		super();
	}

	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	@Override
	public String toString() {
		return "BodyResponse [idTransaccion=" + idTransaccion + ", mensajeError=" + mensajeError + ", codigoRespuesta="
				+ codigoRespuesta + ", mensajeRespuesta=" + mensajeRespuesta + "]";
	}
	
	
}
