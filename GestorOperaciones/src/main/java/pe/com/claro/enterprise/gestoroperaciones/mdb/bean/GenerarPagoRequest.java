package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;
import java.util.List;

public class GenerarPagoRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tipoTarjeta;
	private String numeroTarjeta;
	private String numeroAutorizacion;
	private String codigoOperacion;
	private String canal;
	private String correo;
	private String moneda;
	private Double montoTotal;
	private List<DetalleDocumento> listaDetalleDocumentos;
	
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}
	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}
	public List<DetalleDocumento> getListaDetalleDocumentos() {
		return listaDetalleDocumentos;
	}
	public void setListaDetalleDocumentos(List<DetalleDocumento> listaDetalleDocumentos) {
		this.listaDetalleDocumentos = listaDetalleDocumentos;
	}
	public Double getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}
	
	@Override
	public String toString() {
		return "GenerarPagoRequest [tipoTarjeta=" + tipoTarjeta + ", numeroTarjeta=" + numeroTarjeta
				+ ", numeroAutorizacion=" + numeroAutorizacion + ", codigoOperacion=" + codigoOperacion + ", canal="
				+ canal + ", correo=" + correo + ", moneda=" + moneda + ", montoTotal=" + montoTotal
				+ ", listaDetalleDocumentos=" + listaDetalleDocumentos + "]";
	}
}
