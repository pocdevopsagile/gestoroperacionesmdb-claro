package pe.com.claro.enterprise.gestoroperaciones.mdb.util;

public class Constantes {

	private Constantes(){}
	public static final String SERVICE_URI = "GestorOperacionesMDB";
	public static final String TEXTO_VACIO = "";
	public static final String TEXTO_ESPACIO = "";
	public static final String SOCKETTIMEOUTEXCEP = "SocketTimeoutException";  
	public static final String REMOTEACCESSEXCEP = "RemoteAccessException";
	public static final String WEBSERVICEEXCEP = "WebServiceException";
	public static final String CONNECTEXCEP = "ConnectException";
	public static final String CONSTANTE_PUNTO = ".";
	public static final String VALOR_OK = "OK";
	public static final String CODIGO_EXITO = "0";
	public static final String VALOR_1 = "1";
	public static final String VALOR_2 = "2";
	public static final String CONTEXTO = "weblogic.jndi.WLInitialContextFactory";
	
	public static final String PALOTTE= "\\|";
	public static final String VALORIGUAL="=";
	public static final String SEPARADOR_COMA_UNIC = "\\,";
	
	public static final Integer CODIGO_CERO = 0;
	
	public static final String IDTRANSACCION = "idTransaccion";
	public static final String MSGID = "msgid";
	public static final String USERID	= "userId";
	public static final String TIMESTAMP = "timestamp";
	public static final String ACCEPT = "accept";
	
	public static final String CADENA_CERO = "0";
	public static final String CADENA_UNO = "1";
	
	public static final String FORMATO_FECHA_DEFAULT = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
	public static final String MENOSUNOCA = "-1";
	public static final String MENOSTRES = "-3";
	public static final String MENOSCUATRO = "-4";
	
	public static final int MENOSUNO = -1;
	public static final int CERO = 0;
	public static final int UNO = 1;
	public static final int DOS = 2;
	public static final int TRES = 3;
	public static final int CUATRO = 4;
	public static final int CINCO = 5;
	public static final int SEIS = 6;
	public static final int SIETE = 7;
	public static final int OCHO = 8;
	public static final int NUEVE = 9;
	public static final int DIEZ = 10;
	public static final int ONCE = 11;
	public static final int DOCE = 12;
	public static final int VEINTE = 20;
	public static final int DIECISEIS = 16;
	public static final int DIECISIETE = 17;
	public static final int VEINTICUATRO = 24;
	public static final int NOVENTAINUEVE = 99;
	public static final String OCHOCADENA = "8";

	public static final String OPERACIONBD = "$nombre_SP";
	public static final String WS_TIMEOUT_EXCEPTION = "SOCKETTIMEOUTEXCEPTION";
	public static final String SQL_TIMEOUTEXCEPTION = "SQLTIMEOUTEXCEPTION";
	public static final String SQL_BDOUTEXCEPTION = "BDEXCEPTION";
	
	public static final Integer STATUS_CODE_OK = 200;
	
	public static final String LOG_INICIO_ACTIVIDAD = "[INICIO] - Actividad : ";
	public static final String LOG_FIN_ACTIVIDAD = "[FIN] - Actividad : ";
	public static final String PROCESARVENTA	= "Procesar Venta";
	public static final String NOTIFICAPAGO	= "Notifica pago";
	public static final String MOTORPAGOS	="4";
	public static final String APPVENTAS	="6";
	public static final String VERIFICARFLUJO	="1";
	public static final String INICIAREINTENT	=" Inicia Intento N: ";
	public static final String RESPONS = " Response: ";
	
	public static final String INICIO = "Inicio del metodo";
	public static final String ESPACIO = " ";
	public static final String GUION = "-";
	public static final String SEPARADOR_PUNTO = ".";
	public static final String SEPARADOR_COMA = ",";
	
	public static final String PI_NUM_TRANSACCION = "PI_NUM_TRANSACCION";
	public static final String PI_COD_COMPONENTE = "PI_COD_COMPONENTE";
	public static final String PI_NUM_COMPONENTE = "PI_NUM_COMPONENTE";
	public static final String PI_COD_WF = "PI_COD_WF";
	public static final String PI_CODIGO_WF = "PI_CODIGO_WF";
	public static final String PI_RESP_BODY = "PI_RESP_BODY";
	public static final String PO_CODMSG = "PO_CODMSG";
	public static final String PO_MSJERR = "PO_MSJERR";
	public static final String JNDI="Consultando BD MPAGDB, con JNDI=";
	public static final String TIEMPOEJECUCION="Tiempo permitido de ejecucion=";
	public static final String EJECUTANDSP="Ejecutando SP : ";
	public static final String DATOSENTRADA=" Datos de entrada:";
	public static final String DATOSSALIDA=" Datos de salida:";
	public static final String INPUT="[INPUT]";	
	public static final String OUTPUT="[OUTPUT]";	
	public static final String EXPOCURRIDA= "Excepcion ocurrida en la BD {";
	public static final String TIEMPOTOTAL= " Tiempo TOTAL Proceso: [";
	public static final String MILISEGUNDOS=" milisegundos ]";
	public static final String COMPLETARCOD= "0";
	
	public static final String EJECUTADO="E";
	public static final String FALLO="F";
	public static final String PENDIENTE="P";
	public static final String ERROR =" ERROR ";
	
				
	/*KEY ENCRIPTACION APPS*/
	public static final String KEY_REQUEST_ENCRYPT_APPS = "publickeyRequest.dat";
	public static final String KEY_REQUEST_DECRYPT_APPS = "privatekeyRequest.dat";
	public static final String KEY_RESPONSE_ENCRYPT_APPS = "publickeyResponse.dat";
	public static final String KEY_RESPONSE_DECRYPT_APPS = "privatekeyResponse.dat";
	
	// **** Caracteres
	public static final String CHAR_PARENTESIS_IZQUIERDO = "(";
	public static final String CHAR_PARENTESIS_DERECHO = ")";
	public static final String CHAR_INTERROGACION = "?";
	public static final String CHAR_COMA = ",";
	public static final String CHAR_PUNTOCOMA = ";";
	
	
	public static final String ERRORCORREO =" Error Al enviar Correo ";
	public static final String ORACLEDRIVER = "oracle.jdbc.driver.OracleDriver";
	public static final String CANNOT_GET_CONNECTION = "Cannot get connection: ";

    public static final String EXITO = "0";
    public static final String TIPOIDENTIFICADORMOVIL = "02";
    public static final String TIPOIDENTIFICADORCUSTOMER = "01";
    public static final String ADDCUSTOMER = "H";
    
    /* RESTBUtilitarios */

	public static final String MSGID2 = " msgid ";
	public static final String CORCHETE_INI = "[";
	public static final String CORCHETE_FIN = "]";
	public static final String SALTO_LINEA = "\n";
	public static final String IGUAL = "=";
	public static final String TABULAR = "\t";
	public static final String PARAMETROINPUT =" PARAMETROS [INPUT]: ";
	public static final String PARAMETROOUTPUT =" PARAMETROS [OUTPUT]: ";
	
	public static final String NOMBRE_API = "gestorOperacionesMDB";
	public static final String NOMBREWS = "$nombre_ws";
	public static final String NOMBREWS1 ="$ws";
	public static final String OPERACIONWS = "$operacion";
	
	public static final String REGISTRARTIPIFICACION	="2";
	public static final String CREARINTERACCION	="crearInteraccion";
	public static final String TIPIFICACIONFLUJO	="1";
	public static final String TIPIFICACIONSTATUS="1";
	
	public static final String TIMEOUTEXCEPTION = "Timeout";
	public static final String TIMEOUTEXCEPTION2 = "Timed out";
	
	public static final String ERRORINVOCACION = " Ocurrio un error de invocacion a WS \n ";
	public static final String LOG_URL_SOAP = "[Se invoca, metodo= {}, URL={}";
	public static final String LOG_TIEMPOS_CONEXION = "Tiempo de Timeout de Conexion: {}";
	public static final String LOG_TIEMPOS_EJECUCION = "Tiempo de Timeout de Ejecucion: {}";
	public static final String LOG_REQUEST = "Request: {}";
	
	public static final String REGISTRAROAC	="1";
	public static final String CONSULTAROAC	="3";
	public static final String CREARPAGOAC	="crearPago";
	public static final String OACFLUJO	="1";
	public static final String OACSTATUS="1";
	public static final String GESTOROPERACIONESMDB="GestorOperacionesMDB";
	
	public static final String CLASSTIMEOUTCONNECTION 				= "com.sun.xml.ws.connect.timeout";
    public static final String CLASSTIMEOUTREQUEST 					= "com.sun.xml.ws.request.timeout";
    
    public static final String ERROR_PARSEAR_XML = "Error parseando object to xml: ";
    
    public static final String ENVIOCORREOSB	="5";
	public static final String ENVIOCORREOSBMETODO	="enviarCorreoRequest";
	public static final String ENVIOCORREOSBFLUJO	="1";
	public static final String ENVIOCORREOSBSTATUS="1";
	
	public static final String TIMEOUTCEXCEPTION = "Timeout conexion (ms): ";
	public static final String TIMEOUTEJECUCION = "Timeout ejecucion (ms): ";
	
	public static final String INVOCACIONPROCEDURE	=" Se invoca PROCEDURE: {}";
	public static final String TIMEOUTCONFIG	=" timeOut configurado {}";
	public static final String PINUMTRANSACCION	="PI_NUM_TRANSACCION: [";
	public static final String CALL	="call ";
	public static final String PONUMERO	="PO_NUMERO_TRAN_DET: [";
	public static final String POCODMSG	="PO_CODMSG: [";
	public static final String POMSJ	="PO_MSJERR: [";
	public static final String ERROREXEPTION	="ERROR: [Exception] - [";
	public static final String ERROREXEPTIONBD	="Excepcion ocurrida en la BD: ";
	public static final String ERROREXEPTIOLANZADA	="Excepcion lanzada ";
	public static final String ERROREXCEPTION =" ERROR: [Exception] - [";
	public static final String	NOMBRE_SP= "$nombre_SP";
	public static final String PENDIENTEDES="PENDIENTE";

	public static final String EXITOMENSAJE = "OK";
	public static final String LINEA = " LINEA: ";
	
	public static final String ERROR_VISA = "errorVisa";
	public static final String STRING_PALOTE 	= "\\|";
	public static final String CADENA_PALOTE 								= "|";
	public static final String STRING_GUION_ABAJO							= "_";
	public static final String STRING_GUION_ARRIBA							= "-";
	public static final String REGISTRAR_DOCUMENTOS							= "Registrar documentos";
	public static final String REGISTRAR_PAGO								= "Registrar pago OAC";
	public static final String CONSULTAR_PAGO								= "Consultar pago OAC";
	public static final String REGISTRAR_TIPIFICACION						= "Registrar tipificacion";
	public static final String ENVIAR_CORREO_ELECTRONICO					= "Enviar correo electronico";
	public static final String VERIFICARPAGO					= "Verificar Pago Motor Pagos";
	public static final String PAGOSRECIBOS							= "pagoRecibos";
	
	public static final String RESUMEN					= " Resumen ";
	public static final String RESPONSE ="Response";
	public static final String CIEN = "100";
	
	public static final String BUSCAR_T_APLICACION							= "$APLICACION";
    public static final String BUSCAR_T_NUMERO_TARJETA						= "$NUMERO_TARJETA";
    public static final String BUSCAR_T_TIPO_TARJETA						= "$TIPO_TARJETA";
    public static final String BUSCAR_T_FECHA_HORA							= "$FECHA_HORA";
    public static final String BUSCAR_T_NUMERO_OPERACION					= "$NUMERO_OPERACION";
    public static final String BUSCAR_T_MONTO								= "$MONTO";
    public static final String BUSCAR_T_NUMERO_CUENTA						= "$NUMERO_CUENTA";
    public static final String BUSCAR_T_NUMERO_RECIBOS						= "$NUMERO_RECIBOS";
    public static final String BUSCAR_T_AUTORIZACION_BANCARIA				= "$AUTORIZACION_BANCARIA";
    public static final String BUSCAR_T_CUSTOMER							= "$CUSTOMER";
    
    public static final String SALTOLINEAHTML= "<br>";
    public static final String BUSCAR_T_MONTOTOAL							= "$MONTO_TOTAL";
    public static final String BUSCAR_T_TOTALDOC							= "$TOTALDOC";
    public static final String BUSCAR_T_RECIBOREF							= "$RECIBOREF";
    public static final String BUSCAR_T_RECIBO							    = "$RECIBO";
    public static final String BUSCAR_T_RECIBOMONTO							= "$MONTOREC";
    public static final String EMAILHTML							= "$EMAIL";
    
    public static final String REGISTRARCCL	="1";
	public static final String CCLFLUJO	="1";
	public static final String CCLSTATUS="1";
	public static final String CODAPPSTATUS="1";

	public static final String OPERACION_WFREGULARDEUDA= "1";
	public static final String COMPONENTE_GOC= "1";
	
	//ESTADOS
	public static final String ESTADO_TRX_FINALIZADO= "2";
	public static final String ESTADO_TRX_FINALIZADO_CONERRORES= "3";
	
	public static final String NOMBRECAMPOAUDITORIA_IDTRANSACCION = "idTransaccion";
	public static final String NOMBRECAMPOAUDITORIA_MSGID = "msgid";
	public static final String NOMBRECAMPOAUDITORIA_TIMESTAMP = "timestamp";
	public static final String NOMBRECAMPOAUDITORIA_USERID = "userId";
	public static final String NOMBRECAMPOAUDITORIA_ACCEPT = "accept";
	public static final String IPAPLICACION = "ipAplicacion";
	
	public static final String SIGUIENTEWS = "Se invocara el siguiente servicio REST: ";
	public static final String TIPO = "Tipo: ";
	public static final String DATOSDESALIDA = "Datos de Salida:\n ";
	public static final String DATOSHEADER = "Datos del Header:\n ";
	
	public static final String CADENA_VACIA = "";
	public static final String VACIO = "";
	public static final String TM_SOLES = "1";
	public static final String TM_DOLARES = "2";
	public static final int COD_MONEDA_SOLES = 604;
	public static final int COD_MONEDA_DOLARES = 840;	
	
	public static final String WSEJECUTADO = "Servicio REST ejecutado ";
	public static final String CONST_WS_NAME = "$nombre_WS";
	public static final String CONST_WS_OPERATION = "$operacion";	
	public static final String TIEMPOINVOCACION = "Tiempo invocacion: ";
	public static final String MILIS_TXT = "milisegundos";
	
	public static final String USUARIO_AUDITORIA = "USRMOTOR";
	
}
