package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;
import javax.ws.rs.core.HttpHeaders;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;

public class HeaderRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private String idTransaccion;
	private String msgid;
	private String timestamp;
	private String userId;
	private String accept;
	private boolean errorVisa;
	
	public HeaderRequest(){
		super();
	}

	public HeaderRequest(HttpHeaders httpHeaders,String transaccion) {
		super();
//		if(null != httpHeaders.getRequestHeader(Constantes.IDTRANSACCION) 
//				&& !httpHeaders.getRequestHeader(Constantes.IDTRANSACCION).get(Constantes.CERO).isEmpty()){
//			this.idTransaccion = httpHeaders.getRequestHeader(Constantes.IDTRANSACCION).get(Constantes.CERO);
//		}else{
//			this.idTransaccion =transaccion;	
//		}	
		this.idTransaccion =transaccion;	
		this.msgid = null != httpHeaders.getRequestHeader(Constantes.MSGID)
				? httpHeaders.getRequestHeader(Constantes.MSGID).get(Constantes.CERO) : transaccion;
		this.userId = null != httpHeaders.getRequestHeader(Constantes.USERID)
				? httpHeaders.getRequestHeader(Constantes.USERID).get(Constantes.CERO) : null;
		this.accept =null != httpHeaders.getRequestHeader(Constantes.ACCEPT)
				? httpHeaders.getRequestHeader(Constantes.ACCEPT).get(Constantes.CERO) : null;
	
		if(null !=  httpHeaders.getRequestHeader(Constantes.ERROR_VISA)){
			this.errorVisa=Boolean.parseBoolean(httpHeaders.getRequestHeader(Constantes.ERROR_VISA).get(Constantes.CERO));
		}else{
			this.errorVisa=false;
		}
		this.timestamp =null != httpHeaders.getRequestHeader(Constantes.TIMESTAMP)
				? httpHeaders.getRequestHeader(Constantes.TIMESTAMP).get(Constantes.CERO) : Constantes.TEXTO_VACIO;
	}
	
	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}
    

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public boolean isErrorVisa() {
		return errorVisa;
	}

	public void setErrorVisa(boolean errorVisa) {
		this.errorVisa = errorVisa;
	}

	@Override
	public String toString() {
		return "HeaderRequest [idTransaccion=" + idTransaccion + ", msgid=" + msgid + ", timestamp=" + timestamp
				+ ", userId=" + userId + ", accept=" + accept + ", errorVisa=" + errorVisa + "]";
	}

	
	
	
	

}