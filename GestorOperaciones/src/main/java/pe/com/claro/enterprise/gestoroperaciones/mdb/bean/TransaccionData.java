package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TransaccionData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroTransaccion;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroIntento;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private  List<Proceso>  actividadFallos;

	public String getNumeroTransaccion() {
		return numeroTransaccion;
	}

	public void setNumeroTransaccion(String numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}

	public List<Proceso> getActividadFallos() {
		return actividadFallos;
	}

	public void setActividadFallos(List<Proceso> actividadFallos) {
		this.actividadFallos = actividadFallos;
	}

	public String getNumeroIntento() {
		return numeroIntento;
	}

	public void setNumeroIntento(String numeroIntento) {
		this.numeroIntento = numeroIntento;
	}

	@Override
	public String toString() {
		return "TransaccionData [numeroTransaccion=" + numeroTransaccion + ", numeroIntento=" + numeroIntento
				+ ", actividadFallos=" + actividadFallos + "]";
	}

	
	
}
