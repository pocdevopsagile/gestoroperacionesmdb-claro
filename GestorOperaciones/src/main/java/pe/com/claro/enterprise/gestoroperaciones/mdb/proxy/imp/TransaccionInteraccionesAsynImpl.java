package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;


import javax.xml.ws.BindingProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.claro.eai.crmservices.clarify.transaccioninteraccionesasync.TransaccionInteraccionesAsync;
import pe.com.claro.eai.crmservices.clarify.transaccioninteraccionesasync.TransaccionInteraccionesAsync_Service;
import pe.com.claro.eai.servicecommons.AuditType;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.TransaccionInteraccionesAsyn;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearInteraccionRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ClaroUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.JAXBUtilitarios;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;

@Service
public class TransaccionInteraccionesAsynImpl  implements TransaccionInteraccionesAsyn {

	private static final Logger LOGGER = LogManager.getLogger(TransaccionInteraccionesAsynImpl.class);

	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public AuditType crearInteraccion(String transaccion,CrearInteraccionRequest request,String flagProceso,ELKLogLegadoBean elkLegadoBean) {
		long tiempoInicio = System.currentTimeMillis();
		AuditType response = new AuditType();
		try {
			String url=propertiesExterno.transaccionInteraccionAsynUrl;
			LOGGER.info(transaccion + " [INICIO] " + propertiesExterno.transaccionInteraccionAsynNombre
					+ "- Metodo " + propertiesExterno.transaccionInteraccionOperacion);
			LOGGER.info(transaccion + " URL: " + url);

			LOGGER.info(transaccion + " REQUEST: \n" +  JAXBUtilitarios.anyObjectToXmlText(request));

			elkLegadoBean.setNumeroActividad(Double.parseDouble(Constantes.REGISTRARTIPIFICACION));
			elkLegadoBean.setNombreActividad("registrarTipificacion");
			elkLegadoBean.setDetalleActividad("Registro de Tipificacion");
			elkLegadoBean.setTipoConsulta(ELKUtil.TIPO_CONSULTA_WS);
			elkLegadoBean.setDuenoConsulta(Constantes.NOMBRE_API);
			elkLegadoBean.setOrigenConsulta(url);
			elkLegadoBean.setDetalleConsulta("registrarTipificacion");
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_REQUEST);
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(request));

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
			TransaccionInteraccionesAsync_Service transaservice = new TransaccionInteraccionesAsync_Service();
			TransaccionInteraccionesAsync port = transaservice.getTransaccionInteraccionesAsyncSOAP();

			BindingProvider bindingProvider = (BindingProvider) port;
			bindingProvider.getRequestContext().put(javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					url);
			bindingProvider.getRequestContext().put("com.sun.xml.ws.connect.timeout",
					Integer.parseInt(propertiesExterno.transaccionInteraccionAsynConexion));
			bindingProvider.getRequestContext().put("com.sun.xml.ws.request.timeout",
					Integer.parseInt(propertiesExterno.transaccionInteraccionAsynEjecucion));
			
			response = port.crearInteraccion(request.getIdTransaccion(), request.getInteraccionType(),
					request.getInteraccionPlusType(), request.getDetalleInteraccionType(), flagProceso);

			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDF);
			
			elkLegadoBean.setIdfIdtCodigo(response.getErrorCode());
			elkLegadoBean.setIdfIdtMensaje(response.getErrorMsg());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		} catch (Exception e) {
			LOGGER.error(transaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			LOGGER.error(transaccion + "Se deja Evidencia de Error en Log : - [" + e.getStackTrace() + "] ", e);
			if (e.getMessage().toUpperCase().contains(Constantes.TIMEOUTEXCEPTION.toUpperCase())
					|| e.getMessage().toUpperCase().contains(Constantes.TIMEOUTEXCEPTION2.toUpperCase())) {
				response.setErrorCode(propertiesExterno.codError3);
				response.setErrorMsg(propertiesExterno.msjError3
								.replace("$ws", propertiesExterno.transaccionInteraccionAsynNombre)
								.replace("$operacion",  propertiesExterno.transaccionInteraccionOperacion));
				response.setTxId(transaccion);
				
				elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
				elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
				elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
				
				elkLegadoBean.setIdfIdtCodigo(response.getErrorCode());
				elkLegadoBean.setIdfIdtMensaje(response.getErrorMsg());
				elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

				ELKUtil.logObjetoLegado(elkLegadoBean);
				
			} else {
				response.setErrorCode(propertiesExterno.codError4);
				response.setErrorMsg(propertiesExterno.msjError4
								.replace("$ws", propertiesExterno.transaccionInteraccionAsynNombre)
								.replace("$operacion",  propertiesExterno.transaccionInteraccionOperacion));
				response.setTxId(transaccion);
				
				elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
				elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
				elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
				
				elkLegadoBean.setIdfIdtCodigo(response.getErrorCode());
				elkLegadoBean.setIdfIdtMensaje(response.getErrorMsg());
				elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

				ELKUtil.logObjetoLegado(elkLegadoBean);
			}
		} finally {
			LOGGER.info(transaccion + "[FIN] " +propertiesExterno.transaccionInteraccionAsynNombre + "- Metodo "
					+ propertiesExterno.transaccionInteraccionOperacion);
			LOGGER.info(transaccion + " RESPONSE: \n" + JAXBUtilitarios.anyObjectToXmlText(response));
			LOGGER.info(transaccion + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
					+ " milisegundos ]");

		}

		return response;
	}
}
