package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;

import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.MotorPagos;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ClaroUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.RSAMotorPagos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.apache.commons.lang.exception.ExceptionUtils;
import java.util.Locale;

@Service
public class MotorPagosImpl  implements MotorPagos{


	private static final Logger LOGGER = LogManager.getLogger(MotorPagosImpl.class);

	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public VerificarAppResponse verificarPago(String trazabilidad, HeaderRequest header,
			VerificarAppRequest request,ELKLogLegadoBean elkLegadoBean) throws JsonProcessingException {
	
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = "[Verificar Pago] - ";
		trazabilidad = trazabilidad + nombreMetodo;
		LOGGER.info(trazabilidad + "[***** INICIO *****]");
		
		VerificarAppResponse response = null;
		String url=Constantes.TEXTO_VACIO;
		String codigoIdt4=Constantes.TEXTO_VACIO;
		String cmsgIdt4=Constantes.TEXTO_VACIO;
		
		String codigoIdt3=Constantes.TEXTO_VACIO;
		String cmsgIdt3=Constantes.TEXTO_VACIO;
		
		Integer timeoutConexion=Constantes.UNO;
		Integer timeoutEjecucion=Constantes.UNO;
		try {

			LOGGER.info(trazabilidad + " Body Request: \n" + ClaroUtil.printPrettyJSONString(request));
			
			codigoIdt4=propertiesExterno.codError4;
			cmsgIdt4=propertiesExterno.msjError4;
			
			codigoIdt3=propertiesExterno.codError3;
			cmsgIdt3=propertiesExterno.msjError3;
			
			url = propertiesExterno.verificapagoUrl;
			LOGGER.info(trazabilidad + " URL: " + url);
			timeoutConexion =Integer.parseInt( propertiesExterno.verificapagoConexion);
			timeoutEjecucion = Integer.parseInt( propertiesExterno.verificapagoEjecucion) ;
			String requestJson = ClaroUtil.printPrettyJSONString(request);			
			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON); 
			httpHeaders.set(Constantes.IDTRANSACCION, header.getIdTransaccion());
			httpHeaders.set(Constantes.MSGID, header.getMsgid());
			httpHeaders.set(Constantes.TIMESTAMP, ClaroUtil.getFechaString(new Date(), Constantes.FORMATO_TIMESTAMP));
			httpHeaders.set(Constantes.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			httpHeaders.set(Constantes.USERID, propertiesExterno.verificapagoUserid);
			LOGGER.info(trazabilidad + " Header Request: \n" + ClaroUtil.printPrettyJSONString(httpHeaders));
		
			elkLegadoBean.setNumeroActividad(Double.parseDouble(Constantes.MOTORPAGOS));
			elkLegadoBean.setNombreActividad("verificar Motor");
			elkLegadoBean.setDetalleActividad("Verificar Pago en Motor");
			elkLegadoBean.setTipoConsulta(ELKUtil.TIPO_CONSULTA_WS);
			elkLegadoBean.setDuenoConsulta(Constantes.NOMBRE_API);
			elkLegadoBean.setOrigenConsulta(url);
			elkLegadoBean.setDetalleConsulta("verificar Motor");
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_REQUEST);
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);
			elkLegadoBean.setMensaje(requestJson);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
			HttpEntity<String> httpEntity = new HttpEntity<>(requestJson, httpHeaders);

			SimpleClientHttpRequestFactory httpRequestFactory = new SimpleClientHttpRequestFactory();
			httpRequestFactory.setConnectTimeout(timeoutConexion);
			httpRequestFactory.setReadTimeout(timeoutEjecucion);

			LOGGER.info(trazabilidad + "Timeout conexion (ms):  " + timeoutConexion);
			LOGGER.info(trazabilidad + "Timeout ejecucion (ms):  " + timeoutEjecucion);
			
			RestTemplate restTemplate =new RestTemplate(httpRequestFactory);
			ResponseEntity<VerificarAppResponse> responseEntity = restTemplate.exchange(
					url, HttpMethod.POST, httpEntity,
					VerificarAppResponse.class);

			
			
			response = responseEntity.getBody();
			LOGGER.info(trazabilidad + "Servicio REST ejecutado " +response);
			
			VerificarAppResponse responseDesencrypt = new VerificarAppResponse();
			
			try {
				
				RSAMotorPagos rsaMotorPagos = new RSAMotorPagos();
				RSAPrivateKey privateKey = rsaMotorPagos.getPrivateKey(Constantes.KEY_RESPONSE_DECRYPT_APPS);
				responseDesencrypt = new ObjectMapper().readValue(rsaMotorPagos.decryptJson(response.getResponse(), privateKey),
						VerificarAppResponse.class);
			} catch (Exception e) {
				LOGGER.error(trazabilidad + "Respuesta Error Exception:\n" + e.getMessage(),e);
				String codigoError = codigoIdt4;
				String msgError=cmsgIdt4.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo);
				
				response.setCodigoRespuesta(codigoError);
				response.setMensajeError(msgError);
				
				elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
				elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
				elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
				
				elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
				elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
				elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

				ELKUtil.logObjetoLegado(elkLegadoBean);
				
				return response;
			}
			
			
			response = new VerificarAppResponse();
			response =  responseDesencrypt;
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDF);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		} catch (HttpClientErrorException e) {			
			response=new VerificarAppResponse();
			response.setCodigoRespuesta(codigoIdt4);
			response.setMensajeError(cmsgIdt4.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo));				
			LOGGER.error(trazabilidad + "Respuesta Error HttpClientErrorException:\n" + e.getResponseBodyAsString());	
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		}catch (Exception e) {
			
			response = new VerificarAppResponse();
			LOGGER.error(trazabilidad + "Respuesta Error Exception:\n" + e.getMessage());
			String trazaError = ExceptionUtils.getStackTrace(e);
			String codigoError;
			String msgError;
			
			if (trazaError.toUpperCase(Locale.getDefault()).contains(Constantes.WS_TIMEOUT_EXCEPTION)) {
				codigoError = codigoIdt3;
				msgError= cmsgIdt3.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo);
			} else {
				codigoError = codigoIdt4;
				msgError=cmsgIdt4.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo);
			}
			response.setCodigoRespuesta(codigoError);
			response.setMensajeError(msgError);
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
				
		} finally {
			LOGGER.info(trazabilidad + "[***** FIN *****]");
			try{
				LOGGER.info(trazabilidad + " RESPONSE: \n" + ClaroUtil.printPrettyJSONString(response));
			}catch(Exception ex){
				LOGGER.info(trazabilidad + "Respuesta Error Exception:\n" + ex.getMessage());
			}
			LOGGER.info(trazabilidad + "Tiempo invocacion: " + (System.currentTimeMillis() - tiempoInicio) + " milisegundos");
		}

		return response;
	}

}
