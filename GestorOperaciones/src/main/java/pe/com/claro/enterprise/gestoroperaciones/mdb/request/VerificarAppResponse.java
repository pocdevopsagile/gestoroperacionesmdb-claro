package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;

@JsonIgnoreProperties(ignoreUnknown=true)
public class VerificarAppResponse extends BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
