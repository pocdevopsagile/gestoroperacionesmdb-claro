package pe.com.claro.enterprise.gestoroperaciones.mdb.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import oracle.jdbc.OracleTypes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ActEstadoTransaccionRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.AplicacionTarjeta;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ConsultaReintentoResponseModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.Customer;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.ListaCustomer;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.RegistroMonitoreoRequestModel;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.Tarjeta;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionDetalle;
import pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean.TransaccionResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBNoDisponibleException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.exception.DBTimeoutException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;



@Repository
public class OperacionGOCImpl implements OperacionGOC{


	private static final Logger logger = Logger.getLogger(OperacionGOCImpl.class);
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Autowired
	@Qualifier("gocDS")
	private DataSource gocDS;
	
	public  void ejecutarInfo(StringBuilder storedLog){
		 if (logger.isInfoEnabled()) {
		    	logger.info(storedLog.toString());
		  }
    }
	
	@Override
	public void registrarMonitoreo(String trazabilidad,
			RegistroMonitoreoRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {
		logger.info(trazabilidad + " [INI]-[METODO: registrarMonitoreo - DAO] ");
		String mensajeLog = trazabilidad + "[registrarMonitoreo]-";
		String owner = null;
		String paquete = null;
		String procedure = null;
		String bd = null;

		long tiempoInicio = System.currentTimeMillis();
		try {
			bd = propertiesExterno.nombreGoc;
			owner = propertiesExterno.ownerGoc;
			paquete = propertiesExterno.packageGoc;
			procedure = propertiesExterno.registrarMonitoreo;
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(paquete).withProcedureName(procedure)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_NUM_INTENTO", OracleTypes.NUMBER),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			logger.info(mensajeLog + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeLog + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeLog + "Ejecutando SP : " + owner + Constantes.SEPARADOR_PUNTO + paquete
					+ Constantes.SEPARADOR_PUNTO + procedure);
			
			logger.info(mensajeLog + " Datos de entrada:");
			logger.info(mensajeLog + "[INPUT] [PI_NUM_TRANSACCION]: " + request.getIdTransaccion());
			logger.info(mensajeLog + "[INPUT] [PI_NUM_INTENTO]: " + request.getNroIntento());

			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", request.getIdTransaccion())
					.addValue("PI_NUM_INTENTO", request.getNroIntento());

			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			logger.info(mensajeLog + " Datos de salida:");
			logger.info(mensajeLog + "[OUTPUT] [PO_CODMSG]:" + resultMap.get("PO_CODMSG").toString());
			logger.info(mensajeLog + "[OUTPUT] [PO_MSJERR]:" + resultMap.get("PO_MSJERR").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_ESPACIO;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + bd + "}: {" + errorMsg + "}");

		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLog + " [FIN]-[METODO: registrarMonitoreo - DAO] ");
		}
	}
	
	@Override
	public TransaccionResponse insertarTransaccionDetalle(String mensajeTransaccion, TransaccionDetalle input){
		StringBuilder storedLog = new StringBuilder();
		TransaccionResponse respuestaSP = new TransaccionResponse();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" Insertar Transaccion Detalle DAO ");
		
	    ejecutarInfo(storedLog);
	
		String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
		StringBuilder storedProcedure = new StringBuilder();
		
		
		long tiempoInicio = System.currentTimeMillis();
		try {
			
			String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.insertarTransaccionDetalle;
		
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + input.getNumeroTransaccion() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_CODIGO_COMPONENTE : [" + input.getCodigoComponente() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_NUM_COMPONENTE : ["+ input.getNumeroComponente() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_ESTADO: [" + input.getEstado() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_INTENTO: [" + input.getIntento() + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_CODIGO_COMPONENTE", OracleTypes.NUMBER),
							new SqlParameter("PI_NUM_COMPONENTE", OracleTypes.VARCHAR),
							new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR),
							new SqlParameter("PI_INTENTO", OracleTypes.INTEGER),
							new SqlOutParameter("PO_NUMERO_TRAN_DET", OracleTypes.INTEGER),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", input.getNumeroTransaccion())
					.addValue("PI_CODIGO_COMPONENTE", input.getCodigoComponente())
					.addValue("PI_NUM_COMPONENTE", input.getNumeroComponente())
					.addValue("PI_ESTADO", input.getEstado())
					.addValue("PI_INTENTO", input.getIntento());
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			String numeroTransac=resultMap.get("PO_NUMERO_TRAN_DET").toString();
			if(null !=numeroTransac) {
				respuestaSP.setNumeroTransaccionDetale(Integer.parseInt(numeroTransac));
			}
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());

			logger.info(mensajeTransaccion + " Datos de salida:");
			
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.PONUMERO + respuestaSP.getNumeroTransaccionDetale() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");

		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}		
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: Insertar Transaccion Detalle - DAO] ");
		}
		
		return respuestaSP;
		
	}
	
	
	@Override
	public BodyResponse actualizarRecibo(String mensajeTransaccion, TransaccionDetalle input){
		StringBuilder storedLog = new StringBuilder();
		BodyResponse respuestaSP = new BodyResponse();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" Actualizar Recibo DAO ");
		
		ejecutarInfo(storedLog);
	
		String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
		StringBuilder storedProcedure = new StringBuilder();
		
		long tiempoInicio = System.currentTimeMillis();
		try {
			
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.actualizarTransaccionRecibo;		
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + input.getNumeroTransaccion() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_CUSTOMERID : ["+ input.getCustomerId() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + " PI_NUMERO_TRAN_DET : [" + input.getNumeroTransaccionDetale()+ "]");

			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_CUSTOMERID", OracleTypes.VARCHAR),
							new SqlParameter("PI_NUMERO_TRAN_DET", OracleTypes.INTEGER),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", input.getNumeroTransaccion())
					.addValue("PI_CUSTOMERID", input.getCustomerId())
					.addValue("PI_NUMERO_TRAN_DET", input.getNumeroTransaccionDetale());
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());

			logger.info(mensajeTransaccion + " Datos de salida:");
			
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}		
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: Actualizar Recibo - DAO] ");
		}
		return respuestaSP;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public ListaCustomer consultaCustomer(String mensajeTransaccion, String transaccion){
		StringBuilder storedLog = new StringBuilder();
		ListaCustomer respuestaSP = new ListaCustomer();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" Consultar Customer DAO ");
		
		ejecutarInfo(storedLog);
	
		String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
		StringBuilder storedProcedure = new StringBuilder();
		List<Customer> lista=new ArrayList<>();
		
		long tiempoInicio = System.currentTimeMillis();
		try {
			 
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.buscarTransaccionRecibo;
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + transaccion + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CURSOR",OracleTypes.CURSOR, new RowMapper<Customer>() {
								@Override
								public Customer mapRow(ResultSet rs, int arg1) throws SQLException {
									Customer cust = new Customer();
									cust.setCustomerId(rs.getString("V_CUSTOMERID"));
									return cust;
								}
							}),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", transaccion);
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());
			
			
			logger.info(mensajeTransaccion + " Datos de salida:");
			
			if(Constantes.CODIGO_EXITO.equals(respuestaSP.getCodigoRespuesta())) {
				lista = (List<Customer>) resultMap.get("PO_CURSOR");
				logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + " Lista "+ lista.size() + "]");
			}
			respuestaSP.setLista(lista);

			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}			
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: Consultar Customer - DAO] ");
		}
		return respuestaSP;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ListaCustomer consultaCustomerAP(String mensajeTransaccion, String transaccion,String estado,String componente,String intento){
		StringBuilder storedLog = new StringBuilder();
		ListaCustomer respuestaSP = new ListaCustomer();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" Consultar Customer DAO ");
		
		ejecutarInfo(storedLog);
	
		String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
		StringBuilder storedProcedure = new StringBuilder();
		List<Customer> lista=new ArrayList<>();
		
		long tiempoInicio = System.currentTimeMillis();
		try {
			 
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.buscarApTransaccionRecibo;
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + transaccion + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_ESTADO: ["+ transaccion + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_COMPONENTE: [" + transaccion + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_INTENTO: [" + transaccion + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR),
							new SqlParameter("PI_COMPONENTE", OracleTypes.NUMBER),
							new SqlParameter("PI_INTENTO", OracleTypes.NUMBER),
							new SqlOutParameter("PO_CURSOR",OracleTypes.CURSOR, new RowMapper<Customer>() {
								@Override
								public Customer mapRow(ResultSet rs, int arg1) throws SQLException {
									Customer cust = new Customer();
									cust.setCustomerId(rs.getString("V_CUSTOMERID"));
									return cust;
								}
							}),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", transaccion)
					.addValue("PI_ESTADO", estado)
					.addValue("PI_COMPONENTE", componente)
					.addValue("PI_INTENTO", intento);
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());
			
			
			logger.info(mensajeTransaccion + " Datos de salida:");
			
			if(Constantes.CODIGO_EXITO.equals(respuestaSP.getCodigoRespuesta())) {
				lista = (List<Customer>) resultMap.get("PO_CURSOR");
				logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + " Lista "+ lista.size() + "]");
			}
			respuestaSP.setLista(lista);

			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}			
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: Consultar Customer - DAO] ");
		}
		return respuestaSP;
		
	}
	
	@Override
	public BodyResponse insertarRequest(String mensajeTransaccion, String numeroTransaccion, String cComponent,
			String cFlujo, String body){
		StringBuilder storedLog = new StringBuilder();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" InsertarRequest DAO ");
		
		ejecutarInfo(storedLog);
		
	    BodyResponse respuestaSP = new BodyResponse();
		
	    String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
	
		StringBuilder storedProcedure = new StringBuilder();

		long tiempoInicio = System.currentTimeMillis();
		try {
			
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.insertarRequest;
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);
			
			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + numeroTransaccion + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_COD_COMPONENTE: [" + cComponent + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_CODIGO_WF: [" + cFlujo + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_RESP_BODY: [" + body + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_COD_COMPONENTE", OracleTypes.VARCHAR),
							new SqlParameter("PI_CODIGO_WF", OracleTypes.INTEGER),
							new SqlParameter("PI_RESP_BODY", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", numeroTransaccion)
					.addValue("PI_COD_COMPONENTE", cComponent)
					.addValue("PI_CODIGO_WF", cFlujo)
					.addValue("PI_RESP_BODY", body);
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());

			logger.info(mensajeTransaccion + " Datos de salida:");
			
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}			
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: InsertarRequest - DAO] ");
		}
		return respuestaSP;
		
	}
	
	@Override
	public BodyResponse insertarResponse(String mensajeTransaccion, String numeroTransaccion, String cComponent,
			String cFlujo, String body){
		StringBuilder storedLog = new StringBuilder();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" InsertarResponse DAO ");
		
		ejecutarInfo(storedLog);
		
	    BodyResponse respuestaSP = new BodyResponse();
	    StringBuilder storedProcedure = new StringBuilder();
	    
	    String dbbscs = Constantes.TEXTO_VACIO;
  		String ownerbscs = Constantes.TEXTO_VACIO;
  		String packagebscs = Constantes.TEXTO_VACIO;
  		String sp = Constantes.TEXTO_VACIO;
		
  		long tiempoInicio = System.currentTimeMillis();
		try {
			
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.insertResponse;
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);		

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + Constantes.PINUMTRANSACCION + numeroTransaccion + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_COD_COMPONENTE: [" + cComponent + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_CODIGO_WF: [" + cFlujo + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_RESP_BODY: [" + body + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_COD_COMPONENTE", OracleTypes.VARCHAR),
							new SqlParameter("PI_CODIGO_WF", OracleTypes.INTEGER),
							new SqlParameter("PI_RESP_BODY", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", numeroTransaccion)
					.addValue("PI_COD_COMPONENTE", cComponent)
					.addValue("PI_CODIGO_WF", cFlujo)
					.addValue("PI_RESP_BODY", body);
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());

			logger.info(mensajeTransaccion + " Datos de salida:");
			
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}			
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: InsertarResponse - DAO] ");
		}
		return respuestaSP;
		
	}
	
	@Override
	public ConsultaReintentoResponseModel consultarReintento(String trazabilidad,
			ConsultaReintentoRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {
		logger.info(trazabilidad + " [INI]-[METODO: consultarReintento - DAO] ");
		String mensajeLog = trazabilidad + "[consultarReintento]-";
		String dbbscs = Constantes.TEXTO_VACIO;
  		String ownerbscs = Constantes.TEXTO_VACIO;
  		String packagebscs = Constantes.TEXTO_VACIO;
  		String sp = Constantes.TEXTO_VACIO;
  		String mensaje1 = Constantes.TEXTO_VACIO;
  		String mensaje2 = Constantes.TEXTO_VACIO;
		ConsultaReintentoResponseModel response = null;

		long tiempoInicio = System.currentTimeMillis();
		try {
			
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.consultarReintento;
			 
			 mensaje1=propertiesExterno.codError1.replace(Constantes.NOMBRE_SP, sp);
			 mensaje2=propertiesExterno.codError2.replace(Constantes.NOMBRE_SP, sp);
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_COMPONENTE", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			logger.info(mensajeLog + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeLog + "Tiempo permitido de ejecucion=" + timeOut);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeLog + "Ejecutando SP : " + ownerbscs + Constantes.SEPARADOR_PUNTO + packagebscs
					+ Constantes.SEPARADOR_PUNTO + sp);
			
			logger.info(mensajeLog + " Datos de entrada:");
			logger.info(mensajeLog + "[INPUT] [PI_NUM_TRANSACCION]: " + request.getNroTransaccion());
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", request.getNroTransaccion());

			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			logger.info(mensajeLog + " Datos de salida:");
			logger.info(mensajeLog + "[OUTPUT] [PO_COMPONENTE]:" + resultMap.get("PO_COMPONENTE"));
			logger.info(mensajeLog + "[OUTPUT] [PO_CODMSG]:" + resultMap.get("PO_CODMSG").toString());
			logger.info(mensajeLog + "[OUTPUT] [PO_MSJERR]:" + resultMap.get("PO_MSJERR").toString());

			response = new ConsultaReintentoResponseModel();
			response.setCodigoRespuesta(Integer.parseInt(resultMap.get("PO_CODMSG").toString()));
			response.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());
			
			if(Integer.parseInt(resultMap.get("PO_CODMSG").toString())==Constantes.CERO) {
				
				response.setCodComponente(resultMap.get("PO_COMPONENTE").toString());
				
			}
			
		} catch (Exception e) {
			response = new ConsultaReintentoResponseModel();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeLog + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+sp +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				response.setMensajeRespuesta(mensaje1);
				response.setCodigoRespuesta(Integer.parseInt(propertiesExterno.codError1));
			} else {
				logger.error(mensajeLog + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+sp +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				response.setMensajeRespuesta(mensaje2);
				response.setCodigoRespuesta(Integer.parseInt(propertiesExterno.codError2));
			}		
		}  finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLog + " [FIN]-[METODO: consultarReintento - DAO] ");
		}
		
		return response;
	}
	
	@Override
	public void actualizarEstadoTransaccion(String trazabilidad, PropertiesExterno properties,
			ActEstadoTransaccionRequestModel request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {
		logger.info(trazabilidad + " [INI]-[METODO: actualizarEstadoTransaccion - DAO] ");
		String mensajeLog = trazabilidad + "[actualizarEstadoTransaccion]-";
		String dbbscs = Constantes.TEXTO_VACIO;
  		String ownerbscs = Constantes.TEXTO_VACIO;
  		String packagebscs = Constantes.TEXTO_VACIO;
  		String sp = Constantes.TEXTO_VACIO;

		long tiempoInicio = System.currentTimeMillis();
		try {
			
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.actualizarEstado;
			 
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_NUM_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODMSG", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			logger.info(mensajeLog + "Consultando BD MPAGDB, con JNDI=" + properties.jndiGoc);
			logger.info(mensajeLog + "Tiempo permitido de ejecucion=" + timeOut);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(timeOut));
			logger.info(mensajeLog + "Ejecutando SP : " + ownerbscs + Constantes.SEPARADOR_PUNTO + packagebscs
					+ Constantes.SEPARADOR_PUNTO + sp);
			
			logger.info(mensajeLog + " Datos de entrada:");
			logger.info(mensajeLog + "[INPUT] [PI_NUM_TRANSACCION]: " + request.getIdTransaccion());
			logger.info(mensajeLog + "[INPUT] [PI_ESTADO]: " + request.getEstado());

			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_NUM_TRANSACCION", request.getIdTransaccion())
					.addValue("PI_ESTADO", request.getEstado());

			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			logger.info(mensajeLog + " Datos de salida:");
			logger.info(mensajeLog + "[OUTPUT] [PO_CODMSG]:" + resultMap.get("PO_CODMSG").toString());
			logger.info(mensajeLog + "[OUTPUT] [PO_MSJERR]:" + resultMap.get("PO_MSJERR").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + dbbscs + "}: {" + errorMsg + "}");

		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLog + " [FIN]-[METODO: actualizarEstadoTransaccion - DAO] ");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AplicacionTarjeta consultarAplicacion(String mensajeTransaccion, String identificador, String tarjeta){
		StringBuilder storedLog = new StringBuilder();
		AplicacionTarjeta respuestaSP = new AplicacionTarjeta();
		storedLog.append(mensajeTransaccion);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(Constantes.INICIO);
		storedLog.append(Constantes.ESPACIO);
		storedLog.append(" Consultar Apliacion DAO ");
		
		ejecutarInfo(storedLog);
	
		String dbbscs = Constantes.TEXTO_VACIO;
		String ownerbscs = Constantes.TEXTO_VACIO;
		String packagebscs = Constantes.TEXTO_VACIO;
		String sp = Constantes.TEXTO_VACIO;
		StringBuilder storedProcedure = new StringBuilder();
		List<Tarjeta> lista=new ArrayList<>();
		
		long tiempoInicio = System.currentTimeMillis();
		try {
			 
			 String timeOut = propertiesExterno.ejecucionTimeout;
			 dbbscs = propertiesExterno.nombreGoc;
			 ownerbscs = propertiesExterno.ownerGoc;
			 packagebscs = propertiesExterno.packageGoc;
			 sp = propertiesExterno.consultarApliacion;
			
			storedProcedure.append(ownerbscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(packagebscs);
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
			storedProcedure.append(sp);

			logger.info(mensajeTransaccion + Constantes.INVOCACIONPROCEDURE+storedProcedure);
			logger.info(mensajeTransaccion + Constantes.TIMEOUTCONFIG+ timeOut);		
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_IDENTIFICADOR: ["+ identificador + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROINPUT + "PI_DESCR_TARJETA: ["+ tarjeta + "]");
			
			gocDS.setLoginTimeout(Integer.parseInt(propertiesExterno.coneccionTimeout));
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(gocDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(ownerbscs).withCatalogName(packagebscs).withProcedureName(sp)
					.declareParameters(new SqlParameter("PI_IDENTIFICADOR", OracleTypes.VARCHAR),
							new SqlParameter("PI_DESCR_TARJETA", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CURSOR",OracleTypes.CURSOR, new RowMapper<Tarjeta>() {
								@Override
								public Tarjeta mapRow(ResultSet rs, int arg1) throws SQLException {
									Tarjeta cust = new Tarjeta();
									cust.setCanal(rs.getString("APLIV_CANAL"));
									cust.setCodigoCanal(rs.getString("APLIV_CODIGO"));
									cust.setCodigoTarjeta(rs.getString("TARJN_CODIGO"));
									cust.setDescripcion(rs.getString("APLIV_DESCRIPCION"));
									cust.setMedioPago(rs.getString("APLIV_MEDIO_PAGO"));
									cust.setCodigoCanalOAC(rs.getString("APLIV_COD_CANAL_OAC"));
									cust.setEmpleadoTipi(rs.getString("APLIV_EMPLEADO"));
									cust.setMetodoTipi(rs.getString("APLIV_METODO"));
									return cust;
								}
							}),
							new SqlOutParameter("PO_CODMSG", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJERR", OracleTypes.VARCHAR));
			
			logger.info(mensajeTransaccion + "Consultando BD MPAGDB, con JNDI=" + propertiesExterno.jndiGoc);
			logger.info(mensajeTransaccion + "Tiempo permitido de ejecucion=" + propertiesExterno.ejecucionTimeout);
			jdbcCall.getJdbcTemplate().setQueryTimeout(Integer.parseInt(propertiesExterno.ejecucionTimeout));
			logger.info(mensajeTransaccion + "Ejecutando SP : " +storedProcedure);
			
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_IDENTIFICADOR", identificador)
					.addValue("PI_DESCR_TARJETA", tarjeta);
			
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);
			
			respuestaSP.setCodigoRespuesta(resultMap.get("PO_CODMSG").toString());
			respuestaSP.setMensajeRespuesta(resultMap.get("PO_MSJERR").toString());
			
			
			logger.info(mensajeTransaccion + " Datos de salida:");
			
			
			if(respuestaSP.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
				lista = (List<Tarjeta>) resultMap.get("PO_CURSOR");
				logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT +"Lista "+ lista.size() + "]");
			}
			respuestaSP.setListTarjeta(lista);

			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POCODMSG + respuestaSP.getCodigoRespuesta() + "]");
			logger.info(mensajeTransaccion + Constantes.PARAMETROOUTPUT + Constantes.POMSJ + respuestaSP.getMensajeRespuesta() + "]");
			
			
		}catch (Exception e) {

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError=String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getClass().toString());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError1.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError1);
			} else {
				logger.error(mensajeTransaccion + Constantes.ERROREXEPTIONBD+ dbbscs+ Constantes.ESPACIO+storedProcedure.toString() +Constantes.ESPACIO+ e.getMessage());
				logger.error(Constantes.ERROREXEPTIOLANZADA + e.getLocalizedMessage());
				respuestaSP.setMensajeRespuesta(propertiesExterno.msjError2.replace(Constantes.NOMBRE_SP, storedProcedure.toString()));
				respuestaSP.setCodigoRespuesta(propertiesExterno.codError2);
			}			
		} finally {
			logger.info(mensajeTransaccion + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeTransaccion + " [FIN]-[METODO: Consultar Apliacion - DAO] ");
		}
		return respuestaSP;
		
	}

	
}
