package pe.com.claro.enterprise.gestoroperaciones.mdb.exception;

public class BDException extends Exception{

	private static final long serialVersionUID = -7482288873992395827L;
	private final String codigo;
	private final String mensaje;
	
	public  BDException(String codigo,String mensaje,Exception e){
		super(e);
		this.codigo=codigo;
		this.mensaje=mensaje;
	}
	
	public  BDException(String codigo,String mensaje){
		this.codigo=codigo;
		this.mensaje=mensaje;
	}

	public String getCodigo() {
		return codigo;
	}
	public String getMensaje() {
		return mensaje;
	}

}
