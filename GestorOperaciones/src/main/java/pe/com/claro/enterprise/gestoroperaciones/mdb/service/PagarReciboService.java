package pe.com.claro.enterprise.gestoroperaciones.mdb.service;

import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;

public interface PagarReciboService {

	public void pagarRecibo(String nroTransaccion,PagarReciboRequest request );
}
