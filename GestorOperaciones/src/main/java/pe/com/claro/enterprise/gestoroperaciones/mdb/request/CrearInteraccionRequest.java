package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import pe.com.claro.eai.crm.clarify.InteraccionPlusType;
import pe.com.claro.eai.crm.clarify.InteraccionType;
import pe.com.claro.eai.crmservices.clarify.transaccioninteraccionesasync.DetalleInteraccionType;

public class CrearInteraccionRequest{
	
	private String idTransaccion;
	private InteraccionType interaccionType;
	private InteraccionPlusType interaccionPlusType;
	private DetalleInteraccionType detalleInteraccionType;
	private String flagProceso;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public InteraccionType getInteraccionType() {
		return interaccionType;
	}
	public void setInteraccionType(InteraccionType interaccionType) {
		this.interaccionType = interaccionType;
	}
	public InteraccionPlusType getInteraccionPlusType() {
		return interaccionPlusType;
	}
	public void setInteraccionPlusType(InteraccionPlusType interaccionPlusType) {
		this.interaccionPlusType = interaccionPlusType;
	}
	public DetalleInteraccionType getDetalleInteraccionType() {
		return detalleInteraccionType;
	}
	public void setDetalleInteraccionType(DetalleInteraccionType detalleInteraccionType) {
		this.detalleInteraccionType = detalleInteraccionType;
	}
	public String getFlagProceso() {
		return flagProceso;
	}
	public void setFlagProceso(String flagProceso) {
		this.flagProceso = flagProceso;
	}
	
	
}
