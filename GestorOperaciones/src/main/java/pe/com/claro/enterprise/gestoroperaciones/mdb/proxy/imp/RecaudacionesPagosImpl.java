package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;

import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.RecaudacionesPagos;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.ConsultaPagoOACRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ClaroUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;

@Service
public class RecaudacionesPagosImpl implements RecaudacionesPagos {

	private static final Logger LOGGER = LogManager.getLogger(RecaudacionesPagosImpl.class);

	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public BodyResponse consultarPago(String trazabilidad,
			ConsultaPagoOACRequest request,ELKLogLegadoBean elkLegadoBean) throws JsonProcessingException {
	
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = "[consultar Pago] - ";
		trazabilidad = trazabilidad + nombreMetodo;
		LOGGER.info(trazabilidad + "[***** INICIO *****]");
		
		BodyResponse response = null;
		String url=Constantes.TEXTO_VACIO;
		String codigoIdt4=Constantes.TEXTO_VACIO;
		String cmsgIdt4=Constantes.TEXTO_VACIO;
		
		String codigoIdt3=Constantes.TEXTO_VACIO;
		String cmsgIdt3=Constantes.TEXTO_VACIO;
		
		Integer timeoutConexion=Constantes.UNO;
		Integer timeoutEjecucion=Constantes.UNO;
		try {

			LOGGER.info(trazabilidad + " Body Request: \n" + ClaroUtil.printPrettyJSONString(request));
			
			codigoIdt4=propertiesExterno.codError4;
			cmsgIdt4=propertiesExterno.msjError4;
			
			codigoIdt3=propertiesExterno.codError3;
			cmsgIdt3=propertiesExterno.msjError3;
			
			url = propertiesExterno.recaudacionesPagosUrl;
			
			String urlGet = url
					+"?transaccionId="+request.getTransaccionId()
					+"&codAplicacion="+request.getCodAplicacion()
					+"&trace="+request.getTrace()
					+"&nroOperacion="+request.getNroOperacion();
			
			LOGGER.info(trazabilidad + " URL: " + urlGet);
			timeoutConexion =Integer.parseInt( propertiesExterno.recaudacionesPagosConexion);
			timeoutEjecucion = Integer.parseInt( propertiesExterno.recaudacionesPagosEjecucion) ;
			String requestJson = ClaroUtil.printPrettyJSONString(request);			
			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON); 
			httpHeaders.set(Constantes.IDTRANSACCION, request.getTransaccionId());
			httpHeaders.set(Constantes.MSGID,  request.getTransaccionId());
			httpHeaders.set(Constantes.TIMESTAMP, ClaroUtil.getFechaString(new Date(), Constantes.FORMATO_TIMESTAMP));
			httpHeaders.set(Constantes.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			httpHeaders.set(Constantes.USERID, propertiesExterno.verificapagoUserid);
			LOGGER.info(trazabilidad + " Header Request: \n" + ClaroUtil.printPrettyJSONString(httpHeaders));
		
			elkLegadoBean.setNumeroActividad(Double.parseDouble(Constantes.MOTORPAGOS));
			elkLegadoBean.setNombreActividad("consultar Pago");
			elkLegadoBean.setDetalleActividad("consultar Pago OAC");
			elkLegadoBean.setTipoConsulta(ELKUtil.TIPO_CONSULTA_WS);
			elkLegadoBean.setDuenoConsulta(Constantes.NOMBRE_API);
			elkLegadoBean.setOrigenConsulta(url);
			elkLegadoBean.setDetalleConsulta("consultar Pago");
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_REQUEST);
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);
			elkLegadoBean.setMensaje(requestJson);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
			HttpEntity<String> httpEntity = new HttpEntity<>(requestJson, httpHeaders);

			SimpleClientHttpRequestFactory httpRequestFactory = new SimpleClientHttpRequestFactory();
			httpRequestFactory.setConnectTimeout(timeoutConexion);
			httpRequestFactory.setReadTimeout(timeoutEjecucion);

			LOGGER.info(trazabilidad + "Timeout conexion (ms):  " + timeoutConexion);
			LOGGER.info(trazabilidad + "Timeout ejecucion (ms):  " + timeoutEjecucion);
			
			RestTemplate restTemplate =new RestTemplate(httpRequestFactory);
			ResponseEntity<BodyResponse> responseEntity = restTemplate.exchange(
					urlGet, HttpMethod.GET, httpEntity,
					BodyResponse.class);

			LOGGER.info(trazabilidad + "Servicio REST ejecutado " +responseEntity.getBody());
			
			response = responseEntity.getBody();	

			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDF);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		} catch (HttpClientErrorException e) {			
			response=new BodyResponse();
			response.setCodigoRespuesta(codigoIdt4);
			response.setMensajeError(cmsgIdt4.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo));				
			LOGGER.error(trazabilidad + "Respuesta Error HttpClientErrorException:\n" + e.getResponseBodyAsString());	
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		}catch (Exception e) {
			
			response = new BodyResponse();
			LOGGER.error(trazabilidad + "Respuesta Error Exception:\n" + e.getMessage());
			String trazaError = ExceptionUtils.getStackTrace(e);
			String codigoError;
			String msgError;
			
			if (trazaError.toUpperCase(Locale.getDefault()).contains(Constantes.WS_TIMEOUT_EXCEPTION)) {
				codigoError = codigoIdt3;
				msgError= cmsgIdt3.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo);
			} else {
				codigoError = codigoIdt4;
				msgError=cmsgIdt4.replace(Constantes.NOMBREWS1, url).replace(Constantes.OPERACIONWS, nombreMetodo);
			}
			response.setCodigoRespuesta(codigoError);
			response.setMensajeError(msgError);
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
			
			elkLegadoBean.setIdfIdtCodigo(response.getCodigoRespuesta());
			elkLegadoBean.setIdfIdtMensaje(response.getMensajeRespuesta());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
				
		} finally {
			LOGGER.info(trazabilidad + "[***** FIN *****]");
			try{
				LOGGER.info(trazabilidad + " RESPONSE: \n" + ClaroUtil.printPrettyJSONString(response));
			}catch(Exception ex){
				LOGGER.info(trazabilidad + "Respuesta Error Exception:\n" + ex.getMessage());
			}
			LOGGER.info(trazabilidad + "Tiempo invocacion: " + (System.currentTimeMillis() - tiempoInicio) + " milisegundos");
		}

		return response;
	}
}
