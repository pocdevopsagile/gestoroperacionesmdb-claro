package pe.com.claro.enterprise.gestoroperaciones.mdb.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;


@Component
public class RSAMotorPagos {

	/**
	 * Constructs a public void to Save Key to File
	 *
	 * @param Key PEM Private Key
	 * @param filename PEM Private Key
	 * @throws Exception
	 */
	public void generateKeys(String name) throws Exception {		
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		/*PUBLIC KEY*/
		PublicKey publicKey= keyPair.getPublic();
		String fileName = "publickey"+name+".dat";
		byte[] publicKeyBytes = Base64.encodeBase64(publicKey.getEncoded());
		FileOutputStream fos = new FileOutputStream(fileName);
		fos.write(publicKeyBytes);
		fos.close();
		/*PRIVATE KEY*/
		PrivateKey privateKey = keyPair.getPrivate();
		fileName = "privatekey"+name+".dat";
		publicKeyBytes = Base64.encodeBase64(privateKey.getEncoded());
		fos = new FileOutputStream(fileName);
		fos.write(publicKeyBytes);
		fos.close();		
	}
	
	public String getKey(String filename) throws IOException {
		// Read key from file
		Charset charset = StandardCharsets.UTF_8;
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filename);
		
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset))) {	
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
		}
	 
		return stringBuilder.toString();
	}

	/**
	 * Constructs a private key (RSA) from the given file
	 *
	 * @param filename PEM Private Key
	 * @return RSA Private Key
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public RSAPrivateKey getPrivateKey(String filename) throws IOException, GeneralSecurityException {
		String privateKeyPEM = getKey(filename);
		return getPrivateKeyFromString(privateKeyPEM);
	}

	/**
	 * Constructs a private key (RSA) from the given string
	 *
	 * @param privateKeyPEM PEM Private Key
	 * @return RSA Private Key
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	private RSAPrivateKey getPrivateKeyFromString(String privateKeyPEM2) throws IOException, GeneralSecurityException {
		String privateKeyPEM = privateKeyPEM2;

		// Remove the first and last lines
		privateKeyPEM = privateKeyPEM.replace("-----BEGIN PRIVATE KEY-----n", "");
		privateKeyPEM = privateKeyPEM.replace("-----END PRIVATE KEY-----", "");

		// Base64 decode data
		byte[] encoded = Base64.decodeBase64(privateKeyPEM);

		KeyFactory kf = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
		RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
		return privKey;
	}

	/**
	 * Constructs a public key (RSA) from the given file
	 *
	 * @param filename PEM Public Key
	 * @return RSA Public Key
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public RSAPublicKey getPublicKey(String filename) throws IOException, GeneralSecurityException {
		String publicKeyPEM = getKey(filename);
		return getPublicKeyFromString(publicKeyPEM);
	}

	/**
	 * Constructs a public key (RSA) from the given string
	 *
	 * @param key PEM Public Key
	 * @return RSA Public Key
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	private static RSAPublicKey getPublicKeyFromString(String key) throws IOException, GeneralSecurityException {
		String publicKeyPEM = key;

		// Remove the first and last lines
		publicKeyPEM = publicKeyPEM.replace("-----BEGIN PUBLIC KEY-----n", "");
		publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");

		// Base64 decode data
		byte[] encoded = Base64.decodeBase64(publicKeyPEM);

		KeyFactory kf = KeyFactory.getInstance("RSA");

		KeySpec keySpec = new X509EncodedKeySpec(encoded);
		RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpec);
		return pubKey;
	}

	/**
	 * Encrypts the text with the public key (RSA)
	 *
	 * @param rawText Text to be encrypted
	 * @param publicKey
	 * @return
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	private String encrypt(String rawText, RSAPublicKey publicKey) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return Base64.encodeBase64String(cipher.doFinal(rawText.getBytes("UTF-8")));
	}

	public String encryptJson(String textToEncrypt, RSAPublicKey publicKey)
			throws IOException, GeneralSecurityException {
		String encryptResponse = "";
		int length = 117;//new for 2018 bytes
		//int length = 245;//for 2018 bytes original
		//int length = 177;//for 1024 bytes
		int count = textToEncrypt.length() / length;
		int diff = textToEncrypt.length() % length;
		int maximo = 0;
		String textProcess = null;
		for (int i = 1; i <= count; i++) {
			if (i == 1) {
				textProcess = textToEncrypt.substring(0, length);
				encryptResponse = encryptResponse + encrypt(textProcess, publicKey);
			} else {
				textProcess = textToEncrypt.substring(maximo - 1, i * length);
				encryptResponse = encryptResponse + encrypt(textProcess, publicKey);
			}
			maximo = i * length + 1;
		}
		if (diff > 0 && count != 0) {
			textProcess = textToEncrypt.substring(maximo - 1, textToEncrypt.length());
			encryptResponse = encryptResponse + encrypt(textProcess, publicKey);
		}else if (diff > 0 && count == 0) {
			textProcess = textToEncrypt.substring(0, textToEncrypt.length());
			encryptResponse = encryptResponse + encrypt(textProcess, publicKey);
		}
		return encryptResponse;
	}

	/**
	 * Decrypts the text with the private key (RSA)
	 *
	 * @param cipherText
	 *            Text to be decrypted
	 * @param privateKey
	 * @return Decrypted text (Base64 encoded)
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	private String decrypt(String cipherText, PrivateKey privateKey) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return new String(cipher.doFinal(Base64.decodeBase64(cipherText)), "UTF-8");
	}

	public String decryptJson(String decryptToText, RSAPrivateKey privateKey)
			throws IOException, GeneralSecurityException {
		String decryptResponse = "";
		int length = 344;//for 2048 bytes original
		//int length = 172;//for 1024 bytes
		int count = decryptToText.length() / length;
		int diff = decryptToText.length() % length;
		int maximo = 0;
		String textProcess;
		for (int i = 1; i <= count; i++) {
			if (i == 1) {
				textProcess = decryptToText.substring(0, length);
				decryptResponse = decryptResponse + decrypt(textProcess, privateKey);
			} else {
				textProcess = decryptToText.substring(maximo - 1, i * length);
				decryptResponse = decryptResponse + decrypt(textProcess, privateKey);
			}
			maximo = i * length + 1;
		}		
		if (diff > 0 && count != 0) {
			textProcess = decryptToText.substring(maximo - 1, decryptToText.length());
			decryptResponse = decryptResponse + decrypt(textProcess, privateKey);
		}else if (diff > 0 && count == 0) {
			textProcess = decryptToText.substring(0, decryptToText.length());
			decryptResponse = decryptResponse + decrypt(textProcess, privateKey);
		}
		
		return decryptResponse;
	}
	
	public static void main(String[] args) {
		RSAMotorPagos rsaMotorPagos = new RSAMotorPagos();
		
	

		//REQUEST VERIFICAR
		String requestTexto = "{\n" + 
		"	\"codigoComercio\" : \"342062522\",\n" + 
		"	\"codigoPortal\" : 1,\n" +
		"	\"linea\" : \"957269940\",\n" +
		"	\"numeroOrdenPortal\" : \"668357951\",\n" +
		"	\"numeroOrdenMotor\" : 24736,\n" + 
		"	\"medioPago\" : \"3\",\n" +
		"	\"estadoOrden\" : 9\n" +
		"}";

	
		try {
			String requestEncriptado= "ttl0o7XN8Rh2G+xuUoHR28JObQLckl5cuqbX6mb8PlKfbKsj9wpQQnuk4rXz5MQDYHUP0OhWAz/IoSdKxPpYmwehNUKPA54H3aFOytibgxUX94NzW3nBeukUGBymA1dgiiiLERwFlRuzo6sQ4hPmPhXU7KMZovSYgRE6UULcBYsz6OImYbQdjUv/paLBAN+vk83hLr+l9CICnMtyt1Hl1QNo7gHojySoB3Q6edXqshdtoDxI2cpErhXpc1FT1/xzML5oH9VGAJW/rd/TdIJvkBmOrbVys732p7DYwT30sqgjWTk8lJmYCwVAjJRJPBLFjBzEeZOIHUjvZvxsxexYgQ==Bxg2su19ULXyLFAGfLsx27mJnFBWy83RIVon4Kln1qbbYGglxHlvN1W//8Ql5DJADpUdjNGtZZmOKar18OejQ2Z04Hu5Bq9u/sb2uOQp3sjhSGc9xpuWmxUBm1S8zFq8XI6+U2sRBoK6IfNS/BeKsD7UwjRfGfAhjNyfc5jtp+Cc+JyirVx2bYkWmV9MD8E4yNgrD7vXI6LasBk/LyUXuqt0Wx9b2lOw+NwP67buGrvEXAv2eDCpRACTh+Do/fXkc6GOj7RgfxRSV35c9hLNjxrw2W4CQWLjjPk7PDiB6O7vJQT0wwsP6A8acQSMNwLD2gOJPfEM/RAN5XMYvcikOw==hI/1fF5qkdKqx1c/XoDqqA96ql8UrJjMDXTMIx0tpGVLU2ac9vLfAzNlDBTNkRsLXItzwcidobAbif6TFK+xw00cMSd2siDYOv1W6eP3WGTyP9Z7OYmkMXxiWHyC5SJO3bzdlI+bFAGYs/OZYFeoryaZbvV9cNh7S1ODhqP7cWfmJBliqWaAPDBqMZD7g9UoEwSnT1Lz03nwgwAmrW+54jDYzTC02hetJTKRvqNVK/r6UdAqRFCAi1G3nDlYObASlBFiHFCWitzfqq75XW1MXHADnXY6B0b/kmvrMumcKet0F5qZHMTPR2M54vxZuW3cXrYt4Q+HxdZ47lOAXqrwLg==fJXUk1Tf27d4KcGC0BGBPLLB1uobmfjZFyJlFE75nIM8+pPVmxcGrYzxZUkL2SziJ5Y8byG1WRkmkwSez63hVxUXlPQN7+Au5IoDv6Quzh/6/y/gEVNeqXhwlAxqGdE/GlgJZw9qbnlE6muvS+7sa9j45Ld9bPUKJsfAqetuyHqqoURTuWb1o1JGuVeH8VLBfcvcnwlkOkEG3fFXmmYaOwyuzE+CL1ricJXd/aSagTXgzjDBVCE2czx2zL/Ez33it7bLbzu9eg7ck5PSNTBrmS/Y2nCKSLOp9CMNh+dkpK/6zSwsK6aoAwjxHluqQGvNkxQfKGafzcQE/qecTsen6A==jPBItEO6+e9zoSAQE0KrC8l4uujduLPezVfBgFKITZHqdKiQG56fRh4d1bM2s50rpOd5zPfMncSw4SC8Rr2e2kN1IjxHsAHAyMRTcnrcbqwlxIh8Eyn9xextcfpuLIJWcMf6UfQnCe26azU8/M5vd5YLdt/IVIc8frqAvnASixtIIMyrfcJrFzPw9zxj6FcYFQsjZblKmpV+d4QPEmbSH1JrF5/Pi8hFmIRyx8K3uN2ccA77iC0LeS2w+e3BUTnZuZsNYJMGA2EnAj8HlsQMoxMuejHih12ch4XtgHhuVFpYgAO4pWfGVrEsFdiWAcANguOBrvggpYbj4HAYMGpP7Q==f9LQtC6rY/lPqm3TRxZ7JMhiF+pQEx5va6mSjIg1ZyVfULjQY+IbQnPUAtitWWs5mliilsCNJ1CRayD0GuHCzEd/sqDhjiLYbVzjGd+sjokpgWNWo5s9EDRDcfp5rGThZ+vby0+FMq6JGayQYgXAbF47uRSP1F421ZD9mlPe+a/LTJ5swaAuALyid6zrzATYY6GYKZSPbxnqfW0yBcfI5Xy5AU5GdP7LTDHfru5hKaDbXmgr6ZUTzOeiCDdpLCVe5GBO0PZMtufPCahNlpYjYu16e1vfAet2yfAIb0eoBTOrQZ52NG627XqO7vrzOylrbs+TZ9QGqdFom0I1Zu8Xhg==kQd/WR8Ld6YPM9Aidv1i0JuED5YaPLfqAcpuQ6hG+Dz+xYDDgdTLRTXgT0zadE10j+wAiu5Ue/tkYLaRYI6sG1YYy14bplB4pyYCRgy1+HuUAdRYs1LIdAWz/0wXjs7ZcYKCJKxrnT1wTM01qebr+Pif2pkbcjqIw6VkIOuo/E5PsNCuLH/dNqKn8WMMri0DO3rEHAFQnBMt2NKyLNCElHjIZneljD37uNRHA9qxG6kYzmi5o3kcKT38KcJSTnOTJG8GMZImwhIr7K7kuZEptVP+hK54A40DpMNocNbCT2Ui4e93qs86i7epUCU2Zp6TQPEeDjCR2by1KvT6aMDlJA==";

			RSAPrivateKey privateKey = rsaMotorPagos.getPrivateKey(Constantes.KEY_REQUEST_DECRYPT_APPS);
			String requestJson = rsaMotorPagos.decryptJson(requestEncriptado, privateKey);
			System.out.println("requestJsonHoy: " + requestJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			RSAPublicKey publicKey = rsaMotorPagos.getPublicKey(Constantes.KEY_REQUEST_ENCRYPT_APPS);
			String requestEncriptado = rsaMotorPagos.encryptJson(requestTexto, publicKey);
			System.out.println("requestEncriptado: " + requestEncriptado);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		
		String responseEncriptado = "AMriPZmc0rSmpSQSVtyrifo1ppJJLtQFPdcApIn7dQh1X93bEn5ee8HtgSQLmL3a7yt4l91lHrIXwE3T+S0uMfsSeGhjtGuhZnbeRojoatySHqPuZU63V+Mrc3oKPOTeNvcXvDg0B/RiDnJMTUrm7Nx+KqgxegbCdL/h8O4r5wXc5OkN70PdMqTbjmnIsmw+dDkxTD6FZMmfddDuhHH+lRqH43ICjSGRb/7oOXLXoXI/5/tWd/XVXBtbnydIj8exPrYdqj2Z91Vxs0ly/MJFoZcrc+0WzyKTRTuaBJy5cW1GM8kX9Z/UsQAnqjhgq5HRJQSQRiPD2AxTtIHC3Gcj9g==HuiXjd/SieJBOv+xW0WZC+O8Ll2wmRaTf+HYqbcNR8RWrEn3xWfVANFRztH9qZkskM7t+GNlFscQXqZvRAy6Gf/xrb5JSAuTzMfZxoQg5Jna3Gq7hhO/0CzA//IwLP+sHxzS4pt6cUAutu3Sz3MDslMd42x5lEKsxaPQ+RdaFCwKi95X/GjBWwFO85v33LulBPwWQ4/ji1J7xC3x5uZwSIqjGnzf/+g16PGi3W8dE51FC6Klwg4XDApP8L8qJLOliMj38IRK+0E6Gk0NOzeMQ0nTZvxJHc6H333+Qj1lyPkLmwZTXgTCJ1Ux5yA7fQ+81AL8YnUZuWjPi5rmsk9IFg==Eoi4tDM7UZpWueEInQ71lLSsV1yZrmGqTjjweGdqPir+HyNti3ybh72FfxyY4/CAMkcsBBwwOHuJYKDvgHu6acZgQ8d06aOXEYEJUqZcyHe5jnFsjBbGRVZITjQd+Fp0CCCscluLqbwWNaiD6hdHLUoe19C4XTZ5xBQy+cFPuV5gP8eniMVTr4vQJ5dJ5bROm/MaBspaGQcZMivLeq7JOQOswkc2QMCTqsdK9e1TAnm/f9oeXQxVAorFiFlM4R17YPRuXnkSJyujvcYeleLakUJfv8Jtl+v4SmRuuwkOtAhuE49nxbhaas/lvk5xTAJlZOCr6oDlR43eg3RJf1QM7w==";
		try {
			RSAPrivateKey privateKey = rsaMotorPagos.getPrivateKey(Constantes.KEY_RESPONSE_DECRYPT_APPS);
			String responseDesencriptado = rsaMotorPagos.decryptJson(responseEncriptado, privateKey);
			System.out.println("responseDesencriptado: " + responseDesencriptado);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}
	
}
