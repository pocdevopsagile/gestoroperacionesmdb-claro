package pe.com.claro.enterprise.gestoroperaciones.mdb.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.claro.enterprise.gestoroperaciones.mdb.models.Cuenta;
import pe.com.claro.enterprise.gestoroperaciones.mdb.repositories.BancoRepository;
import pe.com.claro.enterprise.gestoroperaciones.mdb.repositories.CuentaRepository;

import java.math.BigDecimal;
import java.util.List;


@Service
public class CuentaServiceImpl implements CuentaService{
    private CuentaRepository cuentaRepository;
    public CuentaServiceImpl(CuentaRepository cuentaRepository, BancoRepository bancoRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> findAll() {
        return cuentaRepository.findAll();
    }   
   

	@Override
	public Cuenta findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cuenta save(Cuenta cuenta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int revisarTotalTransferencias(Long bancoId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal revisarSaldo(Long cuentaId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void transferir(Long numCuentaOrigen, Long numCuentaDestino, BigDecimal monto, Long bancoId) {
		// TODO Auto-generated method stub
		
	}
}
