package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

public class PagosDetalle {

	private String idTransaccion;
	private String codigoOperacion;
	private int contador;
	private String moneda;
	private String nombreComercio;
	private String numeroComercio;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	public String getNombreComercio() {
		return nombreComercio;
	}
	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}
	public String getNumeroComercio() {
		return numeroComercio;
	}
	public void setNumeroComercio(String numeroComercio) {
		this.numeroComercio = numeroComercio;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	
}
