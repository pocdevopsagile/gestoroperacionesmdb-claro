package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;

public class ListaCustomer extends BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private boolean ingreso;
	private List<Customer> lista;

	public List<Customer> getLista() {
		return lista;
	}

	public void setLista(List<Customer> lista) {
		this.lista = lista;
	}

	public boolean isIngreso() {
		return ingreso;
	}

	public void setIngreso(boolean ingreso) {
		this.ingreso = ingreso;
	}

	@Override
	public String toString() {
		return "ListaCustomer [ingreso=" + ingreso + ", lista=" + lista + "]";
	}
	

	
	
	

}
