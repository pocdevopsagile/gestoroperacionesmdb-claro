package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

public class Monitoreo {

	
	private String idTransaccion;
	private String codigoComponente;
	private String numeroIntento;
	private String errorId;
	private String msjError;
	private String errorProceso;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getCodigoComponente() {
		return codigoComponente;
	}
	public void setCodigoComponente(String codigoComponente) {
		this.codigoComponente = codigoComponente;
	}
	public String getNumeroIntento() {
		return numeroIntento;
	}
	public void setNumeroIntento(String numeroIntento) {
		this.numeroIntento = numeroIntento;
	}
	public String getErrorId() {
		return errorId;
	}
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
	public String getMsjError() {
		return msjError;
	}
	public void setMsjError(String msjError) {
		this.msjError = msjError;
	}
	public String getErrorProceso() {
		return errorProceso;
	}
	public void setErrorProceso(String errorProceso) {
		this.errorProceso = errorProceso;
	}
	
	
}
