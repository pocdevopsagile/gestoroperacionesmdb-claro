package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.imp;


import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetServiciosRespType;
import pe.com.claro.eai.oac.transaccionpagos.TransaccionPagos;
import pe.com.claro.eai.oac.transaccionpagos.TransaccionPagos_Service;
import pe.com.claro.eai.servicecommons.AuditType;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.proxy.TransaccionPagosProxy;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearPagoRespo;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ClaroUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.ELKUtil;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.JAXBUtilitarios;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.PropertiesExterno;

@Service
public class TransaccionPagosProxyImpl implements TransaccionPagosProxy{
	
	private static final Logger LOG = LogManager.getLogger(TransaccionPagosProxyImpl.class);
	
	@Autowired
	private PropertiesExterno propertiesExterno;

	@Override
	public CrearPagoRespo realizarPago(CrearPagoRequest request,String transaccion,ELKLogLegadoBean elkLegadoBean)  {
		
		long tiempoInicio = System.currentTimeMillis();
		String metodo = Thread.currentThread().getStackTrace()[1].getMethodName();	
		LOG.info(Constantes.INICIO+ metodo);
		
		CrearPagoRespo response = new CrearPagoRespo();
		
		Holder<AuditType> audit = new Holder<>();
		Holder<String> xCodAplicacion = new Holder<>();
		Holder<String> xCodMoneda = new Holder<>();
		Holder<String> xTipoIdentific = new Holder<>();
		Holder<String> xDatoIdentific = new Holder<>();
		Holder<String> xNombreCliente = new Holder<>();
		Holder<String> xTrace = new Holder<>();
		Holder<String> xRucAcreedor = new Holder<>();
		Holder<String> xNroIdentifCli = new Holder<>();
		Holder<String> xNroOperacionCobr = new Holder<>();
		Holder<String> xNroOperacionAcre = new Holder<>();
		Holder<String> xNroReferencia = new Holder<>();
		Holder<String> xCodZonaDeudor = new Holder<>();
		Holder<String> xDatoTransaccion = new Holder<>();
		Holder<CrearPagoDetServiciosRespType> xDetDocumentos = new Holder<>();
		Holder<String> xErrStatus = new Holder<>();
		Holder<String> xErrMessage = new Holder<>();
		
		try {
			String url=propertiesExterno.transaccionPagosUrl;
			TransaccionPagos_Service wsService = new TransaccionPagos_Service();
			String connectionTimeOut =propertiesExterno.transaccionPagosConexion;
			String executionTimeOut = propertiesExterno.transaccionPagosEjecucion;
			
			LOG.info(transaccion + " [INICIO] " + propertiesExterno.transaccionPagosNombre
			+ "- Metodo " + propertiesExterno.transaccionPagosOperacion);
			LOG.info(transaccion + " URL: " + url);
			TransaccionPagos portType = wsService.getTransaccionPagosSOAP();
			
			BindingProvider bindingProvider = (BindingProvider) portType;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
			
			LOG.info(transaccion +Constantes.LOG_TIEMPOS_CONEXION + connectionTimeOut);
			bindingProvider.getRequestContext().put("com.sun.xml.ws.connect.timeout",
					Integer.parseInt(connectionTimeOut));
			
			LOG.info(transaccion +Constantes.LOG_TIEMPOS_EJECUCION + executionTimeOut);
			bindingProvider.getRequestContext().put("com.sun.xml.ws.request.timeout",
					Integer.parseInt(executionTimeOut));

			LOG.info(transaccion + " REQUEST: \n" + JAXBUtilitarios.anyObjectToXmlText(request));
			
			elkLegadoBean.setNumeroActividad(Double.parseDouble(Constantes.REGISTRAROAC));
			elkLegadoBean.setNombreActividad("registrarOAC");
			elkLegadoBean.setDetalleActividad("Registro de Pago en OAC");
			elkLegadoBean.setTipoConsulta(ELKUtil.TIPO_CONSULTA_WS);
			elkLegadoBean.setDuenoConsulta(Constantes.NOMBRE_API);
			elkLegadoBean.setOrigenConsulta(url);
			elkLegadoBean.setDetalleConsulta("registrarOAC");
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_REQUEST);
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);
			elkLegadoBean.setMensaje( ClaroUtil.printPrettyJSONString(request));

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
			portType.crearPago(
					request.getTxId(),
					request.getpCodAplicacion(),
					request.getpExtorno(),
					request.getpCodBanco(),
					request.getpCodReenvia(),
					request.getpCodMoneda(),
					request.getpTipoIdentific(),
					request.getpDatoIdentific(),
					request.getpFechaHora(),
					request.getpTrace(),
					request.getpNroOperacion(),
					request.getpNombreComercio(),
					request.getpNroComercio(),
					request.getpCodAgencia(),
					request.getpCodCanal(),
					request.getpCodCiudad(),
					request.getpNroTerminal(),
					request.getpPlaza(),
					request.getpMedioPago(),
					request.getpNroReferencia(),
					request.getpNroCheque1(),
					request.getpNroCheque2(),
					request.getpNroCheque3(),
					request.getpPlazaBcoCheque1(),
					request.getpPlazaBcoCheque2(),
					request.getpPlazaBcoCheque3(),
					request.getpBcoGiradCheque1(), 
					request.getpBcoGiradCheque2(),
					request.getpBcoGiradCheque3(),
					request.getpPagoEfectivo(),
					request.getpPagoTotal(),
					request.getpDatoTransaccion(),
					request.getpDetDocumentos(),
					
					audit,
					xCodAplicacion,
					xCodMoneda,
					xTipoIdentific,
					xDatoIdentific,
					xNombreCliente,
					xTrace,
					xRucAcreedor,
					xNroIdentifCli,
					xNroOperacionCobr,
					xNroOperacionAcre,
					xNroReferencia,
					xCodZonaDeudor,
					xDatoTransaccion,
					xDetDocumentos,
					xErrStatus, 
					xErrMessage);
			
			response.setAudit(audit);
			response.setxCodAplicacion(xCodAplicacion);
			response.setxCodMoneda(xCodMoneda);
			response.setxTipoIdentific(xTipoIdentific);
			response.setxDatoIdentific(xDatoIdentific);
			response.setxNombreCliente(xNombreCliente);
			response.setxTrace(xTrace);
			response.setxRucAcreedor(xRucAcreedor);
			response.setxNroIdentifCli(xNroIdentifCli);
			response.setxNroOperacionCobr(xNroOperacionCobr);
			response.setxNroOperacionAcre(xNroOperacionAcre);
			response.setxNroReferencia(xNroReferencia);
			response.setxCodZonaDeudor(xCodZonaDeudor);
			response.setxDatoTransaccion(xDatoTransaccion);
			response.setxDetDocumentos(xDetDocumentos);
			response.setxErrStatus(xErrStatus);
			response.setxErrMessage(xErrMessage);
			
			elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
			elkLegadoBean.setMensaje( ClaroUtil.printPrettyJSONString(response));
			elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDF);
			
			elkLegadoBean.setIdfIdtCodigo(response.getAudit().value.getErrorCode());
			elkLegadoBean.setIdfIdtMensaje(response.getAudit().value.getErrorMsg());
			elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

			ELKUtil.logObjetoLegado(elkLegadoBean);
			
		} catch (Exception e) {
			LOG.info("{} Detalle error: {}"+ e.getMessage());
			if (e.getMessage().toUpperCase().contains(Constantes.TIMEOUTEXCEPTION.toUpperCase())
					|| e.getMessage().toUpperCase().contains(Constantes.TIMEOUTEXCEPTION2.toUpperCase())) {
			
				AuditType aud=new AuditType();
				aud.setErrorCode(propertiesExterno.codError3);
				aud.setErrorMsg(propertiesExterno.msjError3
						.replace("$ws", propertiesExterno.transaccionPagosNombre)
						.replace("$operacion",  propertiesExterno.transaccionPagosOperacion));

				audit.value=aud;
				response.setAudit(audit);
				LOG.info(transaccion + " codigo Respuesta:" + audit.value.getErrorCode());
				
				elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
				elkLegadoBean.setMensaje(ClaroUtil.printPrettyJSONString(response));
				elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
				
				elkLegadoBean.setIdfIdtCodigo(response.getAudit().value.getErrorCode());
				elkLegadoBean.setIdfIdtMensaje(response.getAudit().value.getErrorMsg());
				elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

				ELKUtil.logObjetoLegado(elkLegadoBean);
				
			} else {
				AuditType aud=new AuditType();
				aud.setErrorCode(propertiesExterno.codError3);
				aud.setErrorMsg(propertiesExterno.msjError3
						.replace("$ws", propertiesExterno.transaccionPagosNombre)
						.replace("$operacion",  propertiesExterno.transaccionPagosOperacion));

				audit.value=aud;
				response.setAudit(audit);
				LOG.info(transaccion + " codigo Respuesta:" + audit.value.getErrorCode());
				
				elkLegadoBean.setTipoMensaje(ELKUtil.TIPO_MENSAJE_RESPONSE);
				elkLegadoBean.setMensaje( ClaroUtil.printPrettyJSONString(response));
				elkLegadoBean.setTipoId(ELKUtil.TIPO_ID_IDT);
				
				elkLegadoBean.setIdfIdtCodigo(response.getAudit().value.getErrorCode());
				elkLegadoBean.setIdfIdtMensaje(response.getAudit().value.getErrorMsg());
				elkLegadoBean.setTiempoRespuesta(System.currentTimeMillis() - tiempoInicio);

				ELKUtil.logObjetoLegado(elkLegadoBean);
			}
		} finally {
			
			LOG.info(transaccion + "[FIN] " +propertiesExterno.transaccionPagosNombre + "- Metodo "
					+ propertiesExterno.transaccionPagosOperacion);
			LOG.info(transaccion + " RESPONSE: \n" + JAXBUtilitarios.anyObjectToXmlText(response));
			
			LOG.info(transaccion + "Tiempo TOTAL Proceso: [" + (System.currentTimeMillis() - tiempoInicio)
					+ " milisegundos ]");
		}
		
		return response;
	}

}
