package pe.com.claro.enterprise.gestoroperaciones.mdb.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExterno {

	@Value("${codigo.idt1}") public String codError1;
	@Value("${mensaje.idt1}") public String msjError1;
	@Value("${codigo.idt2}") public String codError2;
	@Value("${mensaje.idt2}") public String msjError2;
	@Value("${codigo.idt3}") public String codError3;
	@Value("${mensaje.idt3}") public String msjError3;
	@Value("${codigo.idt4}") public String codError4;
	@Value("${mensaje.idt4}") public String msjError4;
	
	@Value("${codigo.idf0}") public String codErrorFuncional0;
	@Value("${mensaje.idf0}") public String msjErrorFuncional0;
	@Value("${codigo.idf1}") public String codErrorFuncional1;
	@Value("${mensaje.idf1}") public String msjErrorFuncional1;
	@Value("${codigo.idf2}") public String codErrorFuncional2;
	@Value("${mensaje.idf2}") public String msjErrorFuncional2;
	@Value("${codigo.idf3}") public String codErrorFuncional3;
	@Value("${mensaje.idf3}") public String msjErrorFuncional3;
	
	//Motor Pagos
	@Value("${motor.pagos.verificar.pago.url}") public String verificapagoUrl;
	@Value("${motor.pagos.verificar.pago.timeout.conexion.ms}") public String verificapagoConexion;
	@Value("${motor.pagos.verificar.pago.timeout.ejecucion.ms}") public String verificapagoEjecucion;
	@Value("${motor.pagos.verificar.pago.userid}") public String verificapagoUserid;
	
	//WS Tipificacion
	@Value("${ws.transaccioninteracciones.service}") public String transaccionInteraccionAsynUrl;
	@Value("${ws.transaccioninteracciones.service.coneccion.milisecond}") public String transaccionInteraccionAsynConexion;
	@Value("${ws.transaccioninteracciones.service.execucion.milisecond}") public String transaccionInteraccionAsynEjecucion;
	@Value("${ws.transaccioninteracciones.service.operacion}") public String transaccionInteraccionOperacion;
	@Value("${ws.transaccioninteracciones.service.nombre}") public String transaccionInteraccionAsynNombre;
	
	@Value("${transaccion.interaccion.request.clase}") public String transaccionInteraccionClase;
	@Value("${transaccion.interaccion.request.codigo.empleado}") public String transaccionInteraccionCodigoEmpleado;
	@Value("${transaccion.interaccion.request.codigo.sistema}") public String transaccionInteraccionCodigoSistema;
	@Value("${transaccion.interaccion.request.cuenta}") public String transaccionInteraccionCuenta;
	@Value("${transaccion.interaccion.request.flag.caso}") public String transaccionInteraccionFlagCaso;
	@Value("${transaccion.interaccion.request.hecho.uno}") public String transaccionInteraccionHechoUno;
	@Value("${transaccion.interaccion.request.metodo.contacto}") public String transaccionInteraccionMetodoContacto;
	@Value("${transaccion.interaccion.request.objId}") public String transaccionInteraccionObjid;
	@Value("${transaccion.interaccion.request.siteObjId}") public String transaccionInteraccionSiteObjId;
	@Value("${transaccion.interaccion.request.subClase}") public String transaccionInteraccionSubClase;
	@Value("${transaccion.interaccion.request.text.resultado}") public String transaccionInteraccionTextResultado;
	@Value("${transaccion.interaccion.request.tipo}") public String transaccionInteraccionTipo;
	@Value("${trtransaccion.interaccion.request.tipo.interaccion}") public String transaccionInteraccionTipoInteraccion;
	@Value("${trtransaccion.interaccion.flag.proceso}") public String transaccionInteraccionFlagProceso;
	@Value("${transaccion.interaccion.request.notas}") public String transaccionInteraccionNota;
	
	//WS TransaccionPagos
	@Value("${ws.transaccion.pagos.service}") public String transaccionPagosUrl;
	@Value("${ws.transaccion.pagos.service.coneccion.milisecond}") public String transaccionPagosConexion;
	@Value("${ws.transaccion.pagos.service.execucion.milisecond}") public String transaccionPagosEjecucion;
	@Value("${ws.transaccion.pagos.service.nombre}") public String  transaccionPagosNombre;
	@Value("${ws.transaccion.pagos.service.operacion}") public String transaccionPagosOperacion;
	
	//WS claro-enterprise-recaudaciones-pagos
	@Value("${ws.recaudaciones.consulta.pagos}") public String recaudacionesPagosUrl;
	@Value("${ws.recaudaciones.consulta.pagos.coneccion.milisecond}") public String recaudacionesPagosConexion;
	@Value("${ws.recaudaciones.consulta.pagos.execucion.milisecond}") public String recaudacionesPagosEjecucion;

	@Value("${transaccion.pagos.request.codigo.extorno}") public String transaccionPagoRequestCodigoExtorno;
	@Value("${transaccion.pagos.request.estado.deudor}") public String transaccionPagoRequestEstadoDeudor;
	@Value("${transaccion.pagos.request.codigo.moneda.eq}") public String transaccionPagoRequestMon;
	
	
	//WS Envio Correo SB
	@Value("${url.envioCorreo}") public String envioCorreoSBUrl;
	@Value("${ws.envioCorreo.conexion.ms}") public String envioCorreoSBConexion;
	@Value("${ws.envioCorreo.ejecucion.ms}") public String envioCorreoSBEjecucion;
	@Value("${campo.auditoria.nombreaplicacion}") public String  nombreAplicacionSB;
	@Value("${campo.auditoria.userid}") public String userIdSB;
	@Value("${envioCorreo.remitente}") public String remitenteSB;
	@Value("${envioCorreo.asunto}") public String asuntoSB;
	@Value("${envioCorreo.mensaje}") public String mensajeSB;
	@Value("${envioCorreo.mensaje.no.exito}") public String  mensajeNoExito;
	@Value("${envioCorreo.flag.no.exito}") public String flagNoExito;
	@Value("${envioCorreo.htmlFlag}") public String htmlFlag;
	
	@Value("${ws.enviocorreo.motor}") public String envioCorreoMotor;
	
	//BD  CCL
	@Value("${bd.ccl.jndi}") public String jndiGoc;
	@Value("${bd.ccl.nombre}") public String nombreGoc;
	@Value("${bd.ccl.owner}") public String ownerGoc;
	@Value("${bd.ccl.package.goc}") public String  packageGoc;
	@Value("${bd.ccl.insertar.transaccion.detalle}") public String insertarTransaccionDetalle;
	@Value("${bd.ccl.actualizar.transaccion.recibo}") public String actualizarTransaccionRecibo;
	@Value("${bd.ccl.buscar.transaccion.recibo}") public String buscarTransaccionRecibo;
	@Value("${bd.ccl.buscar.transaccion.recibo.ap}") public String buscarApTransaccionRecibo;
	@Value("${bd.ccl.insertar.request}") public String  insertarRequest;
	@Value("${bd.ccl.insertar.response}") public String insertResponse;
	@Value("${bd.ccl.sp.registrar.monitoreo}") public String registrarMonitoreo;
	@Value("${bd.ccl.sp.consultar.reintento}") public String consultarReintento;
	@Value("${bd.ccl.sp.actualizardet.estado.trx}") public String actualizarEstado;
	@Value("${bd.ccl.sp.consultar.codigo.aplicacion}") public String consultarApliacion;
	@Value("${bd.ccl.coneccion.timeout}") public String coneccionTimeout;
	@Value("${bd.ccl.ejecucion.timeout}") public String ejecucionTimeout;
	
	//Reintento
	@Value("${wf.medio.pagos.reintento}") public String reintentoWS;
	@Value("${wf.medio.pagos.intento.total}") public String reintentoWF;
	@Value("${wf.medio.pagos.usr.id") public String usrId;
	
	@Value("${jms.goc.provider.url}") public String jmsProvider;
	@Value("${jms.goc.connfactory}") public String jmsFactory;
	@Value("${jms.goc.simple.queue}") public String jmsQueue;


	
}
