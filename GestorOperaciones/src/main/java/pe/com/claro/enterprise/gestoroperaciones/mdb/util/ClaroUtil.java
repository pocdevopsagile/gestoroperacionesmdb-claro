package pe.com.claro.enterprise.gestoroperaciones.mdb.util;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.text.MessageFormat;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Connection;
public class ClaroUtil {

	ClaroUtil(){}

	private static final Logger LOG = LogManager.getLogger(ClaroUtil.class);
	static Locale locale = new Locale("es","ES");
	
	
	public static String obtenerCodigoMoneda(String plantilla,String codigo) {
		String mon=Constantes.MENOSUNOCA;
		try {
			String[] dato=plantilla.split(Constantes.PALOTTE);
			for(String dat:dato) {
				String[] moneda=dat.split(Constantes.SEPARADOR_COMA_UNIC);
				if(codigo.equals(moneda[0])) {
					mon=moneda[1];
				}
			}
			
		}catch(Exception e) {
			LOG.error("Error al obtener moneda", e);
		} return mon;
	}
	
	public static XMLGregorianCalendar obtenerFechaXMLGregorianCalendar(Date fecha) {

		XMLGregorianCalendar xmlGregorianCalendar = null;
		Date fechaActual = new Date();

		try {
			GregorianCalendar gc = new GregorianCalendar();
			if (fecha != null) {
				fechaActual = fecha;
			}
			gc.setTime(fechaActual);
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (Exception e) {
			LOG.info("Error al crear XMLGregorianCalendar", e);
		}

		return xmlGregorianCalendar;

	}
	
	public static Map<String, String> obtenerMapConfiguracion(String cadenaValores, String delimitadorPrimario,
			String delimitadorSecundario) {
		HashMap<String, String> mapConfiguracion = new HashMap<>();
		try {
			String strRegistros = cadenaValores;
			if (strRegistros != null && !strRegistros.isEmpty()) {
				String[] arrayRegistros = strRegistros.split(delimitadorPrimario);
				for (int i = 0; i < arrayRegistros.length; i++) {
					String key = arrayRegistros[i].split(delimitadorSecundario)[0];
					String value = arrayRegistros[i].split(delimitadorSecundario)[1];
					mapConfiguracion.put(key, value);
				}
			}
		} catch (Exception e) {
			LOG.info("Error al obtener el Map de Configuracion: [" + cadenaValores + "]", e);
		}
		return mapConfiguracion;
	}

	public static String nuloAVacio(Object object) {

		if (object == null) {
			return Constantes.TEXTO_VACIO;
		} else {
			return object.toString();
		}
	}

	public static Object nuloAVacioObject(Object object) {
		if (object == null) {
			return Constantes.TEXTO_VACIO;
		} else {
			return object;
		}
	}

	public static String verifiyNull(Object object) {
		String a = null;
		if (object != null) {
			a = object.toString();
		}
		return a;
	}

	public static String convertProperties(Object object) {
		String a = null;
		if (object != null) {
			a = object.toString();
			try {
				a = new String(a.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			} catch (Exception e) {
				LOG.error("Error getProperties Encoding Failed, trayendo Encoding por defecto", e);
			}
		}
		return a;
	}
		
	public static Integer convertirInteger(Object object) {

		Integer res = null;
		if (object instanceof BigDecimal) {
			BigDecimal bd = (BigDecimal) object;
			res = bd.intValueExact();
		}
		return res;
	}

	public static Float convertirFloat(Object object) {
		Float res = null;
		if (object instanceof BigDecimal) {
			BigDecimal bd = (BigDecimal) object;
			res = bd.floatValue();
		}
		return res;
	}

	/**
	 * Genera un String a partir de un Date, si fecha es NULL retorna ""
	 * (vacio).
	 *
	 * @param fecha
	 *            tipo Date
	 * @return String de la forma dd/MM/yyyy
	 */
	public static String dateAString(Date fecha) {
		if (fecha == null) {
			return Constantes.TEXTO_VACIO;
		}
		return dateAString(fecha, Constantes.FORMATO_FECHA_DEFAULT);
	}

	/**
	 * Genera un String a partir de un Date de acuerdo al fomrato enviado, si
	 * fecha es NULL toma la fecha actual.
	 *
	 * @param fecha
	 * @param formato
	 * @return
	 */
	public static String dateAString(Date fecha, String formato) {
		SimpleDateFormat formatoDF = new SimpleDateFormat(formato, Locale.getDefault());
		return formatoDF.format(fecha);
	}

	public static Calendar toCalendar(final String iso8601string) {
		Calendar calendar = Calendar.getInstance();
		
		try {
			boolean exito = false;
			String s = iso8601string.replace("Z", "+00:00");
			if (iso8601string.length() == Constantes.VEINTE) { // *** Sin
																// Precision de
																// Milisegundos
				s = s.substring(0, 22) + s.substring(23);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (iso8601string.length() == Constantes.VEINTICUATRO) { // *** Con
																		// Precision
																		// de
																		// Milisegundos
				s = s.substring(0, 26) + s.substring(27);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (!exito) {
				calendar = null;
			}
		} catch (IndexOutOfBoundsException e) {
			LOG.error("Ocurrio un error al recorrer la cadena de Fecha", e);
			calendar = null;
		} catch (ParseException e) {
			LOG.error("Ocurrio un error al convertir a Date la cadena de la fecha", e);
			calendar = null;
		}
		return calendar;
	}

	public static boolean isValidFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			date = null;
		}
		return date != null;
	}

	public static Date getValidFormatDate(String format, String value) {
		Date date = null;
		if (value != null && !value.isEmpty()) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
				date = sdf.parse(value);
				if (!value.equals(sdf.format(date))) {
					date = null;
				}
			} catch (ParseException ex) {
				date = null;
			}
		}
		return date;
	}
	
	public static String getStoredProcedureByParameters(Object owner, Object packg, Object name) {
		StringBuilder storedProcedure = new StringBuilder();
		if (owner != null && !owner.toString().isEmpty()) {
			storedProcedure.append(owner.toString());
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
		}
		if (packg != null && !packg.toString().isEmpty()) {
			storedProcedure.append(packg.toString());
			storedProcedure.append(Constantes.SEPARADOR_PUNTO);
		}
		if (name != null && !name.toString().isEmpty()) {
			storedProcedure.append(name.toString());
		}
		return storedProcedure.toString();
	}
	

	public static String getExceptionToMensaje(Exception e) {
		String msg = Constantes.TEXTO_VACIO;
		if (e.getCause() != null) {
			msg += e.getCause().toString();
		} else {
			msg += e.toString();
		}
		return msg;
	}

	public static Float floatParse(String object) {
		if (object != null && !object.isEmpty()) {
			return Float.parseFloat(object);
		}
		return null;
	}

	public static String getStoredProcedureJDBC(String sp, int parameters) {
		StringBuilder call = new StringBuilder();
		call.append("call ");
		call.append(sp);
		call.append(Constantes.CHAR_PARENTESIS_IZQUIERDO);
		for (int i = 0; i < parameters; i++) {
			call.append(Constantes.CHAR_INTERROGACION);
			if (i + 1 < parameters) {
				call.append(Constantes.CHAR_COMA);
			}
		}
		call.append(Constantes.CHAR_PARENTESIS_DERECHO);
		return call.toString();
	}

	public static DateFormat getLocalFormat() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat;
	}

	public static String printPrettyJSONString(Object o) {
		try {
			return new ObjectMapper().setDateFormat(ClaroUtil.getLocalFormat())
					.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).writerWithDefaultPrettyPrinter()
					.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			LOG.error("Error JsonProcessingException en metodo printPrettyJSONString", e);
			return null;
		}
	}

	public static String printJSONString(Object o) {
		try {
			return new ObjectMapper().setDateFormat(ClaroUtil.getLocalFormat())
					.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).writeValueAsString(o);
		} catch (JsonProcessingException e) {
			LOG.error("[Metodo printJSONString - Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			return Constantes.TEXTO_VACIO;
		}
	}

	public static Double getDouble(Object valor) {
		try {
			return Double.valueOf(valor.toString());
		} catch (Exception e) {
			LOG.error("[Metodo getDouble - Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			return 0.0;
		}
	}

	public static Date convertirStringADate(String fecha, String formato) {

		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat(formato);

			return formatoFecha.parse(fecha);
		} catch (Exception e) {
			LOG.error("[Metodo convertirStringADate - Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			return null;
		}
	}

	public static double redondear(Object valor) {
		double dblValor = Double.parseDouble(valor.toString());
		return (double) Math.round(dblValor * 10) / 10;
	}

	public static String decimalesCadenasRound(Double valor) {
		valor = (double) Math.round(valor * 100) / 100.0;
		DecimalFormatSymbols simbolos = DecimalFormatSymbols.getInstance(Locale.US);
		DecimalFormat decimalFormat = new DecimalFormat("###0.00", simbolos);
		return decimalFormat.format(valor);
	}

	public static Date modificarFecha(Date fecha, int campo, int cantidad) {
		try {
			Calendar fechaFinal = Calendar.getInstance();
			fechaFinal.setTime(fecha);
			fechaFinal.add(campo, cantidad);
			return fechaFinal.getTime();
		} catch (Exception e) {
			LOG.error("[Metodo modificarFecha - Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			return fecha;
		}
	}

	public static String dateToString(Date fecha, String formato) {
		try {
			return new SimpleDateFormat(formato, Locale.getDefault()).format(fecha);
		} catch (Exception e) {
			LOG.error("[Metodo dateToString - Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			return Constantes.TEXTO_VACIO;
		}
	}

	public static boolean existStringSplitValue(String[] array, String value) {
		boolean existe = false;
		if (array != null) {
			for (String val : array) {
				if (val.equals(value)) {
					existe = true;
					break;
				}
			}
		}
		return existe;
	}

	public static Date stringToJavaDateLegado(String sDate) {
		Date date = null;
		try {
			date = new SimpleDateFormat(Constantes.FORMATO_FECHA_DEFAULT, new Locale("es", "ES")).parse(sDate);
		} catch (ParseException e) {
			date = null;
		}
		return date;
	}
	
    public static Date getUtilDate(String fecha, String formato)  {
    	try {
    	return new SimpleDateFormat(formato,locale).parse(fecha);
    	} catch (ParseException e) {
			return null;
		}
	}
    
    public static String getMessagePropertyParametros(String property, Object... params) {
        return MessageFormat.format(property, params);
    }
    
	public static String obtenerFechaString(String pattern, Date fecha) {

		Date fechaActual = new Date();
		String formato = Constantes.FORMATO_FECHA_DEFAULT;
		String fechaCadena = Constantes.TEXTO_VACIO;

		if (fecha != null) {
			fechaActual = fecha;
		}

		if (pattern != null && !pattern.isEmpty()) {
			formato = pattern;
		}

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(formato);
			fechaCadena = sdf.format(fechaActual);
		} catch (Exception e) {
			LOG.info("Error obtenerFechaString {}", e);
		}

		return fechaCadena;
	}
	
	public static String obtenerFechaActualCadena(String pattern) {

		SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
		String fechaActual=Constantes.TEXTO_VACIO;
		try{
			 fechaActual = sdf.format(new Date());
		}catch(Exception e){
			LOG.info("Error obtenerFechaString {}", e);
		}
		return fechaActual;

	}
	
	public static String getFechaString(Date fecha, String formato) {
		DateFormat dateFormat = new SimpleDateFormat(formato, Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat.format(fecha);
	}
	
	public static String getIp() {
		String ip = Constantes.TEXTO_VACIO;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (Exception e) {
			LOG.error("Ocurrio un error al obtener la IP: ", e);
		}
		return ip;
	}
	
	public static Connection getJNDIConnection(String mensajeTransaccion, String jndi, String tiempoConexion)
			throws Exception {

		long tiempoInicio = System.currentTimeMillis();
		String metodoJava = Thread.currentThread().getStackTrace()[1].getMethodName();
		String cadenaMensaje = mensajeTransaccion + " [" + metodoJava + "]";
		LOG.info(cadenaMensaje + "[INICIO] - METODO: [" + metodoJava + "]");

		Connection result = null;
		try {
			Class.forName(Constantes.ORACLEDRIVER);
			Class.forName(Constantes.ORACLEDRIVER);
			Context initialContext = new InitialContext();
			DataSource datasource = (DataSource) initialContext.lookup(jndi);
			datasource.setLoginTimeout(Integer.parseInt(tiempoConexion));
			result = datasource.getConnection();
		} catch (SQLException | NamingException ex) {
			LOG.error(mensajeTransaccion + Constantes.CANNOT_GET_CONNECTION + ex);
			throw ex;
		}finally{
			LOG.info( cadenaMensaje + "Tiempo TOTAL Proceso: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			LOG.info( cadenaMensaje + "[FIN] - METODO: ["+metodoJava+"]" );
		}
		return result;
	}

	
}