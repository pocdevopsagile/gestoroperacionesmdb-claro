package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DetalleDocumento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String identificador;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String tipoIdentificador;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String tipoTecnologia;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Double monto;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<Documento> listaDocumentos;
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getTipoIdentificador() {
		return tipoIdentificador;
	}
	public void setTipoIdentificador(String tipoIdentificador) {
		this.tipoIdentificador = tipoIdentificador;
	}
	public List<Documento> getListaDocumentos() {
		return listaDocumentos;
	}
	public void setListaDocumentos(List<Documento> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}
	
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
		
	public String getTipoTecnologia() {
		return tipoTecnologia;
	}
	public void setTipoTecnologia(String tipoTecnologia) {
		this.tipoTecnologia = tipoTecnologia;
	}
	
	@Override
	public String toString() {
		return "DetalleDocumento [identificador=" + identificador + ", tipoIdentificador=" + tipoIdentificador
				+ ", tipoTecnologia=" + tipoTecnologia + ", monto=" + monto + ", listaDocumentos=" + listaDocumentos
				+ "]";
	}
	
	
}
