package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import java.math.BigDecimal;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import pe.com.claro.eai.oac.transaccionpagos.CrearPagoDetServiciosReqType;

public class CrearPagoRequest {
	private String txId;
	private String pCodAplicacion;
	private Holder<String> pExtorno;
	private String pCodBanco;
	private String pCodReenvia;
	private String pCodMoneda;
	private String pTipoIdentific;
	private String pDatoIdentific;
	private XMLGregorianCalendar pFechaHora;
	private String pTrace;
	private String pNroOperacion;
	private String pNombreComercio;
	private String pNroComercio;
	private String pCodAgencia;
	private String pCodCanal;
	private String pCodCiudad;
	private String pNroTerminal;
	private String pPlaza;
	private String pMedioPago;
	private String pNroReferencia;
	private String pNroCheque1;
	private String pNroCheque2;
	private String pNroCheque3;
	private String pPlazaBcoCheque1;
	private String pPlazaBcoCheque2;
	private String pPlazaBcoCheque3;
	private String pBcoGiradCheque1;
	private String pBcoGiradCheque2;
	private String pBcoGiradCheque3;
	private BigDecimal pPagoEfectivo;
	private BigDecimal pPagoTotal;
	private String pDatoTransaccion;
	private CrearPagoDetServiciosReqType pDetDocumentos;
	
	public String getTxId() {
		return txId;
	}
	public void setTxId(String txId) {
		this.txId = txId;
	}
	public String getpCodAplicacion() {
		return pCodAplicacion;
	}
	public void setpCodAplicacion(String pCodAplicacion) {
		this.pCodAplicacion = pCodAplicacion;
	}
	public Holder<String> getpExtorno() {
		return pExtorno;
	}
	public void setpExtorno(Holder<String> pExtorno) {
		this.pExtorno = pExtorno;
	}
	public String getpCodBanco() {
		return pCodBanco;
	}
	public void setpCodBanco(String pCodBanco) {
		this.pCodBanco = pCodBanco;
	}
	public String getpCodReenvia() {
		return pCodReenvia;
	}
	public void setpCodReenvia(String pCodReenvia) {
		this.pCodReenvia = pCodReenvia;
	}
	public String getpCodMoneda() {
		return pCodMoneda;
	}
	public void setpCodMoneda(String pCodMoneda) {
		this.pCodMoneda = pCodMoneda;
	}
	public String getpTipoIdentific() {
		return pTipoIdentific;
	}
	public void setpTipoIdentific(String pTipoIdentific) {
		this.pTipoIdentific = pTipoIdentific;
	}
	public String getpDatoIdentific() {
		return pDatoIdentific;
	}
	public void setpDatoIdentific(String pDatoIdentific) {
		this.pDatoIdentific = pDatoIdentific;
	}
	public XMLGregorianCalendar getpFechaHora() {
		return pFechaHora;
	}
	public void setpFechaHora(XMLGregorianCalendar pFechaHora) {
		this.pFechaHora = pFechaHora;
	}
	public String getpTrace() {
		return pTrace;
	}
	public void setpTrace(String pTrace) {
		this.pTrace = pTrace;
	}
	public String getpNroOperacion() {
		return pNroOperacion;
	}
	public void setpNroOperacion(String pNroOperacion) {
		this.pNroOperacion = pNroOperacion;
	}
	public String getpNombreComercio() {
		return pNombreComercio;
	}
	public void setpNombreComercio(String pNombreComercio) {
		this.pNombreComercio = pNombreComercio;
	}
	public String getpNroComercio() {
		return pNroComercio;
	}
	public void setpNroComercio(String pNroComercio) {
		this.pNroComercio = pNroComercio;
	}
	public String getpCodAgencia() {
		return pCodAgencia;
	}
	public void setpCodAgencia(String pCodAgencia) {
		this.pCodAgencia = pCodAgencia;
	}
	public String getpCodCanal() {
		return pCodCanal;
	}
	public void setpCodCanal(String pCodCanal) {
		this.pCodCanal = pCodCanal;
	}
	public String getpCodCiudad() {
		return pCodCiudad;
	}
	public void setpCodCiudad(String pCodCiudad) {
		this.pCodCiudad = pCodCiudad;
	}
	public String getpNroTerminal() {
		return pNroTerminal;
	}
	public void setpNroTerminal(String pNroTerminal) {
		this.pNroTerminal = pNroTerminal;
	}
	public String getpPlaza() {
		return pPlaza;
	}
	public void setpPlaza(String pPlaza) {
		this.pPlaza = pPlaza;
	}
	public String getpMedioPago() {
		return pMedioPago;
	}
	public void setpMedioPago(String pMedioPago) {
		this.pMedioPago = pMedioPago;
	}
	public String getpNroReferencia() {
		return pNroReferencia;
	}
	public void setpNroReferencia(String pNroReferencia) {
		this.pNroReferencia = pNroReferencia;
	}
	public String getpNroCheque1() {
		return pNroCheque1;
	}
	public void setpNroCheque1(String pNroCheque1) {
		this.pNroCheque1 = pNroCheque1;
	}
	public String getpNroCheque2() {
		return pNroCheque2;
	}
	public void setpNroCheque2(String pNroCheque2) {
		this.pNroCheque2 = pNroCheque2;
	}
	public String getpNroCheque3() {
		return pNroCheque3;
	}
	public void setpNroCheque3(String pNroCheque3) {
		this.pNroCheque3 = pNroCheque3;
	}
	public String getpPlazaBcoCheque1() {
		return pPlazaBcoCheque1;
	}
	public void setpPlazaBcoCheque1(String pPlazaBcoCheque1) {
		this.pPlazaBcoCheque1 = pPlazaBcoCheque1;
	}
	public String getpPlazaBcoCheque2() {
		return pPlazaBcoCheque2;
	}
	public void setpPlazaBcoCheque2(String pPlazaBcoCheque2) {
		this.pPlazaBcoCheque2 = pPlazaBcoCheque2;
	}
	public String getpPlazaBcoCheque3() {
		return pPlazaBcoCheque3;
	}
	public void setpPlazaBcoCheque3(String pPlazaBcoCheque3) {
		this.pPlazaBcoCheque3 = pPlazaBcoCheque3;
	}
	public String getpBcoGiradCheque1() {
		return pBcoGiradCheque1;
	}
	public void setpBcoGiradCheque1(String pBcoGiradCheque1) {
		this.pBcoGiradCheque1 = pBcoGiradCheque1;
	}
	public String getpBcoGiradCheque2() {
		return pBcoGiradCheque2;
	}
	public void setpBcoGiradCheque2(String pBcoGiradCheque2) {
		this.pBcoGiradCheque2 = pBcoGiradCheque2;
	}
	public String getpBcoGiradCheque3() {
		return pBcoGiradCheque3;
	}
	public void setpBcoGiradCheque3(String pBcoGiradCheque3) {
		this.pBcoGiradCheque3 = pBcoGiradCheque3;
	}
	public BigDecimal getpPagoEfectivo() {
		return pPagoEfectivo;
	}
	public void setpPagoEfectivo(BigDecimal pPagoEfectivo) {
		this.pPagoEfectivo = pPagoEfectivo;
	}
	public BigDecimal getpPagoTotal() {
		return pPagoTotal;
	}
	public void setpPagoTotal(BigDecimal pPagoTotal) {
		this.pPagoTotal = pPagoTotal;
	}
	public String getpDatoTransaccion() {
		return pDatoTransaccion;
	}
	public void setpDatoTransaccion(String pDatoTransaccion) {
		this.pDatoTransaccion = pDatoTransaccion;
	}
	public CrearPagoDetServiciosReqType getpDetDocumentos() {
		return pDetDocumentos;
	}
	public void setpDetDocumentos(CrearPagoDetServiciosReqType pDetDocumentos) {
		this.pDetDocumentos = pDetDocumentos;
	}
	
	@Override
	public String toString() {
		return "CrearPagoRequest [txId=" + txId + ", pCodAplicacion=" + pCodAplicacion + ", pExtorno=" + pExtorno
				+ ", pCodBanco=" + pCodBanco + ", pCodReenvia=" + pCodReenvia + ", pCodMoneda=" + pCodMoneda
				+ ", pTipoIdentific=" + pTipoIdentific + ", pDatoIdentific=" + pDatoIdentific + ", pFechaHora="
				+ pFechaHora + ", pTrace=" + pTrace + ", pNroOperacion=" + pNroOperacion + ", pNombreComercio="
				+ pNombreComercio + ", pNroComercio=" + pNroComercio + ", pCodAgencia=" + pCodAgencia + ", pCodCanal="
				+ pCodCanal + ", pCodCiudad=" + pCodCiudad + ", pNroTerminal=" + pNroTerminal + ", pPlaza=" + pPlaza
				+ ", pMedioPago=" + pMedioPago + ", pNroReferencia=" + pNroReferencia + ", pNroCheque1=" + pNroCheque1
				+ ", pNroCheque2=" + pNroCheque2 + ", pNroCheque3=" + pNroCheque3 + ", pPlazaBcoCheque1="
				+ pPlazaBcoCheque1 + ", pPlazaBcoCheque2=" + pPlazaBcoCheque2 + ", pPlazaBcoCheque3=" + pPlazaBcoCheque3
				+ ", pBcoGiradCheque1=" + pBcoGiradCheque1 + ", pBcoGiradCheque2=" + pBcoGiradCheque2
				+ ", pBcoGiradCheque3=" + pBcoGiradCheque3 + ", pPagoEfectivo=" + pPagoEfectivo + ", pPagoTotal="
				+ pPagoTotal + ", pDatoTransaccion=" + pDatoTransaccion + ", pDetDocumentos=" + pDetDocumentos + "]";
	}
	
}
