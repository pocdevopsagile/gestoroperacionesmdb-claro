package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class RegistroMonitoreoRequestModel {

	private String idTransaccion;
	private Integer nroIntento;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public Integer getNroIntento() {
		return nroIntento;
	}
	public void setNroIntento(Integer nroIntento) {
		this.nroIntento = nroIntento;
	}
	
	
}
