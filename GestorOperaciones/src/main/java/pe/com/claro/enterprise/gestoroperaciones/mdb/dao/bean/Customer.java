package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

import java.io.Serializable;
public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;
	private String customerId;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + "]";
	}
	
	
}
