package pe.com.claro.enterprise.gestoroperaciones.mdb.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.nio.charset.StandardCharsets;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

public class GestorOperacionesUtil {

	private static final Logger wlLogger = Logger.getLogger(GestorOperacionesUtil.class);
	
	private GestorOperacionesUtil(){}
	public static DateFormat getLocalFormat() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX", Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat;
	}
	
	public static String printPrettyJSONString(Object o) {
		try {
			return new ObjectMapper().setDateFormat(GestorOperacionesUtil.getLocalFormat())
					.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).writerWithDefaultPrettyPrinter()
					.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			wlLogger.error("Error JsonProcessingException en metodo printPrettyJSONString", e);
			return null;
		}
	}
	
	public static String getFechaString(Date fecha, String formato) {
		DateFormat dateFormat = new SimpleDateFormat(formato, Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat.format(fecha);
	}
	
	public static String obtenerFechaString(String pattern, Date fecha) {

		Date fechaActual = new Date();
		String formato = Constantes.FORMATO_FECHA_DEFAULT;
		String fechaCadena = Constantes.TEXTO_VACIO;

		if (fecha != null) {
			fechaActual = fecha;
		}

		if (pattern != null && !pattern.isEmpty()) {
			formato = pattern;
		}

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(formato);
			fechaCadena = sdf.format(fechaActual);
		} catch (Exception e) {
			wlLogger.info("Error obtenerFechaString {}", e);
		}

		return fechaCadena;
	}
	
	public static Map<String, String> obtenerMapConfiguracion(String cadenaValores, String delimitadorPrimario,
			String delimitadorSecundario) {
		HashMap<String, String> mapConfiguracion = new HashMap<>();
		try {
			String strRegistros = cadenaValores;
			if (strRegistros != null && !strRegistros.isEmpty()) {
				String[] arrayRegistros = strRegistros.split(delimitadorPrimario);
				for (int i = 0; i < arrayRegistros.length; i++) {
					String key = arrayRegistros[i].split(delimitadorSecundario)[0];
					String value = arrayRegistros[i].split(delimitadorSecundario)[1];
					mapConfiguracion.put(key, value);
				}
			}
		} catch (Exception e) {
			wlLogger.info("Error al obtener el Map de Configuracion: [" + cadenaValores + "]", e);
		}
		return mapConfiguracion;
	}
	
	public static XMLGregorianCalendar obtenerFechaXMLGregorianCalendar(Date fecha) {

		XMLGregorianCalendar xmlGregorianCalendar = null;
		Date fechaActual = new Date();

		try {
			GregorianCalendar gc = new GregorianCalendar();
			if (fecha != null) {
				fechaActual = fecha;
			}
			gc.setTime(fechaActual);
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (Exception e) {
			wlLogger.info("Error al crear XMLGregorianCalendar", e);
		}

		return xmlGregorianCalendar;

	}
	

	public static String convertProperties(Object object) {
		String a = null;
		if (object != null) {
			a = object.toString();
			try {
				a = new String(a.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			} catch (Exception e) {
				wlLogger.error("Error getProperties Encoding Failed, trayendo Encoding por defecto", e);
			}
		}
		return a;
	}

	/**
     * Reemplaza los saltos de linea de una cadena y tambien los espacios en blanco de ser solicitado
     * @param cadena cadena a modificar
     * @param reemplazo reemplazo para los saltos de linea
     * @param sinEspacios reemplazar espacios tambien
     * @return nueva cadena
     */
    public static String removerSaltosLinea(String cadena, String reemplazo, boolean sinEspacios) {
        return cadena != null ? cadena.replaceAll("\r?\n|\r" + (sinEspacios ? "|\\s" : StringUtils.EMPTY), reemplazo) : null;
    }
	
	/**
     * Reemplaza los saltos de linea de una cadena
     * @param cadena cadena a modificar
     * @param reemplazo reemplazo para los saltos de linea
     * @return nueva cadena
     */
    public static String removerSaltosLinea(String cadena, String reemplazo) {
        return removerSaltosLinea(cadena, reemplazo, false);
    }
	
	/**
     * Remueve los saltos de linea de una cadena
     * @param cadena cadena a modificar
     * @return cadena sin saltos de linea
     */
    public static String removerSaltosLinea(String cadena) {
        return removerSaltosLinea(cadena, StringUtils.EMPTY);
    }
}
