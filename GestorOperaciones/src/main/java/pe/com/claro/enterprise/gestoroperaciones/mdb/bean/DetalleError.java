package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DetalleError implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String actividad;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String descripcionError;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String mensajeError;
	
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	@Override
	public String toString() {
		return "DetalleError [actividad=" + actividad + ", mensajeError=" + mensajeError + ", descripcionError="
				+ descripcionError + "]";
	}	

}
