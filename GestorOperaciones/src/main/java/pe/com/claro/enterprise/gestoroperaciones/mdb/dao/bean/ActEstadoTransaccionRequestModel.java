package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

public class ActEstadoTransaccionRequestModel {

	private String idTransaccion;
	private String estado;
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
	
	
}
