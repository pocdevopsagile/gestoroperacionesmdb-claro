package pe.com.claro.enterprise.gestoroperaciones.mdb.service;

import javax.ws.rs.core.UriInfo;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.PagarReciboRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.response.PagarReciboResponse;

public interface OperacionService {

	
	 public PagarReciboResponse pagarRecibo(String traza, PagarReciboRequest request,HeaderRequest header);
}
