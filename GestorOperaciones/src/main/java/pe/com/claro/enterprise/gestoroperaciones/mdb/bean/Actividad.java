package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

public class Actividad {

	private DetalleError detalleError;
	private boolean resultado;
	
	public DetalleError getDetalleError() {
		return detalleError;
	}
	public void setDetalleError(DetalleError detalleError) {
		this.detalleError = detalleError;
	}
	public boolean isResultado() {
		return resultado;
	}
	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
	
	
}
