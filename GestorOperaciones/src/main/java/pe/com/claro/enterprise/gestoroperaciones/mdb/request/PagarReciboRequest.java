package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.DetalleDocumento;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.TransaccionData;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PagarReciboRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String tipoTarjeta;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroTarjeta;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroAutorizacion;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String codigoOperacion;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String canal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String correo;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String moneda;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Double montoTotal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<DetalleDocumento> listaDetalleDocumentos;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private  TransaccionData  transaccionData;
	
	//VerificarAPP
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String codigoComercio;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Integer codigoPortal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String linea;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroOrdenPortal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Long numeroOrdenMotor;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String medioPago;	
	
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}
	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}
	public List<DetalleDocumento> getListaDetalleDocumentos() {
		return listaDetalleDocumentos;
	}
	public void setListaDetalleDocumentos(List<DetalleDocumento> listaDetalleDocumentos) {
		this.listaDetalleDocumentos = listaDetalleDocumentos;
	}
	public Double getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getCodigoComercio() {
		return codigoComercio;
	}
	public void setCodigoComercio(String codigoComercio) {
		this.codigoComercio = codigoComercio;
	}
	public Integer getCodigoPortal() {
		return codigoPortal;
	}
	public void setCodigoPortal(Integer codigoPortal) {
		this.codigoPortal = codigoPortal;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getNumeroOrdenPortal() {
		return numeroOrdenPortal;
	}
	public void setNumeroOrdenPortal(String numeroOrdenPortal) {
		this.numeroOrdenPortal = numeroOrdenPortal;
	}
	public Long getNumeroOrdenMotor() {
		return numeroOrdenMotor;
	}
	public void setNumeroOrdenMotor(Long numeroOrdenMotor) {
		this.numeroOrdenMotor = numeroOrdenMotor;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	public TransaccionData getTransaccionData() {
		return transaccionData;
	}
	public void setTransaccionData(TransaccionData transaccionData) {
		this.transaccionData = transaccionData;
	}
	@Override
	public String toString() {
		return "PagarReciboRequest [tipoTarjeta=" + tipoTarjeta + ", numeroTarjeta=" + numeroTarjeta
				+ ", numeroAutorizacion=" + numeroAutorizacion + ", codigoOperacion=" + codigoOperacion + ", canal="
				+ canal + ", correo=" + correo + ", moneda=" + moneda + ", montoTotal=" + montoTotal
				+ ", listaDetalleDocumentos=" + listaDetalleDocumentos + ", transaccionData=" + transaccionData
				+ ", codigoComercio=" + codigoComercio + ", codigoPortal=" + codigoPortal + ", linea=" + linea
				+ ", numeroOrdenPortal=" + numeroOrdenPortal + ", numeroOrdenMotor=" + numeroOrdenMotor + ", medioPago="
				+ medioPago + "]";
	}
	
}
