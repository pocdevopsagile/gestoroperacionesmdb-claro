package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;

import pe.com.claro.eai.servicecommons.AuditType;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.CrearInteraccionRequest;

public interface TransaccionInteraccionesAsyn {

	
	public AuditType crearInteraccion(String transaccion,CrearInteraccionRequest request,String flagProceso
			,ELKLogLegadoBean elkLegadoBean);
}
