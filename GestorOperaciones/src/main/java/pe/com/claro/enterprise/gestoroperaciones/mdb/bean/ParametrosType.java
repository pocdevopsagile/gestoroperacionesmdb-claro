package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;

import java.io.IOException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.claro.enterprise.gestoroperaciones.mdb.util.Constantes;

public class ParametrosType {

	private static final Logger LOG = LogManager.getLogger(ParametrosType.class);
	private String campo;
	private String valor;
	public static ParametrosType fromString(String jsonRepresentation) {
		ObjectMapper mapper = new ObjectMapper();
		ParametrosType o = null;
		try {
			o = mapper.readValue(jsonRepresentation, ParametrosType.class);
		} catch (IOException e) {
			LOG.error("[Ocurrio un error]" + Constantes.SALTO_LINEA + e.getMessage(), e);
			o = null;
		}
		return o;
	}

	public ParametrosType() {
		super();
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
