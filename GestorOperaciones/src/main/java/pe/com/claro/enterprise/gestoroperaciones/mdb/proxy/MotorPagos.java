package pe.com.claro.enterprise.gestoroperaciones.mdb.proxy;


import com.fasterxml.jackson.core.JsonProcessingException;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.ELKLogLegadoBean;
import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.HeaderRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppRequest;
import pe.com.claro.enterprise.gestoroperaciones.mdb.request.VerificarAppResponse;


public interface MotorPagos {

	public VerificarAppResponse verificarPago(String trazabilidad, HeaderRequest header,VerificarAppRequest request
			,ELKLogLegadoBean elkLegadoBean)throws JsonProcessingException;
}
