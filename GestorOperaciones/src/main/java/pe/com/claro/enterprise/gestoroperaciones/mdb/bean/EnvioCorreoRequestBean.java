package pe.com.claro.enterprise.gestoroperaciones.mdb.bean;


public class EnvioCorreoRequestBean {

	private AuditoriaRequestSOAPBean auditoriaRequest;
	private String remitente;
	private String destinatario;
	private String asunto;
	private String mensaje;
	private String htmlFlag;
	
	public AuditoriaRequestSOAPBean getAuditoriaRequest() {
		return auditoriaRequest;
	}
	public void setAuditoriaRequest(AuditoriaRequestSOAPBean auditoriaRequest) {
		this.auditoriaRequest = auditoriaRequest;
	}
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getHtmlFlag() {
		return htmlFlag;
	}
	public void setHtmlFlag(String htmlFlag) {
		this.htmlFlag = htmlFlag;
	}
	
}
