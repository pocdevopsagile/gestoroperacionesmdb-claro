package pe.com.claro.enterprise.gestoroperaciones.mdb.dao.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import pe.com.claro.enterprise.gestoroperaciones.mdb.bean.BodyResponse;


@JsonIgnoreProperties(ignoreUnknown=true)
public class TransaccionResponse extends BodyResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private int numeroTransaccionDetale;

	public int getNumeroTransaccionDetale() {
		return numeroTransaccionDetale;
	}

	public void setNumeroTransaccionDetale(int numeroTransaccionDetale) {
		this.numeroTransaccionDetale = numeroTransaccionDetale;
	}

	@Override
	public String toString() {
		return "TransaccionResponse [numeroTransaccionDetale=" + numeroTransaccionDetale + "]";
	}
	
	


	
}
