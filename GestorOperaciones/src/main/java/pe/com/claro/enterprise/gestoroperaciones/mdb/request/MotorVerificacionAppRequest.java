package pe.com.claro.enterprise.gestoroperaciones.mdb.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MotorVerificacionAppRequest {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String request;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String codigoComercio;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Integer codigoPortal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String linea;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String numeroOrdenPortal;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Long numeroOrdenMotor;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String medioPago;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Integer estadoOrden;

	public String getCodigoComercio() {
		return codigoComercio;
	}

	public void setCodigoComercio(String codigoComercio) {
		this.codigoComercio = codigoComercio;
	}

	public Integer getCodigoPortal() {
		return codigoPortal;
	}

	public void setCodigoPortal(Integer codigoPortal) {
		this.codigoPortal = codigoPortal;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getNumeroOrdenPortal() {
		return numeroOrdenPortal;
	}

	public void setNumeroOrdenPortal(String numeroOrdenPortal) {
		this.numeroOrdenPortal = numeroOrdenPortal;
	}

	public Long getNumeroOrdenMotor() {
		return numeroOrdenMotor;
	}

	public void setNumeroOrdenMotor(Long numeroOrdenMotor) {
		this.numeroOrdenMotor = numeroOrdenMotor;
	}

	public String getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	public Integer getEstadoOrden() {
		return estadoOrden;
	}

	public void setEstadoOrden(Integer estadoOrden) {
		this.estadoOrden = estadoOrden;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "MotorVerificacionAppRequest [request=" + request + ", codigoComercio=" + codigoComercio
				+ ", codigoPortal=" + codigoPortal + ", linea=" + linea + ", numeroOrdenPortal=" + numeroOrdenPortal
				+ ", numeroOrdenMotor=" + numeroOrdenMotor + ", medioPago=" + medioPago + ", estadoOrden=" + estadoOrden
				+ "]";
	}
	
	
}
