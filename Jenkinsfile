#!Pipeline
pipeline {
agent any
environment { 
		EMAIL_RECIPIENTS = 'alessandra.rosado2308@gmail.com'
	}
stages {	
    //Build
        stage("Build") {
			tools {
				jdk "JDK8_JENKINS"
			}
			steps {
				echo "build number: ${env.BUILD_NUMBER}"
				echo "build url: ${env.BUILD_URL}"
				echo "workspace: ${env.WORKSPACE}"
				sh "mvn -Dintegration-tests.skip=true -Dmaven.test.skip=true clean package"
			}
		}
	//test       
        stage("Tests") {
			parallel {
					stage('Unit Tests') {
						steps {
							echo "Unit Tests"
							sh 'mvn surefire:test'
							junit allowEmptyResults: true, testResults: '**/target/surefire-reports/*.xml'
						}
					}
					stage('Integration Tests') {
						steps {
							echo "Integration Tests"
							sh 'mvn failsafe:integration-test'
							junit allowEmptyResults: true, testResults: '**/target/surefire-reports/TEST-*.xml'
						}
					}

                    stage('Generate Test Report Jacoco') {
                        tools {
                            jdk "JDK11_JENKINS"
                            }
                            steps {
                                sh 'mvn org.jacoco:jacoco-maven-plugin:report'
                                junit allowEmptyResults: true, testResults: '**/target/surefire-reports/*.xml'
                            }    
                    }

				}
		}

        //sonar

        stage('SonarQube analysis') {
			tools {
				jdk "JDK11_JENKINS"
			}
            steps {
                withSonarQubeEnv('SonarQubeJenkins') {
                    sh "mvn verify sonar:sonar -Dintegration-tests.skip=true -Dmaven.test.failure.ignore=true" + 
							"-Dsonar.projectKey=gestoroperacionesmdb " +
							"-Dsonar.projectName=gestoroperacionesmdb " +
							"-Dsonar.projectVersion=1.0 " +
							"-Dsonar.language=java " +
							"-Dsonar.sources=src " +
							"-Dsonar.sourceEncoding=UTF-8 " +
							"-Dsonar.tests=. " +
							"-Dsonar.test.inclusions=**/*Test*/** " +
							"-Dsonar.exclusions=**/*Test*/**"
                }
            }
       }

        //Nexus

             stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    //dir(cada carpeta)
                    dir ("GestorOperaciones"){
                        def pom = readMavenPom file: "pom.xml";
						dir ("target") {                       
                        nexusArtifactUploader(
                            nexusVersion: 'nexus3',
                            protocol: 'http',
                            nexusUrl: '158.69.62.15:8081',
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: 'GestorOperacionesMDB',
                            credentialsId: 'nexuscredenciales',
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: pom.artifactId + '-1.0.0.' + pom.packaging,
                                type: pom.packaging]
                            ]
                        )
                    }
                    }                   
                                     
                }
            }
        }



	}

	post {
        success {
            sendEmail("Successful");
			notifySlack("SUCCESS");
        }
        unstable {
            sendEmail("Unstable");
			notifySlack("UNSTABLE");
        }
        failure {
            sendEmail("Failed");
			notifySlack("FAILED");
        }
    }

}


def sendEmail(status) {
	BUILD_TRIGGER_BY = "${currentBuild.getBuildCauses()[0].shortDescription} / ${currentBuild.getBuildCauses()[0].userId}"
	echo "BUILD_TRIGGER_BY: ${BUILD_TRIGGER_BY}"
					   
    mail(
            to: "$EMAIL_RECIPIENTS",
            subject: "Build $BUILD_NUMBER - " + status + " (${currentBuild.fullDisplayName})",
            body: "Changes:\n " + getChangeString() + "\n\n Check console output at: $BUILD_URL/console" + "\n")
}

@NonCPS
def getChangeString() {
    MAX_MSG_LEN = 100
    def changeString = ""

    echo "Gathering SCM changes"
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            truncated_msg = entry.msg.take(MAX_MSG_LEN)
            changeString += " - ${truncated_msg} [${entry.author}]\n"
        }
    }

    if (!changeString) {
        changeString = " - No new changes"
    }
    return changeString
}

def notifySlack(String buildStatus = "STARTED") {
    buildStatus = buildStatus ?: "SUCCESS"

    def color

    if (buildStatus == "STARTED") {
        colorNotify = "#D4DADF"
    } else if (buildStatus == "SUCCESS") {
        colorNotify = "#BDFFC3"
    } else if (buildStatus == "UNSTABLE") {
        colorNotify = "#FFFE89"
    } else {
        colorNotify = "#FF9FA1"
    }

    def msg = "${buildStatus}: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"

    slackSend(color: colorNotify, message: msg)
}

